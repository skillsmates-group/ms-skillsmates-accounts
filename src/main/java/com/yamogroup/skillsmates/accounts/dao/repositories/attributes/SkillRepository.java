package com.yamogroup.skillsmates.accounts.dao.repositories.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.attributes.SkillEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepository extends AttributeRepository<SkillEntity> {}
