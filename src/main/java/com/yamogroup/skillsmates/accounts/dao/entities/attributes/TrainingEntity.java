package com.yamogroup.skillsmates.accounts.dao.entities.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.*;
import com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "z_training")
public class TrainingEntity extends AttributeEntity {
    @Column(name = "education")
    @Enumerated(EnumType.STRING)
    private EducationEnum education;

    @ManyToOne
    @JoinColumn(name = "schoolType")
    private SchoolTypeEntity schoolType;

    @ManyToOne
    @JoinColumn(name = "activityArea")
    private ActivityAreaEntity activityArea;

    @Column(name = "schoolName", nullable = false)
    private String schoolName;

    @Column(name = "city")
    private String city;

    @ManyToOne
    @JoinColumn(name = "schoolClass")
    private SchoolClassEntity schoolClass;

    @ManyToOne
    @JoinColumn(name = "level")
    private DiplomaLevelEntity level;

    @ManyToOne
    @JoinColumn(name = "teachingName")
    private TeachingNameEntity teachingName;
}
