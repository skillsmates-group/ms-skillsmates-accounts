package com.yamogroup.skillsmates.accounts.dao.specification;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.SearchParam;
import jakarta.persistence.criteria.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class AccountSpecification extends CommonSpecification {

    public static Specification<AccountEntity> accountMatch(SearchParam searchParam) {
        Specification<AccountEntity> specification = accountIsActive().and(accountIsNotDeleted());
        if (StringUtils.isNotBlank(searchParam.city())) {
            specification = specification.and(accountCityContains(searchParam.city()));
        }
        if (StringUtils.isNotBlank(searchParam.country())) {
            specification = specification.and(accountCountryContains(searchParam.country()));
        }
        if (StringUtils.isNotBlank(searchParam.content())) {
            specification = specification.and(accountHasKeywordInAnyOtherField(searchParam.content()));
        }
        if (searchParam.statuses() != null && !searchParam.statuses().isEmpty()) {
            specification = specification.and(accountStatusIn(searchParam.statuses()));
        }
        return specification;
    }

    public static Specification<AccountEntity> networkFollowersAccountsMatch(
            SearchParam searchParam, List<Long> ids) {
        return searchAccountsMatch(searchParam, ids).and(accountIdIn(ids));
    }

    public static Specification<AccountEntity> networkSuggestionsAccountsMatch(
            SearchParam searchParam, List<Long> ids) {
        return searchAccountsMatch(searchParam, ids).and(accountIdNotIn(ids));
    }

    public static Specification<AccountEntity> searchAccountsMatch(
            SearchParam searchParam, List<Long> ids) {
        Specification<AccountEntity> specification = accountIsActive().and(accountIsNotDeleted());
        if (StringUtils.isNotBlank(searchParam.city())) {
            specification = specification.and(accountCityContains(searchParam.city()));
        }
        if (StringUtils.isNotBlank(searchParam.country())) {
            specification = specification.and(accountCountryContains(searchParam.country()));
        }
        if (StringUtils.isNotBlank(searchParam.content())) {
            specification = specification.and(accountHasKeywordInAnyOtherField(searchParam.content()));
        }
        if (searchParam.statuses() != null && !searchParam.statuses().isEmpty()) {
            specification = specification.and(accountStatusIn(searchParam.statuses()));
        }

        return specification;
    }


    private static Specification<AccountEntity> accountHasKeywordInAnyOtherField(String keyword) {
        return accountFirstnameContains(keyword).or(accountLastnameContains(keyword));
        //        .or(accountAddressContains(keyword))
        //        .or(accountEmailContains(keyword))
        //        .or(accountPhoneNumberContains(keyword))
        //        .or(accountCurrentJobContains(keyword))
        //        .or(accountCompanyContains(keyword));
    }

    private static Specification<AccountEntity> accountFirstnameContains(String textToSearch) {
        return accountFieldContains("firstname", textToSearch);
    }

    private static Specification<AccountEntity> accountLastnameContains(String textToSearch) {
        return accountFieldContains("lastname", textToSearch);
    }

    private static Specification<AccountEntity> accountAddressContains(String textToSearch) {
        return accountFieldContains("address", textToSearch);
    }

    private static Specification<AccountEntity> accountEmailContains(String textToSearch) {
        return accountFieldContains("email", textToSearch);
    }

    private static Specification<AccountEntity> accountPhoneNumberContains(String textToSearch) {
        return accountFieldContains("phoneNumber", textToSearch);
    }

    private static Specification<AccountEntity> accountCompanyContains(String textToSearch) {
        return accountFieldContains("currentCompany", textToSearch);
    }

    private static Specification<AccountEntity> accountCurrentJobContains(String textToSearch) {
        return accountFieldContains("currentJob", textToSearch);
    }

    private static Specification<AccountEntity> accountCityContains(String textToSearch) {
        return accountFieldContains("city", textToSearch == null ? "" : textToSearch);
    }

    private static Specification<AccountEntity> accountCountryContains(String textToSearch) {
        return accountFieldContains("country", textToSearch == null ? "" : textToSearch);
    }

    private static Specification<AccountEntity> accountStatusIn(List<String> statusesAsString) {
        List<StatusEnum> statusEnums =
                statusesAsString == null || statusesAsString.isEmpty()
                        ? List.of(StatusEnum.values())
                        : statusesAsString.parallelStream().map(StatusEnum::valueOf).toList();
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.in(root.get("status")).value(statusEnums);
    }

    private static Specification<AccountEntity> accountIdIn(List<Long> ids) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.in(root.get("id")).value(ids);
    }

    private static Specification<AccountEntity> accountIdNotIn(List<Long> ids) {
        return (root, query, criteriaBuilder) -> {
            Predicate predicate = root.get("id").in(ids);
            return criteriaBuilder.not(predicate);
        };
    }

    private static Specification<AccountEntity> accountIsActive() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("active"), true);
    }

    private static Specification<AccountEntity> accountIsNotDeleted() {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("deleted"), false);
    }

    private static Specification<AccountEntity> accountFieldContains(
            String fieldName, String textToSearch) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get(fieldName)), surroundWithPercentSign(textToSearch));
    }
}
