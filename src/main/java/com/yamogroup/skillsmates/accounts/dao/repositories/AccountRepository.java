package com.yamogroup.skillsmates.accounts.dao.repositories;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository
        extends JpaRepository<AccountEntity, Long>, JpaSpecificationExecutor<AccountEntity> {
    Optional<AccountEntity> findByUuid(String uuid);

    Optional<AccountEntity> findByActiveTrueAndUuid(String uuid);

    Optional<AccountEntity> findByEmail(String email);

    List<AccountEntity> findByActiveTrueAndDeletedFalseAndUuidNotIn(List<String> uuids);

    Page<AccountEntity> findByActiveTrueAndDeletedFalseAndUuidNotIn(
            List<String> uuids, Pageable pageable);

    Page<AccountEntity> findByActiveTrueAndDeletedFalseAndAvatarNotNullAndUuidNotIn(
            List<String> uuids, Pageable pageable);

    @Query(
            value =
                    "SELECT COUNT(*) FROM z_post AS p JOIN z_account AS a ON a.id = p.account WHERE p.deleted IS FALSE AND a.uuid = ?1",
            nativeQuery = true)
    long countPostsByAccount(String accountId);

    @Query(
            value =
                    "SELECT COUNT(DISTINCT id) from z_account WHERE deleted=false AND active=true AND id IN (SELECT DISTINCT "
                            + "emitter FROM z_interaction WHERE receiver=(SELECT id FROM z_account WHERE uuid=:uuid) AND "
                            + "interaction_type=(SELECT id FROM z_interaction_type WHERE code='INTERACTION_TYPE_FOLLOWER') AND "
                            + "deleted=false AND active=true)",
            nativeQuery = true)
    long countFollowersByAccount(@Param("uuid") String accountId);

    @Query(
            value =
                    "SELECT COUNT(DISTINCT id) from z_account WHERE deleted=false AND active=true AND id IN (SELECT DISTINCT "
                            + "receiver FROM z_interaction WHERE emitter=(SELECT id FROM z_account WHERE uuid=:uuid) AND "
                            + "interaction_type=(SELECT id FROM z_interaction_type WHERE code='INTERACTION_TYPE_FOLLOWER') AND "
                            + "deleted=false AND active=true)",
            nativeQuery = true)
    long countFolloweesByAccount(@Param("uuid") String accountId);

    @Query(
            value =
                    "SELECT COUNT(DISTINCT id) from z_account WHERE deleted=false AND active=true AND id NOT IN (SELECT DISTINCT "
                            + "receiver FROM z_interaction WHERE emitter=(SELECT id FROM z_account WHERE uuid=:uuid) AND "
                            + "interaction_type IN (SELECT id FROM z_interaction_type WHERE code='INTERACTION_TYPE_FOLLOWER' OR "
                            + "code='INTERACTION_TYPE_FAVORITE')) AND id NOT IN (SELECT DISTINCT emitter FROM z_interaction WHERE "
                            + "receiver=(SELECT id FROM z_account WHERE uuid=:uuid) AND interaction_type IN (SELECT id FROM "
                            + "z_interaction_type WHERE code='INTERACTION_TYPE_FOLLOWER' OR code='INTERACTION_TYPE_FAVORITE'))",
            nativeQuery = true)
    long countSuggestionsByAccount(@Param("uuid") String accountId);

    List<AccountEntity> findByConnectedTrueAndActiveTrueAndDeletedFalse();

    @Query(
            value =
                    "SELECT * from z_account WHERE deleted=false AND active=true AND LOWER(status) LIKE :status AND "
                            + "LOWER(country) LIKE :country AND (CONCAT(LOWER(firstname), LOWER(lastname)) LIKE :name) "
                            + "AND id IN (SELECT DISTINCT emitter FROM z_interaction WHERE receiver=(SELECT id FROM z_account WHERE "
                            + "uuid=:uuid) AND interaction_type=(SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER') AND deleted=false AND active=true)",
            nativeQuery = true)
    Page<AccountEntity> findFollowers(
            @Param("uuid") String uuid,
            @Param("status") String status,
            @Param("country") String country,
            @Param("name") String name,
            Pageable pageable);


    @Query(
            value =
                    "SELECT * from z_account WHERE deleted=false AND active=true AND "
                            + "id IN (SELECT DISTINCT emitter FROM z_interaction WHERE receiver=(SELECT id FROM z_account WHERE "
                            + "uuid=:uuid) AND interaction_type=(SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER') AND deleted=false AND active=true)",
            nativeQuery = true)
    Page<AccountEntity> findFollowers(@Param("uuid") String uuid, Pageable pageable);

    @Query(
            value =
                    "SELECT COUNT(*) from z_account WHERE deleted=false AND active=true AND "
                            + "id IN (SELECT DISTINCT emitter FROM z_interaction WHERE receiver=(SELECT id FROM z_account WHERE "
                            + "uuid=:uuid) AND interaction_type=(SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER') AND deleted=false AND active=true)",
            nativeQuery = true)
    int countFollowers(@Param("uuid") String uuid);

    @Query(
            value =
                    "SELECT * from z_account WHERE deleted=false AND active=true AND LOWER(status) LIKE :status AND "
                            + "LOWER(country) LIKE :country AND (CONCAT(LOWER(firstname), LOWER(lastname)) LIKE :name) "
                            + "AND id IN (SELECT DISTINCT receiver FROM z_interaction WHERE emitter=(SELECT id FROM z_account WHERE "
                            + "uuid=:uuid) AND interaction_type=(SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER') AND deleted=false AND active=true)",
            nativeQuery = true)
    Page<AccountEntity> findFollowees(
            @Param("uuid") String uuid,
            @Param("status") String status,
            @Param("country") String country,
            @Param("name") String name,
            Pageable pageable);

    @Query(
            value =
                    "SELECT * from z_account WHERE deleted=false AND active=true AND "
                            + " id IN (SELECT DISTINCT receiver FROM z_interaction WHERE emitter=(SELECT id FROM z_account WHERE "
                            + "uuid=:uuid) AND interaction_type=(SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER') AND deleted=false AND active=true)",
            nativeQuery = true)
    Page<AccountEntity> findFollowees(@Param("uuid") String uuid, Pageable pageable);

    @Query(
            value =
                    "SELECT COUNT(*) from z_account WHERE deleted=false AND active=true AND "
                            + " id IN (SELECT DISTINCT receiver FROM z_interaction WHERE emitter=(SELECT id FROM z_account WHERE "
                            + "uuid=:uuid) AND interaction_type=(SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER') AND deleted=false AND active=true)",
            nativeQuery = true)
    int countFollowees(@Param("uuid") String uuid);

    @Query(
            value =
                    "SELECT * from z_account WHERE deleted=false AND active=true AND LOWER(status) LIKE :status AND "
                            + "LOWER(country) LIKE :country AND (CONCAT(LOWER(firstname), LOWER(lastname)) LIKE :name) "
                            + "AND id NOT IN (SELECT DISTINCT receiver FROM z_interaction WHERE emitter=(SELECT id FROM z_account "
                            + "WHERE uuid=:uuid) AND interaction_type IN (SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER' OR code='INTERACTION_TYPE_FAVORITE')) AND id NOT IN "
                            + "(SELECT DISTINCT emitter FROM z_interaction WHERE receiver=(SELECT id FROM z_account WHERE uuid=:uuid) "
                            + "AND interaction_type IN (SELECT id FROM z_interaction_type WHERE code='INTERACTION_TYPE_FOLLOWER' OR "
                            + "code='INTERACTION_TYPE_FAVORITE'))",
            nativeQuery = true)
    Page<AccountEntity> findSuggestions(
            @Param("uuid") String uuid,
            @Param("status") String status,
            @Param("country") String country,
            @Param("name") String name,
            Pageable pageable);

    @Query(
            value =
                    "SELECT * from z_account WHERE deleted=false AND active=true AND avatar IS NOT NULL AND NOT (uuid=:uuid) AND "
                            + "id NOT IN (SELECT DISTINCT receiver FROM z_interaction WHERE emitter=(SELECT id FROM z_account "
                            + "WHERE uuid=:uuid) AND interaction_type IN (SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER' OR code='INTERACTION_TYPE_FAVORITE')) AND id NOT IN "
                            + "(SELECT DISTINCT emitter FROM z_interaction WHERE receiver=(SELECT id FROM z_account WHERE uuid=:uuid) "
                            + "AND interaction_type IN (SELECT id FROM z_interaction_type WHERE code='INTERACTION_TYPE_FOLLOWER' OR "
                            + "code='INTERACTION_TYPE_FAVORITE'))", nativeQuery = true)
    Page<AccountEntity> findSuggestions(@Param("uuid") String uuid, Pageable pageable);

    @Query(
            value =
                    "SELECT COUNT(*) from z_account WHERE deleted=false AND active=true AND avatar IS NOT NULL AND NOT (uuid=:uuid) AND "
                            + "id NOT IN (SELECT DISTINCT receiver FROM z_interaction WHERE emitter=(SELECT id FROM z_account "
                            + "WHERE uuid=:uuid) AND interaction_type IN (SELECT id FROM z_interaction_type WHERE "
                            + "code='INTERACTION_TYPE_FOLLOWER' OR code='INTERACTION_TYPE_FAVORITE')) AND id NOT IN "
                            + "(SELECT DISTINCT emitter FROM z_interaction WHERE receiver=(SELECT id FROM z_account WHERE uuid=:uuid) "
                            + "AND interaction_type IN (SELECT id FROM z_interaction_type WHERE code='INTERACTION_TYPE_FOLLOWER' OR "
                            + "code='INTERACTION_TYPE_FAVORITE'))", nativeQuery = true)
    int countSuggestions(@Param("uuid") String uuid);

    Page<AccountEntity>
    findByConnectedTrueAndActiveTrueAndDeletedFalseAndUuidNotAndStatusLikeAndCountryLike(
            String uuid, String status, String country, Pageable pageable);

    @Query(value = "SELECT COUNT(a) FROM AccountEntity as a WHERE a.status = ?1")
    long countAccountsByStatus(StatusEnum status);

    Page<AccountEntity> findAllByActiveTrueAndDeletedFalseAndIdIn(List<Long> ids, Pageable pageable);

    Page<AccountEntity> findAllByActiveTrueAndDeletedFalseAndConnectedAtAfter(
            ZonedDateTime date, Pageable pageable);

    int countAllByActiveTrueAndDeletedFalseAndConnectedAtAfter(ZonedDateTime date);
}
