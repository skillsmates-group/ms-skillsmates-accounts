package com.yamogroup.skillsmates.accounts.dao.specification;

import org.apache.commons.lang3.StringUtils;

public abstract class CommonSpecification {

  private static final String PERCENT_SIGN = "%";

  protected static String surroundWithPercentSign(String text) {
    //    return endWithPercentSign(startWithPercentSign(text));
    return StringUtils.isNotBlank(text) ? PERCENT_SIGN + text + PERCENT_SIGN : "";
  }

  private static String startWithPercentSign(String text) {
    return PERCENT_SIGN.concat(text != null ? text : "");
  }

  private static String endWithPercentSign(String text) {
    return text != null ? text : "".concat(PERCENT_SIGN);
  }
}
