package com.yamogroup.skillsmates.accounts.dao.repositories.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.attributes.TrainingEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingRepository extends AttributeRepository<TrainingEntity> {}
