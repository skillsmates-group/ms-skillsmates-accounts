package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum StatusEnum {
    STUDENT,
    TEACHER,
    PROFESSIONAL;

    public static StatusEnum getStatus(final String name) {
        return Stream.of(StatusEnum.values()).filter(targetEnum -> targetEnum.name().equals(name.trim())).findFirst().orElse(null);
    }
}
