package com.yamogroup.skillsmates.accounts.dao.enums.document;

import lombok.Getter;

@Getter
public enum OriginEnum {
    AVATAR,
    CHAT,
    POST,
    STATUS
}
