package com.yamogroup.skillsmates.accounts.dao.enums.document;

import lombok.Getter;

@Getter
public enum DocumentClassEnum {
    DOCUMENT,
    IMAGE,
    VIDEO,
    LINK,
    AUDIO
}
