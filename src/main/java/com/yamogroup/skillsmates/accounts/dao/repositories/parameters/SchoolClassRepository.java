package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolClassEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolClassRepository extends ParameterRepository<SchoolClassEntity> {}
