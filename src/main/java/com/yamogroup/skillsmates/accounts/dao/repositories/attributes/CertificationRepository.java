package com.yamogroup.skillsmates.accounts.dao.repositories.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.attributes.CertificationEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificationRepository extends AttributeRepository<CertificationEntity> {}
