package com.yamogroup.skillsmates.accounts.dao.repositories.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.attributes.DiplomaEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface DiplomaRepository extends AttributeRepository<DiplomaEntity> {}
