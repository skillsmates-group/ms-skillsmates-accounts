package com.yamogroup.skillsmates.accounts.dao.enums.skill;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum SkillLevelEnum {
    LEVEL_1("Notions de base"),
    LEVEL_2( "A approfondir"),
    LEVEL_3( "Bien"),
    LEVEL_4("Très bien");

    private final String label;

    SkillLevelEnum(String label) {
        this.label = label;
    }

    public static SkillLevelEnum getLevel(final String name) {
        return Stream.of(SkillLevelEnum.values()).filter(targetEnum -> targetEnum.name().equals(name.trim())).findFirst().orElse(null);
    }
}
