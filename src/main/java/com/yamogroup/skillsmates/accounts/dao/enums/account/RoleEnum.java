package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

@Getter
public enum RoleEnum {
    ADMIN,
    USER
}
