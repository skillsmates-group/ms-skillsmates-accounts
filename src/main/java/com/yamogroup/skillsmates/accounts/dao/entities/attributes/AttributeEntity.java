package com.yamogroup.skillsmates.accounts.dao.entities.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.BaseEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public class AttributeEntity extends BaseEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    @Lob
    private String description;

    @ManyToOne
    @JoinColumn(name = "account")
    private AccountEntity account;
}
