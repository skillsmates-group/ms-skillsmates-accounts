package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum EducationEnum {
    SECONDARY,
    HIGHER;

    public static EducationEnum getEducation(final String name) {
        return Stream.of(EducationEnum.values()).filter(targetEnum -> targetEnum.name().equals(name.trim())).findFirst().orElse(null);
    }
}
