package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

@Getter
public enum TeachingTypeEnum {
    TEACHING_TYPE_1,
    TEACHING_TYPE_2,
    TEACHING_TYPE_3
}
