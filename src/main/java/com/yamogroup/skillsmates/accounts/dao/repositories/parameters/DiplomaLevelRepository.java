package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.DiplomaLevelEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface DiplomaLevelRepository extends ParameterRepository<DiplomaLevelEntity> {}
