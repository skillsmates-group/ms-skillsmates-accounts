package com.yamogroup.skillsmates.accounts.dao.repositories.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.AttributeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AttributeRepository<T extends AttributeEntity> extends JpaRepository<T, Long> {
  Optional<T> findByDeletedFalseAndUuid(String uuid);

  Optional<Page<T>> findByDeletedFalseAndAccount(AccountEntity account, Pageable pageable);

  List<T> findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(String accountUuid);

  long countDistinctByAccountUuidAndDeletedFalse(String accountId);
}
