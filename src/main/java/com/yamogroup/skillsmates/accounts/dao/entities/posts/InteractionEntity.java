package com.yamogroup.skillsmates.accounts.dao.entities.posts;

import com.yamogroup.skillsmates.accounts.dao.entities.BaseEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "z_interaction")
public class InteractionEntity extends BaseEntity {

    @Column(name = "active")
    private boolean active = true;

    @Column(name = "entity", nullable = false)
    private String entity;

    @ManyToOne
    @JoinColumn(name = "interactionType")
    private InteractionTypeEntity interactionType;

    @ManyToOne
    @JoinColumn(name = "emitter")
    private AccountEntity emitter;

    @ManyToOne
    @JoinColumn(name = "receiver")
    private AccountEntity receiver;

    @Column(name = "content")
    private String content;
}
