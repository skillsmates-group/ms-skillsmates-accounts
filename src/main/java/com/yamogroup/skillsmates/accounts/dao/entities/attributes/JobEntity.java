package com.yamogroup.skillsmates.accounts.dao.entities.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivitySectorEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "z_job")
public class JobEntity extends AttributeEntity {

    @ManyToOne
    @JoinColumn(name = "activitySector")
    private ActivitySectorEntity activitySector;

    @ManyToOne
    @JoinColumn(name = "activityArea")
    private ActivityAreaEntity activityArea;

    @Column(name = "company", nullable = false)
    private String company;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "startDate", nullable = false)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDate")
    private Date endDate;

    @Column(name = "city")
    private String city;

    @Column(name = "currentJob")
    private boolean currentJob;
}
