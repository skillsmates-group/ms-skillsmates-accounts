package com.yamogroup.skillsmates.accounts.dao.entities.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "z_certification")
public class CertificationEntity extends AttributeEntity {

    @ManyToOne
    @JoinColumn(name = "schoolType")
    private SchoolTypeEntity schoolType;

    @Column(name = "schoolName", nullable = false)
    private String schoolName;

    @ManyToOne
    @JoinColumn(name = "activityArea")
    private ActivityAreaEntity activityArea;

    @Column(name = "permanent")
    private boolean permanent;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "startDate", nullable = false)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDate")
    private Date endDate;
}
