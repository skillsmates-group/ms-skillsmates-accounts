package com.yamogroup.skillsmates.accounts.dao.entities.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public class ParameterEntity extends BaseEntity {
    @Column(name = "label", nullable = false)
    private String label;

    @Column(name = "code", nullable = false, unique = true)
    private String code;
}
