package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

@Getter
public enum TeachingAreaEnum {
    TEACHING_AREA_1,
    TEACHING_AREA_2,
    TEACHING_AREA_3
}
