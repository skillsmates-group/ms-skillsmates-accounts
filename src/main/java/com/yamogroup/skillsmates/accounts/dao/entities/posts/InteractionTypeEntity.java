package com.yamogroup.skillsmates.accounts.dao.entities.posts;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ParameterEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * LIKE,
 * COMMENT,
 * SHARE,
 * FOLLOWER,
 * FAVORITE,
 * POST
 */
@Getter
@Setter
@Entity
@Table(name = "z_interaction_type")
public class InteractionTypeEntity extends ParameterEntity {
}
