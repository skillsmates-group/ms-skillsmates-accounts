package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

@Getter
public enum SchoolClassEnum {
    SCHOOL_CLASS_1,
    SCHOOL_CLASS_2,
    SCHOOL_CLASS_3
}
