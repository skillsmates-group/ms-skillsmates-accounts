package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.TeachingFieldEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface TeachingFieldRepository extends ParameterRepository<TeachingFieldEntity> {}
