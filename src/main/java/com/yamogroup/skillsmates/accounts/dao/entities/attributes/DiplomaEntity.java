package com.yamogroup.skillsmates.accounts.dao.entities.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.DiplomaLevelEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "z_diploma")
public class DiplomaEntity extends AttributeEntity {

    @Column(name = "education")
    @Enumerated(EnumType.STRING)
    private EducationEnum education;

    @ManyToOne
    @JoinColumn(name = "level")
    private DiplomaLevelEntity level;

    @ManyToOne
    @JoinColumn(name = "schoolType")
    private SchoolTypeEntity schoolType;

    @ManyToOne
    @JoinColumn(name = "activityArea")
    private ActivityAreaEntity activityArea;

    @Column(name = "schoolName", nullable = false)
    private String schoolName;

    @Column(name = "city")
    private String city;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "startDate", nullable = false)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDate")
    private Date endDate;
}
