package com.yamogroup.skillsmates.accounts.dao.enums.interaction;

import lombok.Getter;

@Getter
public enum InteractionTypeEnum {
  LIKE("INTERACTION_TYPE_LIKE"),
  COMMENT("INTERACTION_TYPE_COMMENT"),
  SHARE("INTERACTION_TYPE_SHARE"),
  FOLLOWER("INTERACTION_TYPE_FOLLOWER"),
  FAVORITE("INTERACTION_TYPE_FAVORITE"),
  POST("INTERACTION_TYPE_POST");

  public final String code;

  InteractionTypeEnum(String code) {
    this.code = code;
  }
}
