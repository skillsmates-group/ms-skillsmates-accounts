package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum GenderEnum {
    MALE,
    FEMALE;

    public static GenderEnum getStatus(final String name) {
        return Stream.of(GenderEnum.values()).filter(targetEnum -> targetEnum.name().equals(name.trim())).findFirst().orElse(null);
    }
}
