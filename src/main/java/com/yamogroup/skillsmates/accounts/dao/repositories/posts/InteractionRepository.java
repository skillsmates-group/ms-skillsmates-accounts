package com.yamogroup.skillsmates.accounts.dao.repositories.posts;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.posts.InteractionEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.posts.InteractionTypeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InteractionRepository extends JpaRepository<InteractionEntity, Integer> {
    long countAllByActiveTrueAndDeletedFalseAndReceiverAndInteractionType(AccountEntity receiver, InteractionTypeEntity type);

    long countAllByActiveTrueAndDeletedFalseAndReceiver(AccountEntity receiver);

    Page<InteractionEntity> findAllByActiveTrueAndDeletedFalseAndInteractionTypeAndEntity(
            InteractionTypeEntity interactionType, String entity, Pageable pageable);

    Page<InteractionEntity> findAllByActiveTrueAndDeletedFalseAndInteractionTypeAndEmitter(
            InteractionTypeEntity interactionType, AccountEntity receiver, Pageable pageable);

    @Query(
            value =
                    """
                                SELECT COUNT(*) FROM z_interaction as i 
                                WHERE i.active IS TRUE AND 
                                        i.deleted IS FALSE AND 
                                        i.emitter = :emitter AND 
                                        i.receiver = :receiver AND 
                                        i.interaction_type = :interactionType
                            """, nativeQuery = true)
    long countInteractionsByEmitterAndReceiverAndInteractionType(
            @Param("emitter") long emitter, @Param("receiver") long receiver, @Param("interactionType") long interactionType);

    long countAllByActiveTrueAndDeletedFalseAndEmitterAndReceiverAndInteractionType(AccountEntity emitter, AccountEntity receiver, InteractionTypeEntity type);
}
