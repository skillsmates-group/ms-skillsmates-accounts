package com.yamogroup.skillsmates.accounts.dao.enums.account;

import java.util.stream.Stream;

public enum NetworkTypeEnum {
  FOLLOWERS,
  FOLLOWEES,
  SUGGESTIONS,
  AROUND_YOU,
  FAVORITES,
  ONLINE;

  public static NetworkTypeEnum getNetworkType(final String name) {
    return Stream.of(NetworkTypeEnum.values())
        .filter(targetEnum -> targetEnum.name().equals(name.trim()))
        .findFirst()
        .orElse(null);
  }
}
