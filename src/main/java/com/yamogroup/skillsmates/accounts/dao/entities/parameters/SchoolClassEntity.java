package com.yamogroup.skillsmates.accounts.dao.entities.parameters;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "z_schoolClass")
public class SchoolClassEntity extends ParameterEntity {
}
