package com.yamogroup.skillsmates.accounts.dao.enums.post;

import lombok.Getter;

@Getter
public enum PostTypeEnum {
    ARTICLE,
    STATUS
}
