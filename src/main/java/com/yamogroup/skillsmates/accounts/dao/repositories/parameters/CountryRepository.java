package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.CountryEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends ParameterRepository<CountryEntity> {}
