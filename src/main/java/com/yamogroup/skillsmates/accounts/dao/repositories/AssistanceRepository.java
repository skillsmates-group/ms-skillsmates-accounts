package com.yamogroup.skillsmates.accounts.dao.repositories;

import com.yamogroup.skillsmates.accounts.dao.entities.AssistanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssistanceRepository extends JpaRepository<AssistanceEntity, Integer> {}
