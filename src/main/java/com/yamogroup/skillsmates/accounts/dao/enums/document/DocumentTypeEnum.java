package com.yamogroup.skillsmates.accounts.dao.enums.document;

import lombok.Getter;

@Getter
public enum DocumentTypeEnum {
    DOCUMENT_TYPE_1(DocumentClassEnum.DOCUMENT, "Cours"),
    DOCUMENT_TYPE_2(DocumentClassEnum.DOCUMENT, "Exercices et corrections"),
    DOCUMENT_TYPE_3(DocumentClassEnum.DOCUMENT, "Sujets d'examents et corrections"),
    DOCUMENT_TYPE_4(DocumentClassEnum.DOCUMENT, "Fiches et astuces de revisions"),
    DOCUMENT_TYPE_5(DocumentClassEnum.DOCUMENT, "Mémoires, Exposés, présentation"),
    DOCUMENT_TYPE_6(DocumentClassEnum.DOCUMENT, "Diplômes, certification, CV et lettre de motivation"),
    DOCUMENT_TYPE_7(DocumentClassEnum.DOCUMENT, "Réalisations professionnelles et fiches métiers"),
    DOCUMENT_TYPE_8(DocumentClassEnum.DOCUMENT, "Travaux et articles de recherches / livres"),
    DOCUMENT_TYPE_9(DocumentClassEnum.DOCUMENT, "Autres"),

    VIDEO_TYPE_1(DocumentClassEnum.VIDEO, "Vidéos éducatives et pédagogiques"),
    VIDEO_TYPE_2(DocumentClassEnum.VIDEO, "Conférences"),
    VIDEO_TYPE_3(DocumentClassEnum.VIDEO, "Vidéos d'actualités et dédats"),
    VIDEO_TYPE_4(DocumentClassEnum.VIDEO, "Ma chaine vidéo"),
    VIDEO_TYPE_5(DocumentClassEnum.VIDEO, "Mes vidéos"),
    VIDEO_TYPE_6(DocumentClassEnum.VIDEO, "Autres"),

    AUDIO_TYPE_1(DocumentClassEnum.AUDIO, "Audios éducatifs et pédagogiques"),
    AUDIO_TYPE_2(DocumentClassEnum.AUDIO, "Conférences"),
    AUDIO_TYPE_3(DocumentClassEnum.AUDIO, "Audios d'actualités et débats"),
    AUDIO_TYPE_4(DocumentClassEnum.AUDIO, "Podcasts"),
    AUDIO_TYPE_5(DocumentClassEnum.AUDIO, "Mes audios"),
    AUDIO_TYPE_6(DocumentClassEnum.AUDIO, "Autres"),

    LINK_TYPE_1(DocumentClassEnum.LINK, "Articles d'actualité et éducatifs"),
    LINK_TYPE_2(DocumentClassEnum.LINK, "Outils"),
    LINK_TYPE_3(DocumentClassEnum.LINK, "Blogs et revues scientifiques"),
    LINK_TYPE_4(DocumentClassEnum.LINK, "Sites web et plateformes"),
    LINK_TYPE_5(DocumentClassEnum.LINK, "Questionnaires et jeux"),
    LINK_TYPE_6(DocumentClassEnum.LINK, "Livres"),
    LINK_TYPE_7(DocumentClassEnum.LINK, "Autres"),

    IMAGE_TYPE_1(DocumentClassEnum.IMAGE, "Documents photographiés"),
    IMAGE_TYPE_2(DocumentClassEnum.IMAGE, "Mes photos"),
    IMAGE_TYPE_3(DocumentClassEnum.IMAGE, "Graphiques, images et dessins"),
    IMAGE_TYPE_4(DocumentClassEnum.IMAGE, "Affiches, flyers, évènements"),
    IMAGE_TYPE_5(DocumentClassEnum.IMAGE, "Autres");



    private final DocumentClassEnum classEnum;
    private final String title;

    DocumentTypeEnum(DocumentClassEnum classEnum, String title) {
        this.classEnum = classEnum;
        this.title = title;
    }
}
