package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

@Getter
public enum DiplomaLevelEnum {
    DIPLOMA_LEVEL_1,
    DIPLOMA_LEVEL_2,
    DIPLOMA_LEVEL_3
}
