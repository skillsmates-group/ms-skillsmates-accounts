package com.yamogroup.skillsmates.accounts.dao.entities.account;

import com.yamogroup.skillsmates.accounts.dao.entities.BaseEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.GenderEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.RoleEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum;
import jakarta.persistence.*;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.Date;

@Getter
@Setter
@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "z_account")
public class AccountEntity extends BaseEntity {

    @Column(name = "active")
    private boolean active = false;

    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Column(name = "address")
    private String address;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "hideBirthdate")
    private boolean hideBirthdate;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private GenderEnum gender;

    @Column(name = "biography")
    @Lob
    private String biography;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    @Column(name = "connected", columnDefinition = "boolean default false")
    private boolean connected;

    @Column(name = "last_connection_date")
    private ZonedDateTime connectedAt;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private RoleEnum role;

    @Column(name = "current_job")
    private String currentJob;

    @Column(name = "current_company")
    private String currentCompany;

    @Column(name = "avatar")
    private String avatar;
}
