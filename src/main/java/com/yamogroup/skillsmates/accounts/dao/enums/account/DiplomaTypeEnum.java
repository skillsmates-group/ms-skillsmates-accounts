package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

@Getter
public enum DiplomaTypeEnum {
    DIPLOMA_TYPE_1,
    DIPLOMA_TYPE_2,
    DIPLOMA_TYPE_3
}
