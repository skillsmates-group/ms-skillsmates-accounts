package com.yamogroup.skillsmates.accounts.dao.entities.attributes;

import com.yamogroup.skillsmates.accounts.dao.enums.skill.CategoryEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.skill.SkillLevelEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "z_skill")
public class SkillEntity extends AttributeEntity {

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    private CategoryEnum category;

    @Column(name = "discipline")
    private String discipline;

    @Column(name = "keywords")
    @Lob
    private String keywords;

    @Column(name = "level")
    @Enumerated(EnumType.STRING)
    private SkillLevelEnum level;
}
