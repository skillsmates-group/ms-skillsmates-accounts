package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolTypeRepository extends ParameterRepository<SchoolTypeEntity> {}
