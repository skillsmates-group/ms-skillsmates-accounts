package com.yamogroup.skillsmates.accounts.dao.enums.email;

import lombok.Getter;

@Getter
public enum TypeEnum {
    LIKE,
    COMMENT,
    FAVORITE,
    FOLLOWER,
    CHAT,
    POST,
    SHARE,
    REQUEST_RESET,
    CONFIRM_RESET,
    ACCOUNT_CREATED,
    ACCOUNT_ACTIVATED,
    ACCOUNT_DEACTIVATED,
    ACCOUNT_DELETED_NOTIFICATION,
    ACCOUNT_DELETED_CONFIRMATION
}
