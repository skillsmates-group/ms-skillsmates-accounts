package com.yamogroup.skillsmates.accounts.dao.enums.email;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum TemplateEnum {

    NOTIFICATION("notification.ftl"),
    ACCOUNT("account.ftl");

    private String filename;

    TemplateEnum(String filename) {
        this.filename = filename;
    }

    /**
     * get template enum from name
     * @param name name
     * @return template enum
     */
    public static TemplateEnum fromName(String name){
        TemplateEnum templateEnum = null;
        if (StringUtils.isNotEmpty(name)){
            for (TemplateEnum t: TemplateEnum.values()){
                if (t.name().equalsIgnoreCase(name)){
                    templateEnum = t;
                    break;
                }
            }
        }
        return templateEnum;
    }
}
