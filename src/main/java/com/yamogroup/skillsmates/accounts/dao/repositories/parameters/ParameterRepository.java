package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ParameterEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface ParameterRepository<T extends ParameterEntity> extends JpaRepository<T, Long> {
  Optional<T> findByDeletedFalseAndCode(String code);

  Optional<Page<T>> findByDeletedFalse(Pageable pageable);
}
