package com.yamogroup.skillsmates.accounts.dao.entities;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "z_assistance")
public class AssistanceEntity extends BaseEntity {

    @Column(name = "topic", nullable = false)
    private String topic;

    @Column(name = "message")
    private String message;

    @ManyToOne
    @JoinColumn(name = "account")
    private AccountEntity account;
}
