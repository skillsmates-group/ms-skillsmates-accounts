package com.yamogroup.skillsmates.accounts.dao.enums.account;

import lombok.Getter;

@Getter
public enum SchoolTypeEnum {
    SCHOOL_TYPE_1,
    SCHOOL_TYPE_2,
    SCHOOL_TYPE_3
}
