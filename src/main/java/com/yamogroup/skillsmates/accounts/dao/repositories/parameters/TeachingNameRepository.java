package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.TeachingNameEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface TeachingNameRepository extends ParameterRepository<TeachingNameEntity> {}
