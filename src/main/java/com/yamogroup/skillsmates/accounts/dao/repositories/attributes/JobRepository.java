package com.yamogroup.skillsmates.accounts.dao.repositories.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.attributes.JobEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends AttributeRepository<JobEntity> {}
