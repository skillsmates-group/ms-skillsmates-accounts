package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityAreaRepository extends ParameterRepository<ActivityAreaEntity> {}
