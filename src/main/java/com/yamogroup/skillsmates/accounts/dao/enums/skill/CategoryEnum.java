package com.yamogroup.skillsmates.accounts.dao.enums.skill;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum CategoryEnum {
    CATEGORY_1("A developper"),
    CATEGORY_2("Maitrisée");

    private String label;

    CategoryEnum(String label) {
        this.label = label;
    }

    public static CategoryEnum getCategory(final String name) {
        return Stream.of(CategoryEnum.values()).filter(targetEnum -> targetEnum.name().equals(name.trim())).findFirst().orElse(null);
    }
}
