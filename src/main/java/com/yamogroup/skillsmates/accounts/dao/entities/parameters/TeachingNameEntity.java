package com.yamogroup.skillsmates.accounts.dao.entities.parameters;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "z_teachingName")
public class TeachingNameEntity extends ParameterEntity {

    @ManyToOne
    @JoinColumn(name = "teachingField")
    private TeachingFieldEntity teachingField;
}
