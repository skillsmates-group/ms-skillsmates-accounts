package com.yamogroup.skillsmates.accounts.dao.repositories.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivitySectorEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivitySectorRepository extends ParameterRepository<ActivitySectorEntity> {}
