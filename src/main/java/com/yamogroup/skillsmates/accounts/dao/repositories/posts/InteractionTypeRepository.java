package com.yamogroup.skillsmates.accounts.dao.repositories.posts;

import com.yamogroup.skillsmates.accounts.dao.entities.posts.InteractionTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.ParameterRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InteractionTypeRepository extends ParameterRepository<InteractionTypeEntity> {}
