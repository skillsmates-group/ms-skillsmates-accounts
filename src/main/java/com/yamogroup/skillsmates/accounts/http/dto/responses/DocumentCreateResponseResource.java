package com.yamogroup.skillsmates.accounts.http.dto.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.BaseResponse;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentCreateResponseResource extends BaseResponse {
  private String link;
  private String thumbnail;
  private String title;
  private String description;
}
