package com.yamogroup.skillsmates.accounts.http;

import com.yamogroup.skillsmates.accounts.http.dto.EmailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmailServiceHttp {

  @Value("${service.email.url}")
  private String emailUrl;

  private final RestTemplate restTemplate;

  @Autowired
  public EmailServiceHttp(RestTemplateBuilder restTemplateBuilder) {
    this.restTemplate = restTemplateBuilder.build();
  }

  public EmailResponse getEmail(String emailId) {
    return this.restTemplate.getForObject(emailUrl + "/" + emailId, EmailResponse.class);
  }

  public EmailResponse sendEmail(EmailResponse emailResponse) {
    return this.restTemplate.postForObject(emailUrl, emailResponse, EmailResponse.class);
  }
}
