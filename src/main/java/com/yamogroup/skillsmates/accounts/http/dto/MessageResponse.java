package com.yamogroup.skillsmates.accounts.http.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageResponse {

  @NotBlank(message = "the sender is required")
  @JsonProperty(value = "from", required = true)
  @Email(message = "the email address must be a valid")
  protected String from;

  @NotBlank(message = "the subject is required")
  @JsonProperty(value = "subject", required = true)
  protected String subject;

  @JsonProperty(value = "body")
  protected String body;

  @NotEmpty(message = "the list of receivers can not be empty")
  @JsonProperty("to")
  private List<@Email(message = "email address of a receiver is not valid") String> to =
      new ArrayList<>();

  @JsonProperty("cc")
  private List<@Email(message = "email address of a receiver in copy is not valid") String> cc =
      new ArrayList<>();

  @JsonProperty("cci")
  private List<@Email(message = "email address of a receiver in hidden copy is not valid") String>
      cci = new ArrayList<>();
}
