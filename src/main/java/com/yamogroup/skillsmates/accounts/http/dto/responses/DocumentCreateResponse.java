package com.yamogroup.skillsmates.accounts.http.dto.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentCreateResponse {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private MetaResponse meta;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private DocumentCreateResponseResource resource;
}
