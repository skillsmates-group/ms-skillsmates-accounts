package com.yamogroup.skillsmates.accounts.http.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DocumentCreationRequest {
  @JsonProperty(value = "url")
  String url;

  @JsonProperty(value = "mediaType")
  String mediaType;

  @JsonProperty("origin")
  String origin;

  @JsonProperty("account")
  String account;

  @JsonProperty("document")
  String document;
}
