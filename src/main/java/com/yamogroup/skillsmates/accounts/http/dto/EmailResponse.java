package com.yamogroup.skillsmates.accounts.http.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailResponse extends BaseDto {

  @Valid
  @NotNull(message = "message is required")
  @JsonProperty("message")
  private MessageResponse message;

  @JsonProperty("attachments")
  private List<AttachmentResponse> attachments;

  @JsonProperty("values")
  private List<EmailKeyValue> values;

  @NotBlank(message = "the template is required")
  @JsonProperty(value = "template", required = true)
  private String template;
}
