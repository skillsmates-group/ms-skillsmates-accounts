package com.yamogroup.skillsmates.accounts.http.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Setter
public class InteractionCreateRequest{
    String content;
    String entity;
    String interactionType;
    String emitter;
    String receiver;
}
