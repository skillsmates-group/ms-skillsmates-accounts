package com.yamogroup.skillsmates.accounts.http.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "uuid")
    protected String uuid;

    @JsonProperty(value = "deleted")
    protected boolean deleted;

    @JsonProperty(value = "createdAt")
    protected Date createdAt;

    @JsonProperty(value = "updatedAt")
    protected Date updatedAt;
}
