package com.yamogroup.skillsmates.accounts.http;

import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.http.dto.requests.DocumentCreationRequest;
import com.yamogroup.skillsmates.accounts.http.dto.responses.DocumentCreateResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
@RequiredArgsConstructor
public class DocumentServiceHttp {

  @Value("${ms.skillsmates.document.url}")
  private String msSkillsmatesDocumentUrl;

  private final RestTemplate restTemplate;

  public ResponseEntity<DocumentCreateResponse> saveDocument(
      DocumentCreationRequest request, MultipartFile file) throws Exception {
    try {
      log.info("posting document: {}", ObjectHelper.toString(request));
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.MULTIPART_FORM_DATA);

      MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
      HttpEntity<byte[]> fileEntity;
      if (!file.isEmpty()) {
        ContentDisposition contentDisposition =
            ContentDisposition.builder("form-data")
                .name("file")
                .filename(file.getOriginalFilename() != null ? file.getOriginalFilename() : "")
                .build();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
        fileMap.add("Content-type", file.getContentType());
        fileEntity = new HttpEntity<>(file.getBytes(), fileMap);
      } else {
        fileEntity = new HttpEntity<>(new byte[0], fileMap);
      }

      MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
      body.add("file", fileEntity);
      body.add("account", request.getAccount());
      body.add("url", request.getUrl());
      body.add("origin", request.getOrigin());
      body.add("mediaType", request.getMediaType());
      body.add("document", request.getDocument());

      HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
      return restTemplate.postForEntity(
          msSkillsmatesDocumentUrl, requestEntity, DocumentCreateResponse.class);
    } catch (Exception e) {
      log.info("error saving document: {} - {}", ObjectHelper.toString(request), e.getMessage());
      throw new Exception(e.getMessage());
    }
  }
}
