package com.yamogroup.skillsmates.accounts.exceptions;

public enum ExceptionTypes {
  INVALID_OPERATOR_ID,
  MISSING_REQUIRED_FIELD,
  BAD_REQUEST;
}
