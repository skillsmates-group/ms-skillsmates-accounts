package com.yamogroup.skillsmates.accounts.exceptions;

import com.yamogroup.skillsmates.accounts.rest.dto.responses.error.ErrorsResponse;

public class DateFormatException extends DomainException {

  public DateFormatException(String message) {
    super(message);
  }

  public DateFormatException(String message, ErrorsResponse errorsResponse) {
    super(message, errorsResponse);
  }
}
