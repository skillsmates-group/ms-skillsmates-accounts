package com.yamogroup.skillsmates.accounts.exceptions;

public class SessionExpiredException extends Exception {
    public SessionExpiredException(String message) {
        super(message);
    }
}
