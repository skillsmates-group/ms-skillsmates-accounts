package com.yamogroup.skillsmates.accounts.exceptions;

import com.yamogroup.skillsmates.accounts.rest.dto.responses.error.ErrorsResponse;

public class UnknownException extends DomainException {

  public UnknownException(final String message) {
    super(message);
  }

  public UnknownException(final String message, final ErrorsResponse errorsResponse) {
    super(message, errorsResponse);
  }
}
