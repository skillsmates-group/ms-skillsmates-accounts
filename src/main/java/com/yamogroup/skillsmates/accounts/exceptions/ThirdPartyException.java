package com.yamogroup.skillsmates.accounts.exceptions;

public class ThirdPartyException extends Exception {

  public ThirdPartyException(String message) {
    super(message);
  }
}
