package com.yamogroup.skillsmates.accounts.exceptions;

public class NotActivatedException extends Exception {

  public NotActivatedException(String message) {
    super(message);
  }
}
