package com.yamogroup.skillsmates.accounts.exceptions;

public class AlreadyExistsException extends Exception {

  public AlreadyExistsException(String message) {
    super(message);
  }
}
