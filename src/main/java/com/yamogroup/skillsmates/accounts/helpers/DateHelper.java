package com.yamogroup.skillsmates.accounts.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateHelper {

    public static Date toDate(String dateString) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    public static long differenceBetweenDates(Date firstDate, Date secondDate) {
        long diffInMill = Math.abs(secondDate.getTime() - firstDate.getTime());
        return TimeUnit.DAYS.convert(Math.abs(diffInMill), TimeUnit.MILLISECONDS);
    }
}
