package com.yamogroup.skillsmates.accounts.config;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ExceptionTypes;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

@Component
public class CryptUtil implements Serializable {

    private static final long serialVersionUID = -2557185165626007488L;

    @Value("${ms.skillsmates.crypt.key}")
    private String key;

    public String crypt(String string) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, UnsupportedEncodingException {
        byte[] decodedKey = Base64.getUrlDecoder().decode(key);
        Key aesKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, aesKey);

        byte[] encrypted = cipher.doFinal(string.getBytes(StandardCharsets.UTF_8));

        return Base64.getUrlEncoder().encodeToString(encrypted);
    }

    public String secureCrypt(String string) throws NoSuchPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, BadRequestException {
        String encrypted = crypt(string);
        if (StringUtils.isBlank(encrypted) || StringUtils.contains(encrypted, "/")) {
            throw new BadRequestException("Failed to encrypt id", ExceptionTypes.BAD_REQUEST);
        }
        return encrypted;
    }

    public String decrypt(String string) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        byte[] decodedKey = Base64.getUrlDecoder().decode(key);
        Key aesKey = new SecretKeySpec(Arrays.copyOf(decodedKey, 16), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        byte[] decrypted = cipher.doFinal(Base64.getUrlDecoder().decode(string));
        return new String(decrypted);
    }
}
