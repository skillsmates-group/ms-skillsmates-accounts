package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.CountryEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CountryServiceImpl extends AbstractParameterServiceImpl<CountryEntity> {

  @Autowired
  protected CountryServiceImpl(CountryRepository countryRepository) {
    super(countryRepository);
  }
}
