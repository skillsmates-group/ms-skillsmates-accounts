package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ParameterEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.ParameterRepository;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.services.ParameterService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

@Service
@Transactional
public abstract class AbstractParameterServiceImpl<E extends ParameterEntity>
    implements ParameterService<E> {

  protected final ParameterRepository<E> repository;

  protected AbstractParameterServiceImpl(ParameterRepository<E> repository) {
    this.repository = repository;
  }

  @Override
  public E findOrCreate(E e) {
    Optional<E> entity = repository.findByDeletedFalseAndCode(e.getCode());
    return entity.orElseGet(() -> saveEntity(e));
  }

  private E saveEntity(E entity) {
    entity.setCode(entity.getUuid().replace("-", "").toUpperCase());
    return repository.save(entity);
  }

  @Override
  public Page<E> findAll() {
    return repository
        .findByDeletedFalse(Pager.of())
        .orElse(new PageImpl<>(Collections.emptyList()));
  }

  @Override
  public Page<E> findAll(Pageable pageable) {
    return repository.findByDeletedFalse(pageable).orElse(new PageImpl<>(Collections.emptyList()));
  }
}
