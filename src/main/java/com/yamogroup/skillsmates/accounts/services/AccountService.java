package com.yamogroup.skillsmates.accounts.services;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.NetworkTypeEnum;
import com.yamogroup.skillsmates.accounts.exceptions.AlreadyExistsException;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.exceptions.ThirdPartyException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.PasswordEdit;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.SearchParam;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDeactivateResource;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDeleteResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountStatusMetricResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.AccountAttributesResponse;
import org.springframework.web.multipart.MultipartFile;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public interface AccountService {
    AccountEntity create(AccountEntity entity)
            throws AlreadyExistsException, NoSuchAlgorithmException, ThirdPartyException;

    AccountDetailsResponse find(String uuid) throws NotFoundException;

    AccountDetailsResponse findSuggestions(String uuid);

    AccountDetailsResponse findOnline();

    AccountAttributesResponse findAccountAttributes(String accountId);

    AccountDetailsResponse loadNetwork(
            NetworkTypeEnum networkTypeEnum,
            String accountId,
            Integer offset,
            Integer limit,
            String status,
            String country,
            String name);

    AccountDetailsResponse updateBiography(String accountId, AccountEntity accountEntity);

    AccountDetailsResponse update(String accountId, AccountEntity accountEntity);

    AccountDeactivateResource deactivateAccount(String accountId);

    AccountDeleteResponse deleteAccount(String accountId);

    AccountDetailsResponse searchAccount(SearchParam searchParam, Integer offset, Integer limit);

    AccountStatusMetricResponse findAccountStatusMetric();

    AccountDetailsResponse initPasswordReset(String email)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException;

    AccountDetailsResponse resetPassword(String password, String token)
            throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException,
            BadPaddingException, InvalidKeyException, UnsupportedEncodingException,
            BadRequestException;

    AccountEntity saveAvatar(String accountId, MultipartFile file) throws ThirdPartyException;

    AccountDetailsResponse changePassword(String accountUuid, PasswordEdit passwordEditRequest)
            throws BadRequestException, NoSuchAlgorithmException;
}
