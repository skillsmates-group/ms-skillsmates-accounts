package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.CertificationEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.AttributeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.ActivityAreaServiceImpl;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.SchoolTypeServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Slf4j
@Service
@Transactional
public class CertificationServiceImpl extends AbstractAttributeServiceImpl<CertificationEntity> {

    private final SchoolTypeServiceImpl schoolTypeService;
    private final ActivityAreaServiceImpl activityAreaService;

    @Autowired
    protected CertificationServiceImpl(
            AccountRepository accountRepository,
            CryptUtil cryptUtil,
            EmailServiceHttp emailServiceHttp,
            AttributeRepository<CertificationEntity> attributeRepository,
            SchoolTypeServiceImpl schoolTypeService,
            ActivityAreaServiceImpl activityAreaService) {
        super(accountRepository, cryptUtil, emailServiceHttp, attributeRepository);
        this.schoolTypeService = schoolTypeService;
        this.activityAreaService = activityAreaService;
    }

    @Override
    public CertificationEntity save(CertificationEntity certificationEntity) {
        log.info(
                "START SERVICE SAVE CERTIFICATION WITH BODY {}",
                ObjectHelper.toString(certificationEntity));
        if (certificationEntity.getActivityArea() != null) {
            certificationEntity.setActivityArea(
                    activityAreaService.findOrCreate(certificationEntity.getActivityArea()));
        }
        if (certificationEntity.getSchoolType() != null) {
            certificationEntity.setSchoolType(
                    schoolTypeService.findOrCreate(certificationEntity.getSchoolType()));
        }
        certificationEntity.setAccount(getAccountEntity(certificationEntity.getAccount().getUuid()));
        return attributeRepository.save(certificationEntity);
    }

    @Override
    public CertificationEntity update(String certificationId, CertificationEntity entity)
            throws BadRequestException {
        log.info("START SERVICE UPDATE CERTIFICATION WITH BODY {}", ObjectHelper.toString(entity));
        CertificationEntity certificationEntity = findEntity(certificationId);
        if (!StringUtils.equals(
                entity.getAccount().getUuid(), certificationEntity.getAccount().getUuid())) {
            log.error(
                    "This certification does not belong to this account: {}  - {}",
                    entity.getAccount().getUuid(),
                    certificationEntity.getAccount().getUuid());
            throw new ForbiddenException("Ce compte n'a pas les droit de modifier cette certification");
        }
        if (entity.getSchoolType() != null) {
            certificationEntity.setSchoolType(schoolTypeService.findOrCreate(entity.getSchoolType()));
        }
        certificationEntity.setSchoolName(entity.getSchoolName());
        certificationEntity.setTitle(entity.getTitle());
        if (entity.getActivityArea() != null) {
            certificationEntity.setActivityArea(
                    activityAreaService.findOrCreate(entity.getActivityArea()));
        }
        certificationEntity.setPermanent(entity.isPermanent());
        certificationEntity.setStartDate(entity.getStartDate());
        certificationEntity.setEndDate(entity.getEndDate());
        certificationEntity.setDescription(entity.getDescription());
        return attributeRepository.save(certificationEntity);
    }

    @Override
    public CertificationEntity delete(String certificationId) {
        log.info("START SERVICE DELETE CERTIFICATION OF ID {}", certificationId);
        CertificationEntity certificationEntity = findEntity(certificationId);
        certificationEntity.setDeleted(true);
        return attributeRepository.save(certificationEntity);
    }

    @Override
    public CertificationEntity find(String certificationId) {
        log.info("START SERVICE FIND CERTIFICATION OF ID {}", certificationId);
        return findEntity(certificationId);
    }

    @Override
    public Page<CertificationEntity> findByAccount(String accountId) {
        log.info("START SERVICE FIND CERTIFICATIONS OF ACCOUNT WITH ID {}", accountId);
        return attributeRepository
                .findByDeletedFalseAndAccount(getAccountEntity(accountId), Pager.of())
                .orElse(new PageImpl<>(Collections.emptyList()));
    }
}
