package com.yamogroup.skillsmates.accounts.services;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.SearchParam;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.CountNetworkResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NetworkService {

    Page<AccountEntity> findSuggestions(String accountId);

    Page<AccountEntity> findFollowers(String accountId);

    Page<AccountEntity> findFollowees(String accountId);

    Page<AccountEntity> findAroundYou(String accountId, Pageable pageable);

    AccountDetailsResponse searchNetworkAccounts(SearchParam searchParam, Pageable pageable);

    CountNetworkResponse countNetworkAccounts(SearchParam searchParam);
}
