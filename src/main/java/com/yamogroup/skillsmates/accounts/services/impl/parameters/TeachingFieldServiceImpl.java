package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.TeachingFieldEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.TeachingFieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeachingFieldServiceImpl extends AbstractParameterServiceImpl<TeachingFieldEntity> {

  @Autowired
  protected TeachingFieldServiceImpl(TeachingFieldRepository teachingFieldRepository) {
    super(teachingFieldRepository);
  }
}
