package com.yamogroup.skillsmates.accounts.services;

import com.yamogroup.skillsmates.accounts.dao.entities.AssistanceEntity;

public interface AssistanceService {
  AssistanceEntity create(AssistanceEntity entity, String accountId);
}
