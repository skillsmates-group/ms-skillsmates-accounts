package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.attributes.TrainingEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.TeachingFieldEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.TeachingNameEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.TeachingNameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeachingNameServiceImpl extends AbstractParameterServiceImpl<TeachingNameEntity> {

  protected final TeachingFieldServiceImpl teachingFieldService;

  @Autowired
  protected TeachingNameServiceImpl(
      TeachingNameRepository teachingNameRepository,
      TeachingFieldServiceImpl teachingFieldService) {
    super(teachingNameRepository);
    this.teachingFieldService = teachingFieldService;
  }

  public TeachingNameEntity findOrCreate(TrainingEntity training) {
    TeachingFieldEntity teachingFieldEntity =
        teachingFieldService.findOrCreate(training.getTeachingName().getTeachingField());
    TeachingNameEntity teachingNameEntity = findOrCreate(training.getTeachingName());
    teachingNameEntity.setTeachingField(teachingFieldEntity);
    return repository.save(teachingNameEntity);
  }
}
