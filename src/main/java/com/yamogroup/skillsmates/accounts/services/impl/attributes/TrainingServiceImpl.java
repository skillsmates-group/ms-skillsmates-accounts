package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.TrainingEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.AttributeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ExceptionTypes;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
@Transactional
public class TrainingServiceImpl extends AbstractAttributeServiceImpl<TrainingEntity> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingServiceImpl.class);

    private final SchoolTypeServiceImpl schoolTypeService;
    private final SchoolClassServiceImpl schoolClassService;
    private final DiplomaLevelServiceImpl diplomaLevelService;
    private final TeachingNameServiceImpl teachingNameService;
    private final ActivityAreaServiceImpl activityAreaService;

    @Autowired
    protected TrainingServiceImpl(
            AccountRepository accountRepository,
            CryptUtil cryptUtil,
            EmailServiceHttp emailServiceHttp,
            AttributeRepository<TrainingEntity> attributeRepository,
            SchoolTypeServiceImpl schoolTypeService,
            SchoolClassServiceImpl schoolClassService,
            DiplomaLevelServiceImpl diplomaLevelService,
            TeachingNameServiceImpl teachingNameService,
            ActivityAreaServiceImpl activityAreaService) {
        super(accountRepository, cryptUtil, emailServiceHttp, attributeRepository);
        this.schoolTypeService = schoolTypeService;
        this.schoolClassService = schoolClassService;
        this.diplomaLevelService = diplomaLevelService;
        this.teachingNameService = teachingNameService;
        this.activityAreaService = activityAreaService;
    }

    @Override
    public TrainingEntity save(TrainingEntity trainingEntity) throws BadRequestException {
        LOGGER.info("saving training: {}", ObjectHelper.toString(trainingEntity));
        validateTraining(trainingEntity);
        if (trainingEntity.getSchoolType() != null) {
            trainingEntity.setSchoolType(schoolTypeService.findOrCreate(trainingEntity.getSchoolType()));
        }
        if (trainingEntity.getSchoolClass() != null) {
            trainingEntity.setSchoolClass(
                    schoolClassService.findOrCreate(trainingEntity.getSchoolClass()));
        }
        if (trainingEntity.getLevel() != null) {
            trainingEntity.setLevel(diplomaLevelService.findOrCreate(trainingEntity.getLevel()));
        }
        if (trainingEntity.getTeachingName() != null) {
            trainingEntity.setTeachingName(
                    teachingNameService.findOrCreate(trainingEntity.getTeachingName()));
        }
        if (trainingEntity.getActivityArea() != null) {
            trainingEntity.setActivityArea(
                    activityAreaService.findOrCreate(trainingEntity.getActivityArea()));
        }
        trainingEntity.setAccount(getAccountEntity(trainingEntity.getAccount().getUuid()));
        return attributeRepository.save(trainingEntity);
    }

    @Override
    public TrainingEntity update(String id, TrainingEntity entity) throws BadRequestException {
        LOGGER.info("updating training: {}", ObjectHelper.toString(entity));
        validateTraining(entity);
        TrainingEntity trainingEntity = findEntity(id);
        if (!StringUtils.equals(entity.getAccount().getUuid(), trainingEntity.getAccount().getUuid())) {
            LOGGER.error(
                    "This training does not belong to this account: {}  - {}",
                    entity.getAccount().getUuid(),
                    trainingEntity.getAccount().getUuid());
            throw new ForbiddenException("Ce compte n'a pas les droit de modifier cette formation");
        }
        if (entity.getSchoolType() != null) {
            trainingEntity.setSchoolType(schoolTypeService.findOrCreate(entity.getSchoolType()));
        }
        trainingEntity.setSchoolName(entity.getSchoolName());
        trainingEntity.setCity(entity.getCity());
        if (entity.getSchoolClass() != null) {
            trainingEntity.setSchoolClass(schoolClassService.findOrCreate(entity.getSchoolClass()));
        }
        if (entity.getLevel() != null) {
            trainingEntity.setLevel(diplomaLevelService.findOrCreate(entity.getLevel()));
        }
        trainingEntity.setDescription(entity.getDescription());
        trainingEntity.setTitle(entity.getTitle());
        if (entity.getTeachingName() != null) {
            trainingEntity.setTeachingName(teachingNameService.findOrCreate(entity));
        }
        if (entity.getActivityArea() != null) {
            trainingEntity.setActivityArea(
                    activityAreaService.findOrCreate(entity.getActivityArea()));
        }
        trainingEntity.setEducation(entity.getEducation());
        return attributeRepository.save(trainingEntity);
    }

    @Override
    public TrainingEntity delete(String id) {
        LOGGER.info("deleting training: {}", id);
        TrainingEntity trainingEntity = findEntity(id);
        trainingEntity.setDeleted(true);
        return attributeRepository.save(trainingEntity);
    }

    @Override
    public TrainingEntity find(String id) {
        LOGGER.info("finding training: {}", id);
        return findEntity(id);
    }

    @Override
    public Page<TrainingEntity> findByAccount(String accountId) {
        LOGGER.info("finding trainings by account: {}", accountId);
        return attributeRepository
                .findByDeletedFalseAndAccount(getAccountEntity(accountId), Pager.of())
                .orElse(new PageImpl<>(Collections.emptyList()));
    }

    /**
     * check if training is correct
     *
     * @param trainingDto dto
     * @throws BadRequestException if training is not correct
     */
    private void validateTraining(TrainingEntity trainingDto) throws BadRequestException {
        if (trainingDto.getEducation() == null) {
            LOGGER.error("Education does not exist: {}", ObjectHelper.toString(trainingDto));
            throw new BadRequestException("Education est incorrect", ExceptionTypes.BAD_REQUEST);
        }
    }
}
