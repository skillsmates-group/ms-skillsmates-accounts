package com.yamogroup.skillsmates.accounts.services.impl;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.AssistanceEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.AssistanceRepository;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.services.AssistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssistanceServiceImpl extends AbstractAccountServiceImpl implements AssistanceService {

  private final AssistanceRepository assistanceRepository;

  @Autowired
  public AssistanceServiceImpl(
      AccountRepository accountRepository,
      EmailServiceHttp emailServiceHttp,
      CryptUtil cryptUtil,
      AssistanceRepository assistanceRepository) {
    super(accountRepository, emailServiceHttp, cryptUtil);
    this.assistanceRepository = assistanceRepository;
  }

  @Override
  public AssistanceEntity create(AssistanceEntity entity, String accountId) {
    entity.setAccount(getAccountEntity(accountId));
    return assistanceRepository.save(entity);
  }
}
