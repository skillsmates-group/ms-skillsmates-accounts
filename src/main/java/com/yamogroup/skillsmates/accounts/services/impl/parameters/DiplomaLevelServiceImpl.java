package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.DiplomaLevelEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.DiplomaLevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DiplomaLevelServiceImpl extends AbstractParameterServiceImpl<DiplomaLevelEntity> {

  @Autowired
  protected DiplomaLevelServiceImpl(DiplomaLevelRepository diplomaLevelRepository) {
    super(diplomaLevelRepository);
  }
}
