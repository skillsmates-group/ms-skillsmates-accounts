package com.yamogroup.skillsmates.accounts.services.impl;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.posts.InteractionEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.posts.InteractionTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.NetworkTypeEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.interaction.InteractionTypeEnum;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.posts.InteractionRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.posts.InteractionTypeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.SearchParam;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResource;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.CountNetworkResponse;
import com.yamogroup.skillsmates.accounts.services.NetworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.yamogroup.skillsmates.accounts.dao.specification.AccountSpecification.networkFollowersAccountsMatch;
import static com.yamogroup.skillsmates.accounts.dao.specification.AccountSpecification.networkSuggestionsAccountsMatch;

@Service
@Transactional
public class NetworkServiceImpl extends AbstractAccountServiceImpl implements NetworkService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkServiceImpl.class);

    private final InteractionTypeRepository interactionTypeRepository;
    private final InteractionRepository interactionRepository;

    @Autowired
    protected NetworkServiceImpl(
            AccountRepository accountRepository, CryptUtil cryptUtil, EmailServiceHttp emailServiceHttp, InteractionTypeRepository interactionTypeRepository, InteractionRepository interactionRepository) {
        super(accountRepository, cryptUtil, emailServiceHttp);
        this.interactionTypeRepository = interactionTypeRepository;
        this.interactionRepository = interactionRepository;
    }

    @Override
    public Page<AccountEntity> findSuggestions(String accountId) {
        return accountRepository.findSuggestions(accountId, Pager.of());
    }

    @Override
    public Page<AccountEntity> findFollowers(String accountId) {
        return accountRepository.findFollowers(accountId, Pager.of());
    }

    @Override
    public Page<AccountEntity> findFollowees(String accountId) {
        return accountRepository.findFollowees(accountId, Pager.of());
    }

    @Override
    public Page<AccountEntity> findAroundYou(String accountId, Pageable pageable) {
        AccountEntity accountEntity = getAccountEntity(accountId);
        List<String> uuids = new ArrayList<>();
        uuids.add(accountEntity.getUuid());
        return accountRepository.findByActiveTrueAndDeletedFalseAndUuidNotIn(uuids, pageable);
    }

    @Override
    public AccountDetailsResponse searchNetworkAccounts(SearchParam searchParam, Pageable pageable) {
        AccountEntity accountEntity = getAccountEntity(searchParam.account());
        NetworkTypeEnum networkType = NetworkTypeEnum.getNetworkType(searchParam.networkType());
        Page<InteractionEntity> interactionEntities;
        Page<AccountEntity> accountEntities = null;
        List<Long> ids = new ArrayList<>();
        if (networkType != null) {
            switch (networkType) {
                case FOLLOWERS -> {
//                    accountEntities = findFollowers(searchParam, pageable);
                    accountEntities = accountRepository.findFollowers(searchParam.account(), pageable);
                }
                case SUGGESTIONS -> {
//                    List<String> accountFollowers = new ArrayList<>();
//                    findFollowers(searchParam, pageable).forEach(account -> accountFollowers.add(account.getUuid()));
//                    accountEntities = accountRepository.findByActiveTrueAndDeletedFalseAndAvatarNotNullAndUuidNotIn(accountFollowers, pageable);
//                    accountEntities = accountRepository.findSuggestions(searchParam.account(), pageable);
//                    accountEntities = findSuggestions(searchParam, pageable);
                    accountEntities = accountRepository.findSuggestions(searchParam.account(), pageable);
                }
                case FOLLOWEES -> {
//                    InteractionTypeEntity interactionType = interactionTypeRepository.findByDeletedFalseAndCode(InteractionTypeEnum.FOLLOWER.getCode()).orElseThrow();
//                    interactionEntities = interactionRepository.findAllByActiveTrueAndDeletedFalseAndInteractionTypeAndEmitter(interactionType, accountEntity, pageable);
//                    if (interactionEntities != null && !interactionEntities.isEmpty()) {
//                        interactionEntities.forEach(entity -> ids.add(entity.getReceiver().getId()));
//                    }
//                    accountEntities = accountRepository.findAll(networkFollowersAccountsMatch(searchParam, ids), pageable);
                    accountEntities = accountRepository.findFollowees(searchParam.account(), pageable);
                }
                case FAVORITES -> {
                    InteractionTypeEntity interactionType = interactionTypeRepository.findByDeletedFalseAndCode(InteractionTypeEnum.FAVORITE.getCode()).orElseThrow();
                    interactionEntities = interactionRepository.findAllByActiveTrueAndDeletedFalseAndInteractionTypeAndEmitter(interactionType, accountEntity, pageable);
                    if (interactionEntities != null && !interactionEntities.isEmpty()) {
                        interactionEntities.forEach(entity -> ids.add(entity.getReceiver().getId()));
                    }
                    accountEntities = accountRepository.findAll(networkFollowersAccountsMatch(searchParam, ids), pageable);
                }
                case ONLINE -> {
                    accountEntities = accountRepository.findAllByActiveTrueAndDeletedFalseAndConnectedAtAfter(ZonedDateTime.now().minusMinutes(10), Pager.of());
                }
            }
        }

        AccountDetailsResponse response = new AccountDetailsResponse(accountEntities);
        response.getResources().forEach(account -> addProfileMetadata(account, searchParam.account()));
        return response;
    }

    @Override
    public CountNetworkResponse countNetworkAccounts(SearchParam searchParam) {
        CountNetworkResponse countNetworkResponse = new CountNetworkResponse();
        countNetworkResponse.setSuggestions(accountRepository.countSuggestions(searchParam.account()));
        countNetworkResponse.setFollowers(accountRepository.countFollowers(searchParam.account()));
        countNetworkResponse.setFollowees(accountRepository.countFollowees(searchParam.account()));
        countNetworkResponse.setOnlines(accountRepository.countAllByActiveTrueAndDeletedFalseAndConnectedAtAfter(ZonedDateTime.now().minusMinutes(10)));
        return countNetworkResponse;
    }

    private Page<AccountEntity> findFollowers(SearchParam searchParam, Pageable pageable) {
        List<Long> ids = new ArrayList<>();
        Page<InteractionEntity> interactionEntities;
        InteractionTypeEntity interactionType = interactionTypeRepository.findByDeletedFalseAndCode(InteractionTypeEnum.FOLLOWER.getCode()).orElseThrow();
        interactionEntities = interactionRepository.findAllByActiveTrueAndDeletedFalseAndInteractionTypeAndEntity(interactionType, searchParam.account(), pageable);
        if (interactionEntities != null && !interactionEntities.isEmpty()) {
            interactionEntities.forEach(entity -> ids.add(entity.getEmitter().getId()));
        }
        return accountRepository.findAll(networkFollowersAccountsMatch(searchParam, ids), pageable);
    }

    private Page<AccountEntity> findSuggestions(SearchParam searchParam, Pageable pageable) {
        List<Long> ids = new ArrayList<>();
        Optional<AccountEntity> accountEntity = accountRepository.findByUuid(searchParam.account());
        if (accountEntity.isPresent()) {
            ids.add(accountEntity.get().getId());
        }
        Page<InteractionEntity> interactionEntities;
        InteractionTypeEntity interactionType = interactionTypeRepository.findByDeletedFalseAndCode(InteractionTypeEnum.FOLLOWER.getCode()).orElseThrow();
        interactionEntities = interactionRepository.findAllByActiveTrueAndDeletedFalseAndInteractionTypeAndEntity(interactionType, searchParam.account(), pageable);
        if (interactionEntities != null && !interactionEntities.isEmpty()) {
            interactionEntities.forEach(entity -> ids.add(entity.getEmitter().getId()));
        }
        return accountRepository.findAll(networkSuggestionsAccountsMatch(searchParam, ids), pageable);
    }

    protected InteractionTypeEntity getInteractionType(String code) {
        Optional<InteractionTypeEntity> interactionType = interactionTypeRepository.findByDeletedFalseAndCode(code);
        if (interactionType.isEmpty()) {
            LOGGER.error("This interaction type does not exist: {}", code);
            throw new NotFoundException("Cet interaction type n'existe pas");
        }
        return interactionType.get();
    }

    protected void addProfileMetadata(AccountDetailsResource resource, String loggedAccount) {
        String accountId = resource.getUuid();
        resource.setAvatar(
                resource.getAvatar() != null ? mediaPath + accountId + "/" + resource.getAvatar() : null);
        resource.setPosts(accountRepository.countPostsByAccount(accountId));
        resource.setFollowers(accountRepository.countFollowersByAccount(accountId));
        resource.setFollowees(accountRepository.countFolloweesByAccount(accountId));
        resource.setSuggestions(accountRepository.countSuggestionsByAccount(accountId));
        resource.setMessages(0L);
//        resource.setNotifications(interactionRepository.countAllByActiveTrueAndDeletedFalseAndReceiver(accountId));
//        resource.setNetwork(
//                interactionRepository.countInteractionsByReceiverAndInteractionType(
//                        accountId, InteractionTypeEnum.FOLLOWER.getCode()));
//        long followedInteractions =
//                interactionRepository.countInteractionsByEmitterAndReceiverAndInteractionType(getAccountEntity(loggedAccount).getId(), getAccountEntity(resource.getUuid()).getId(), getInteractionType(InteractionTypeEnum.FOLLOWER.getCode()).getId());

        long followedInteractions =
                interactionRepository.countAllByActiveTrueAndDeletedFalseAndEmitterAndReceiverAndInteractionType(getAccountEntity(loggedAccount), getAccountEntity(resource.getUuid()), getInteractionType(InteractionTypeEnum.FOLLOWER.getCode()));
        resource.setFollowed(followedInteractions > 0);

//        long favoriteInteractions =
//                interactionRepository.countInteractionsByEmitterAndReceiverAndInteractionType(getAccountEntity(loggedAccount).getId(), getAccountEntity(resource.getUuid()).getId(), getInteractionType(InteractionTypeEnum.FAVORITE.getCode()).getId());
        resource.setFavorite(false);
    }
}
