package com.yamogroup.skillsmates.accounts.services.impl;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.http.dto.EmailKeyValue;
import com.yamogroup.skillsmates.accounts.http.dto.EmailResponse;
import com.yamogroup.skillsmates.accounts.http.dto.MessageResponse;
import com.yamogroup.skillsmates.accounts.services.TemplateKeysEnum;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public abstract class AbstractAccountServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractAccountServiceImpl.class);

    public static final String ACCOUNT_TEMPLATE = "account.ftl";
    protected final AccountRepository accountRepository;
    protected final EmailServiceHttp emailServiceHttp;
    protected final CryptUtil cryptUtil;

    @Value("${skillsmates.trial.duration}")
    protected long trialDuration;
    @Value("${skillsmates.media.path}")
    protected String mediaPath;

    @Value("${ms.skillsmates.hostname}")
    protected String hostname;

    @Value("${ms.skillsmates.activate.account.link}")
    protected String activateAccountLink;

    @Value("${ms.skillsmates.password.reset.init}")
    protected String initPasswordResetAccountLink;

    @Autowired
    protected AbstractAccountServiceImpl(
            AccountRepository accountRepository, CryptUtil cryptUtil, EmailServiceHttp emailServiceHttp) {
        this.accountRepository = accountRepository;
        this.cryptUtil = cryptUtil;
        this.emailServiceHttp = emailServiceHttp;
    }

    protected AccountEntity getAccountEntity(String uuid) {
        Optional<AccountEntity> accountEntity = accountRepository.findByUuid(uuid);
        if (accountEntity.isEmpty()) {
            LOGGER.error("This account does not exist: {}", uuid);
            throw new NotFoundException("Ce compte n'existe pas");
        }
        return accountEntity.get();
    }

    protected AccountEntity getActiveAccountEntity(String uuid) {
        Optional<AccountEntity> accountEntity = accountRepository.findByActiveTrueAndUuid(uuid);
        if (accountEntity.isEmpty()) {
            LOGGER.error("This account does not exist: {}", uuid);
            throw new NotFoundException("Ce compte n'existe pas");
        }
        return accountEntity.get();
    }

    protected void sendEmailToActivateAccount(AccountEntity account)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        sendEmail(
                account,
                generateValuesForAccountActivation(account),
                "Création de compte",
                ACCOUNT_TEMPLATE);
    }

    protected boolean doesTrialPeriodExpire(AccountEntity account) {
        if (account.isActive()) {
            return false;
        } else {
            return trialDuration < Math.abs(ChronoUnit.DAYS.between(account.getCreatedAt(), ZonedDateTime.now()));
        }
    }

    protected void sendValidationEmail(AccountEntity account) {

        List<EmailKeyValue> values = new ArrayList<>();
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.RECEIVER_USERNAME.name(),
                        account.getFirstname() + " " + account.getLastname()));
        values.add(new EmailKeyValue(TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(), hostname));
        values.add(new EmailKeyValue(TemplateKeysEnum.EMAIL_TITLE.name(), "Activation compte"));
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.EMAIL_MESSAGE.name(), "Bienvenue " + account.getFirstname()));
        values.add(new EmailKeyValue(TemplateKeysEnum.CTA_END_TEXT.name(), "pour afficher le message"));

        sendEmail(account, values, "Activation compte", ACCOUNT_TEMPLATE);
    }

    protected void sendInitPasswordResetEmail(AccountEntity account)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        List<EmailKeyValue> values = new ArrayList<>();
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.RECEIVER_USERNAME.name(),
                        account.getFirstname() + " " + account.getLastname()));
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(),
                        initPasswordResetAccountLink + cryptUtil.secureCrypt(account.getUuid())));
        values.add(
                new EmailKeyValue(TemplateKeysEnum.EMAIL_TITLE.name(), "Réinitialisation de mot de passe"));
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.EMAIL_MESSAGE.name(), "Bienvenue " + account.getFirstname()));
        values.add(new EmailKeyValue(TemplateKeysEnum.CTA_END_TEXT.name(), "pour afficher le message"));

        sendEmail(account, values, "Réinitialisation de mot de passe", ACCOUNT_TEMPLATE);
    }

    protected void sendPasswordResetEmail(AccountEntity account)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        List<EmailKeyValue> values = new ArrayList<>();
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.RECEIVER_USERNAME.name(),
                        account.getFirstname() + " " + account.getLastname()));
        values.add(new EmailKeyValue(TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(), hostname));
        values.add(new EmailKeyValue(TemplateKeysEnum.EMAIL_TITLE.name(), "mot de passe modifié"));
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.EMAIL_MESSAGE.name(), "Bienvenue " + account.getFirstname()));
        values.add(new EmailKeyValue(TemplateKeysEnum.CTA_END_TEXT.name(), "pour afficher le message"));

        sendEmail(account, values, "mot de passe modifié", ACCOUNT_TEMPLATE);
    }

    private void sendEmail(
            AccountEntity account, List<EmailKeyValue> values, String subject, String template) {
        EmailResponse emailResponse = new EmailResponse();

        emailResponse.setTemplate(template);

        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setFrom(account.getEmail());
        messageResponse.setTo(Collections.singletonList(account.getEmail()));
        messageResponse.setSubject(subject);
        emailResponse.setMessage(messageResponse);
        emailResponse.setValues(values);
        emailServiceHttp.sendEmail(emailResponse);
    }

    private List<EmailKeyValue> generateValuesForAccountActivation(AccountEntity account)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        String cryptedId = cryptUtil.secureCrypt(account.getUuid());

        List<EmailKeyValue> values = new ArrayList<>();
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.RECEIVER_USERNAME.name(),
                        account.getFirstname() + " " + account.getLastname()));
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.RECEIVER_DISPLAY_LINK.name(), activateAccountLink + cryptedId));

        values.add(new EmailKeyValue(TemplateKeysEnum.EMAIL_TITLE.name(), "Création de compte"));
        values.add(
                new EmailKeyValue(
                        TemplateKeysEnum.EMAIL_MESSAGE.name(), "Bienvenue " + account.getFirstname()));
        values.add(new EmailKeyValue(TemplateKeysEnum.CTA_END_TEXT.name(), "pour afficher le message"));

        return values;
    }
}
