package com.yamogroup.skillsmates.accounts.services;

import com.yamogroup.skillsmates.accounts.dao.entities.attributes.AttributeEntity;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import org.springframework.data.domain.Page;

public interface AttributeService<E extends AttributeEntity> {
  /**
   * save entity
   *
   * @param entity entity
   * @return entity
   */
  E save(E entity) throws BadRequestException;

  /**
   * update entity
   *
   * @param entity entity
   * @return entity
   */
  E update(String id, E entity) throws BadRequestException;

  /**
   * delete a entity
   *
   * @param id id
   * @return entity
   */
  E delete(String id);

  /**
   * find Certification by id
   *
   * @param id uuid
   * @return entity
   */
  E find(String id);

  /**
   * find dtos by account
   *
   * @param accountId account id
   * @return list of entities
   */
  Page<E> findByAccount(String accountId);
}
