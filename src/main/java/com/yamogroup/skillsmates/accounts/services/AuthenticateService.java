package com.yamogroup.skillsmates.accounts.services;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ThirdPartyException;

import java.security.NoSuchAlgorithmException;

public interface AuthenticateService {

  AccountEntity authenticate(AccountEntity entity)
      throws BadRequestException, NoSuchAlgorithmException;

  AccountEntity deleteAccount(AccountEntity entity);

  AccountEntity deactivateAccount(AccountEntity entity);

  String activateAccount(String entity) throws BadRequestException, ThirdPartyException;

  AccountEntity activateAccount(String accountId, String email);

  AccountEntity logoutAccount(AccountEntity entity);
}
