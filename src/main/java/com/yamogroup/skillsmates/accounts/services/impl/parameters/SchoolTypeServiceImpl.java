package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.SchoolTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SchoolTypeServiceImpl extends AbstractParameterServiceImpl<SchoolTypeEntity> {

  @Autowired
  protected SchoolTypeServiceImpl(SchoolTypeRepository schoolTypeRepository) {
    super(schoolTypeRepository);
  }
}
