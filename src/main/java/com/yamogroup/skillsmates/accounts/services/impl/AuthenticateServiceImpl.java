package com.yamogroup.skillsmates.accounts.services.impl;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.posts.InteractionEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.posts.InteractionTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.posts.InteractionRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.posts.InteractionTypeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.*;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.helpers.StringHelper;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.services.AuthenticateService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Optional;

@Service
@Transactional
public class AuthenticateServiceImpl extends AbstractAccountServiceImpl
        implements AuthenticateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticateServiceImpl.class);

    private final InteractionTypeRepository interactionTypeRepository;
    private final InteractionRepository interactionRepository;

    @Autowired
    protected AuthenticateServiceImpl(
            AccountRepository accountRepository,
            CryptUtil cryptUtil,
            EmailServiceHttp emailServiceHttp,
            InteractionTypeRepository interactionTypeRepository,
            InteractionRepository interactionRepository) {
        super(accountRepository, cryptUtil, emailServiceHttp);
        this.interactionTypeRepository = interactionTypeRepository;
        this.interactionRepository = interactionRepository;
    }

    @Override
    public AccountEntity authenticate(AccountEntity entity)
            throws BadRequestException, NoSuchAlgorithmException {
        Optional<AccountEntity> accountEntity = accountRepository.findByEmail(entity.getEmail());
        if (accountEntity.isEmpty()) {
            LOGGER.error("No account found with these credentials: {}", ObjectHelper.toString(entity));
            throw new NotFoundException("Aucun compte avec ses identifiants. Veuillez en creer un");
        }

        if (!accountEntity.get().isActive()) {
            LOGGER.error("Your account is not active: {}", ObjectHelper.toString(accountEntity.get()));
            throw new UnauthorizedException(
                    "Votre compte n'est pas encore activé. Veuillez consulter vos emails");
        }

//        if (doesTrialPeriodExpire(accountEntity.get())) {
//            LOGGER.error("Your trial period has expired: {}", ObjectHelper.toString(accountEntity.get()));
//            throw new UnauthorizedException(
//                    "Votre période d'essai est expiré. Un mail de validation de votre compte vous a été envoyé, veuillez consulter vos emails ou spams");
//        }

        if (!StringUtils.equals(
                accountEntity.get().getPassword(),
                StringHelper.generateSecurePassword(entity.getPassword(), entity.getEmail()))) {
            LOGGER.error("Your credentials are not correct: {}", ObjectHelper.toString(entity));
            throw new BadRequestException(
                    "Votre email/mot de passe n'est pas valide", ExceptionTypes.BAD_REQUEST);
        }

        accountEntity.get().setConnectedAt(ZonedDateTime.now());
        return accountRepository.save(accountEntity.get());
    }

    @Override
    public AccountEntity deleteAccount(AccountEntity entity) {
        return null;
    }

    @Override
    public AccountEntity deactivateAccount(AccountEntity entity) {
        return null;
    }

    @Override
    public String activateAccount(String accountId) throws BadRequestException, ThirdPartyException {
        try {
            String decryptedAccountId = cryptUtil.decrypt(accountId);
            AccountEntity accountEntity = getAccountEntity(decryptedAccountId);

            if (accountEntity.isActive()) {
                LOGGER.error("Your account is already active: {}", accountEntity.isActive());
                throw new BadRequestException("Votre compte a déjà été activé. Veuillez vous connecter");
            }

            accountEntity.setActive(true);
            accountRepository.save(accountEntity);

            // make this account to follow skills-mates account
            makeSkillsmatesAccountFollowsThisAccount(accountEntity);

            sendValidationEmail(accountEntity);
            return hostname + "/confirm/activated";
        } catch (NoSuchPaddingException
                 | NoSuchAlgorithmException
                 | InvalidKeyException
                 | BadPaddingException
                 | IllegalBlockSizeException e) {
            throw new ThirdPartyException(e.getMessage());
        }
    }

    @Override
    public AccountEntity activateAccount(String accountId, String email) {
        return null;
    }

    @Override
    public AccountEntity logoutAccount(AccountEntity entity) {
        return null;
    }

    void makeSkillsmatesAccountFollowsThisAccount(AccountEntity account) {
        Optional<InteractionTypeEntity> interactionTypeEntity =
                interactionTypeRepository.findByDeletedFalseAndCode("INTERACTION_TYPE_FOLLOWER");
        interactionTypeEntity.ifPresent(
                type -> {
                    InteractionEntity interactionEntity = new InteractionEntity();
                    interactionEntity.setInteractionType(type);
                    interactionEntity.setEmitter(getAccountEntity("4349844837027990"));
                    interactionEntity.setReceiver(account);
                    interactionEntity.setEntity(account.getUuid());
                    interactionRepository.save(interactionEntity);
                });
    }
}
