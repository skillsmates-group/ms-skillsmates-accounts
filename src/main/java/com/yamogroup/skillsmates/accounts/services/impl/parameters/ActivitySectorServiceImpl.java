package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivitySectorEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.ActivitySectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ActivitySectorServiceImpl extends AbstractParameterServiceImpl<ActivitySectorEntity> {

  @Autowired
  protected ActivitySectorServiceImpl(ActivitySectorRepository activitySectorRepository) {
    super(activitySectorRepository);
  }
}
