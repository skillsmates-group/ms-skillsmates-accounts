package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.SkillEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.AttributeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ExceptionTypes;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Slf4j
@Service
@Transactional
public class SkillServiceImpl extends AbstractAttributeServiceImpl<SkillEntity> {

    @Autowired
    protected SkillServiceImpl(
            AccountRepository accountRepository,
            CryptUtil cryptUtil,
            EmailServiceHttp emailServiceHttp,
            AttributeRepository<SkillEntity> attributeRepository) {
        super(accountRepository, cryptUtil, emailServiceHttp, attributeRepository);
    }

    @Override
    public SkillEntity save(SkillEntity skillEntity) throws BadRequestException {
        log.info("saving skill: {}", ObjectHelper.toString(skillEntity));
        validateSkill(skillEntity);
        skillEntity.setAccount(getAccountEntity(skillEntity.getAccount().getUuid()));
        return attributeRepository.save(skillEntity);
    }

    @Override
    public SkillEntity update(String skillId, SkillEntity entity) throws BadRequestException {
        log.info("updating skill: {}", ObjectHelper.toString(entity));
        validateSkill(entity);
        SkillEntity skillEntity = findEntity(skillId);
        if (!StringUtils.equals(entity.getAccount().getUuid(), skillEntity.getAccount().getUuid())) {
            log.error(
                    "This skill does not belong to this account: {}  - {}",
                    entity.getAccount(),
                    skillEntity.getAccount());
            throw new ForbiddenException("Ce compte n'a pas les droit de modifier cette competence");
        }

        skillEntity.setDiscipline(entity.getDiscipline());
        skillEntity.setKeywords(entity.getKeywords());
        skillEntity.setTitle(entity.getTitle());
        skillEntity.setDescription(entity.getDescription());
        skillEntity.setCategory(entity.getCategory());
        skillEntity.setLevel(entity.getLevel());
        return attributeRepository.save(skillEntity);
    }

    @Override
    public SkillEntity delete(String skillId) {
        log.info("deleting skill: {}", skillId);
        SkillEntity skillEntity = findEntity(skillId);
        skillEntity.setDeleted(true);
        return attributeRepository.save(skillEntity);
    }

    @Override
    public SkillEntity find(String skillId) {
        log.info("finding diploma: {}", skillId);
        return findEntity(skillId);
    }

    @Override
    public Page<SkillEntity> findByAccount(String accountId) {
        log.info("getting diplomas for account: {}", accountId);
        return attributeRepository
                .findByDeletedFalseAndAccount(getAccountEntity(accountId), Pager.of())
                .orElse(new PageImpl<>(Collections.emptyList()));
    }

    /**
     * validate skill
     *
     * @param skillEntity dto
     * @throws BadRequestException if skill is not valid
     */
    private void validateSkill(SkillEntity skillEntity) throws BadRequestException {
        if (skillEntity.getLevel() == null || skillEntity.getCategory() == null) {
            log.info("Skill or Category can not be identified: {}", ObjectHelper.toString(skillEntity));
            throw new BadRequestException(
                    "Le niveau de competence ou la categorie sont introuvables", ExceptionTypes.BAD_REQUEST);
        }
    }
}
