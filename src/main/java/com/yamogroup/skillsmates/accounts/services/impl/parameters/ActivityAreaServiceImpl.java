package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.ActivityAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ActivityAreaServiceImpl extends AbstractParameterServiceImpl<ActivityAreaEntity> {

  @Autowired
  protected ActivityAreaServiceImpl(ActivityAreaRepository activityAreaRepository) {
    super(activityAreaRepository);
  }
}
