package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.JobEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.AttributeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.ActivityAreaServiceImpl;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.ActivitySectorServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
@Transactional
public class JobServiceImpl extends AbstractAttributeServiceImpl<JobEntity> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobServiceImpl.class);

    private final ActivityAreaServiceImpl activityAreaService;
    private final ActivitySectorServiceImpl activitySectorService;

    protected JobServiceImpl(
            AccountRepository accountRepository,
            CryptUtil cryptUtil,
            EmailServiceHttp emailServiceHttp,
            AttributeRepository<JobEntity> attributeRepository,
            ActivityAreaServiceImpl activityAreaService,
            ActivitySectorServiceImpl activitySectorService) {
        super(accountRepository, cryptUtil, emailServiceHttp, attributeRepository);
        this.activityAreaService = activityAreaService;
        this.activitySectorService = activitySectorService;
    }

    @Override
    public JobEntity save(JobEntity jobEntity) {
        LOGGER.info("saving job: {}", ObjectHelper.toString(jobEntity));
        if (jobEntity.getActivityArea() != null) {
            jobEntity.setActivityArea(activityAreaService.findOrCreate(jobEntity.getActivityArea()));
        }
        if (jobEntity.getActivitySector() != null) {
            jobEntity.setActivitySector(
                    activitySectorService.findOrCreate(jobEntity.getActivitySector()));
        }
        jobEntity.setAccount(getAccountEntity(jobEntity.getAccount().getUuid()));
        return attributeRepository.save(jobEntity);
    }

    @Override
    public JobEntity update(String jobId, JobEntity entity) throws BadRequestException {
        LOGGER.info("updating job: {}", ObjectHelper.toString(entity));
        JobEntity jobEntity = findEntity(jobId);
        if (!StringUtils.equals(entity.getAccount().getUuid(), jobEntity.getAccount().getUuid())) {
            LOGGER.error(
                    "This job does not belong to this account: {}  - {}",
                    entity.getAccount().getUuid(),
                    jobEntity.getAccount().getUuid());
            throw new ForbiddenException(
                    "Ce compte n'a pas les droit de modifier cette experience profesionnelle");
        }
        jobEntity.setTitle(entity.getTitle());
        if (entity.getActivityArea() != null) {
            jobEntity.setActivityArea(activityAreaService.findOrCreate(entity.getActivityArea()));
        }
        if (entity.getActivitySector() != null) {
            jobEntity.setActivitySector(activitySectorService.findOrCreate(entity.getActivitySector()));
        }
        jobEntity.setCompany(entity.getCompany());
        jobEntity.setStartDate(entity.getStartDate());
        jobEntity.setEndDate(entity.getEndDate());
        jobEntity.setCity(entity.getCity());
        jobEntity.setDescription(entity.getDescription());
        jobEntity.setCurrentJob(entity.isCurrentJob());
        return attributeRepository.save(jobEntity);
    }

    @Override
    public JobEntity delete(String jobId) {
        LOGGER.info("deleting job: {}", jobId);
        JobEntity jobEntity = findEntity(jobId);
        jobEntity.setDeleted(true);
        return attributeRepository.save(jobEntity);
    }

    @Override
    public JobEntity find(String jobId) {
        LOGGER.info("finding job: {}", jobId);
        return findEntity(jobId);
    }

    @Override
    public Page<JobEntity> findByAccount(String accountId) {
        LOGGER.info("finding jobs by account: {}", accountId);
        return attributeRepository
                .findByDeletedFalseAndAccount(getAccountEntity(accountId), Pager.of())
                .orElse(new PageImpl<>(Collections.emptyList()));
    }
}
