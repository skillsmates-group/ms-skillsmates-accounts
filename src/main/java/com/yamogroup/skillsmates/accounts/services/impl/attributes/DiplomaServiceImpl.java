package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.DiplomaEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.AttributeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.ActivityAreaServiceImpl;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.DiplomaLevelServiceImpl;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.SchoolTypeServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.UUID;

@Service
@Transactional
public class DiplomaServiceImpl extends AbstractAttributeServiceImpl<DiplomaEntity> {

  private static final Logger LOGGER = LoggerFactory.getLogger(DiplomaServiceImpl.class);

  private final SchoolTypeServiceImpl schoolTypeService;
  private final DiplomaLevelServiceImpl diplomaLevelService;
  private final ActivityAreaServiceImpl activityAreaService;

  @Autowired
  protected DiplomaServiceImpl(
      AccountRepository accountRepository,
      CryptUtil cryptUtil,
      EmailServiceHttp emailServiceHttp,
      AttributeRepository<DiplomaEntity> attributeRepository,
      SchoolTypeServiceImpl schoolTypeService,
      DiplomaLevelServiceImpl diplomaLevelService,
      ActivityAreaServiceImpl activityAreaService) {
    super(accountRepository, cryptUtil, emailServiceHttp, attributeRepository);
    this.schoolTypeService = schoolTypeService;
    this.diplomaLevelService = diplomaLevelService;
    this.activityAreaService = activityAreaService;
  }

  @Override
  public DiplomaEntity save(DiplomaEntity diplomaEntity) {
    LOGGER.info("saving diploma: {}", ObjectHelper.toString(diplomaEntity));
    if (diplomaEntity.getSchoolType() != null) {
      diplomaEntity.setSchoolType(schoolTypeService.findOrCreate(diplomaEntity.getSchoolType()));
    }
    if (diplomaEntity.getLevel() != null) {
      diplomaEntity.setLevel(diplomaLevelService.findOrCreate(diplomaEntity.getLevel()));
    }
    if (diplomaEntity.getActivityArea() != null) {
      diplomaEntity.setActivityArea(
          activityAreaService.findOrCreate(diplomaEntity.getActivityArea()));
    }
    diplomaEntity.setAccount(getAccountEntity(diplomaEntity.getAccount().getUuid()));
    diplomaEntity.setUuid(UUID.randomUUID().toString());
    return attributeRepository.save(diplomaEntity);
  }

  @Override
  public DiplomaEntity update(String id, DiplomaEntity entity) throws BadRequestException {
    LOGGER.info("updating diploma: {}", ObjectHelper.toString(entity));
    DiplomaEntity diplomaEntity = findEntity(id);
    if (!StringUtils.equals(entity.getAccount().getUuid(), diplomaEntity.getAccount().getUuid())) {
      LOGGER.error(
          "This diploma does not belong to this account: {}  - {}",
          entity.getAccount().getUuid(),
          diplomaEntity.getAccount().getUuid());
      throw new ForbiddenException("Ce compte n'a pas les droit de modifier ce diplome");
    }
    diplomaEntity.setTitle(entity.getTitle());
    diplomaEntity.setEducation(entity.getEducation());
    if (entity.getLevel() != null) {
      diplomaEntity.setLevel(diplomaLevelService.findOrCreate(entity.getLevel()));
    }
    if (entity.getSchoolType() != null) {
      diplomaEntity.setSchoolType(schoolTypeService.findOrCreate(entity.getSchoolType()));
    }
    if (entity.getActivityArea() != null) {
      diplomaEntity.setActivityArea(activityAreaService.findOrCreate(entity.getActivityArea()));
    }
    diplomaEntity.setSchoolName(entity.getSchoolName());
    diplomaEntity.setCity(entity.getCity());
    diplomaEntity.setStartDate(entity.getStartDate());
    diplomaEntity.setEndDate(entity.getEndDate());
    diplomaEntity.setDescription(entity.getDescription());
    diplomaEntity.setEducation(entity.getEducation());
    return attributeRepository.save(diplomaEntity);
  }

  @Override
  public DiplomaEntity delete(String id) {
    LOGGER.info("deleting diploma: {}", id);
    DiplomaEntity diplomaEntity = findEntity(id);
    diplomaEntity.setDeleted(true);
    return attributeRepository.save(diplomaEntity);
  }

  @Override
  public DiplomaEntity find(String id) {
    LOGGER.info("finding diploma: {}", id);
    return this.findEntity(id);
  }

  @Override
  public Page<DiplomaEntity> findByAccount(String accountId) {
    LOGGER.info("getting diplomas for account: {}", accountId);
    return attributeRepository
        .findByDeletedFalseAndAccount(getAccountEntity(accountId), Pager.of())
        .orElse(new PageImpl<>(Collections.emptyList()));
  }
}
