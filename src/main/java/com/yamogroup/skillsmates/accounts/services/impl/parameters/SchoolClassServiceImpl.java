package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolClassEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.SchoolClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SchoolClassServiceImpl extends AbstractParameterServiceImpl<SchoolClassEntity> {

  @Autowired
  protected SchoolClassServiceImpl(SchoolClassRepository schoolClassRepository) {
    super(schoolClassRepository);
  }
}
