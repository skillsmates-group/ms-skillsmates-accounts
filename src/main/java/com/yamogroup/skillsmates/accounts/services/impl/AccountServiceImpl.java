package com.yamogroup.skillsmates.accounts.services.impl;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.posts.InteractionTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.NetworkTypeEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.interaction.InteractionTypeEnum;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.*;
import com.yamogroup.skillsmates.accounts.dao.repositories.posts.InteractionRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.posts.InteractionTypeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.*;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.helpers.StringHelper;
import com.yamogroup.skillsmates.accounts.http.DocumentServiceHttp;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.http.dto.requests.DocumentCreationRequest;
import com.yamogroup.skillsmates.accounts.http.dto.responses.DocumentCreateResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.PasswordEdit;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.SearchParam;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.SortDirection;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.*;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.*;
import com.yamogroup.skillsmates.accounts.services.AccountService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.yamogroup.skillsmates.accounts.dao.specification.AccountSpecification.accountMatch;

@Service
public class AccountServiceImpl extends AbstractAccountServiceImpl implements AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    private final SkillRepository skillRepository;
    private final TrainingRepository trainingRepository;
    private final DiplomaRepository diplomaRepository;
    private final JobRepository jobRepository;
    private final CertificationRepository certificationRepository;
    private final InteractionRepository interactionRepository;
    private final DocumentServiceHttp documentServiceHttp;
    private final InteractionTypeRepository interactionTypeRepository;

    @Autowired
    public AccountServiceImpl(
            AccountRepository accountRepository,
            EmailServiceHttp emailServiceHttp,
            CryptUtil cryptUtil,
            SkillRepository skillRepository,
            TrainingRepository trainingRepository,
            DiplomaRepository diplomaRepository,
            JobRepository jobRepository,
            CertificationRepository certificationRepository,
            InteractionRepository interactionRepository,
            DocumentServiceHttp documentServiceHttp,
            InteractionTypeRepository interactionTypeRepository) {
        super(accountRepository, cryptUtil, emailServiceHttp);
        this.skillRepository = skillRepository;
        this.trainingRepository = trainingRepository;
        this.diplomaRepository = diplomaRepository;
        this.jobRepository = jobRepository;
        this.certificationRepository = certificationRepository;
        this.interactionRepository = interactionRepository;
        this.documentServiceHttp = documentServiceHttp;
        this.interactionTypeRepository = interactionTypeRepository;
    }

    @Override
    @Transactional
    public AccountEntity create(AccountEntity entity)
            throws AlreadyExistsException, NoSuchAlgorithmException, ThirdPartyException {
        LOGGER.info("START SERVICE CREATE ENTITY with entity {}", entity);

        Optional<AccountEntity> accountExistsEntity = accountRepository.findByEmail(entity.getEmail());
        if (accountExistsEntity.isPresent() && accountExistsEntity.get().isActive()) {
            LOGGER.error("This account already exists: {}", ObjectHelper.toString(accountExistsEntity));
            throw new AlreadyExistsException("Un compte avec cet adresse email existe deja");
        }

        if (accountExistsEntity.isPresent() && !accountExistsEntity.get().isActive()) {
            LOGGER.error(
                    "An inactive account already exists, it will be resumed: {}",
                    ObjectHelper.toString(accountExistsEntity));
            throw new AlreadyExistsException("Un compte avec cet adresse email est inactif");
        }

        entity.setLastname(entity.getLastname().toUpperCase());
        entity.setFirstname(StringHelper.capitalizeFirstCharacter(entity.getFirstname()));
        entity.setPassword(
                StringHelper.generateSecurePassword(entity.getPassword(), entity.getEmail()));
        entity = accountRepository.save(entity);

        try {
            sendEmailToActivateAccount(entity);
        } catch (Exception e) {
            // if email cannot be sent, delete account
//      accountRepository.delete(entity);
            LOGGER.info("Error while sending email: {}", ObjectHelper.toString(entity));
            throw new ThirdPartyException(
                    "Une erreur est survenue lors de l'envoi de l'email d'activation. Veuillez reessayer plus tard");
        }

        return entity;
    }

    @Override
    public AccountDetailsResponse find(String uuid) throws NotFoundException {
        LOGGER.info("START SERVICE SEARCHING ACCOUNT WITH ID {}", uuid);
        AccountEntity accountEntity = getAccountEntity(uuid);
        AccountDetailsResponse accountDetailsResponse = new AccountDetailsResponse(accountEntity);
        // TODO: replace uuid by the loggedAccount
        this.addProfileMetadata(accountDetailsResponse.getResource(), uuid);
        return accountDetailsResponse;
    }

    @Override
    @Deprecated
    public AccountDetailsResponse findSuggestions(String uuid) {
        List<String> ids = new ArrayList<>();
        ids.add(uuid);
        Page<AccountEntity> entities =
                accountRepository.findByActiveTrueAndDeletedFalseAndAvatarNotNullAndUuidNotIn(
                        ids, Pager.of(0, 20));
        AccountDetailsResponse accountDetailsResponse = new AccountDetailsResponse(entities);
        accountDetailsResponse
                .getResources()
                .forEach(resource -> this.addProfileMetadata(resource, uuid));
        Collections.shuffle(accountDetailsResponse.getResources());
        return accountDetailsResponse;
    }

    @Override
    @Deprecated
    public AccountDetailsResponse findOnline() {
        AccountDetailsResponse response =
                new AccountDetailsResponse(
                        new PageImpl<>(accountRepository.findByConnectedTrueAndActiveTrueAndDeletedFalse()));
        // TODO: pass the loggedAccount to the method
        response.getResources().forEach(account -> addProfileMetadata(account, ""));
        return response;
    }

    @Override
    public AccountDetailsResponse loadNetwork(
            NetworkTypeEnum networkTypeEnum,
            String accountId,
            Integer offset,
            Integer limit,
            String status,
            String country,
            String name) {
        LOGGER.info("START SERVICE LOADING NETWORK OF ACCOUNT WITH ID {}", accountId);
        status = status != null ? surroundWithPercentage(status) : surroundWithPercentage("");
        country = country != null ? surroundWithPercentage(country) : surroundWithPercentage("");
        name =
                name != null
                        ? Stream.of(name.split(" "))
                        .distinct()
                        .map(this::surroundWithPercentage)
                        .collect(Collectors.joining(" OR CONCAT(LOWER(firstname), LOWER(lastname)) LIKE "))
                        : surroundWithPercentage("");
        Pageable pageable = offset != null && limit != null ? Pager.of(offset, limit) : Pager.of();
        Page<AccountEntity> accountEntities =
                switch (networkTypeEnum) {
                    case FOLLOWEES -> accountRepository.findFollowees(
                            accountId, status, country, name, pageable);
                    case FOLLOWERS -> accountRepository.findFollowers(
                            accountId, status, country, name, pageable);
                    case SUGGESTIONS -> accountRepository.findSuggestions(
                            accountId, status, country, name, pageable);
                    // TODO: around you case not yet implemented
                    case AROUND_YOU -> null;
                    // TODO: favourite case not yet implemented
                    case FAVORITES -> null;
                    case ONLINE -> accountRepository
                            .findByConnectedTrueAndActiveTrueAndDeletedFalseAndUuidNotAndStatusLikeAndCountryLike(
                                    accountId, status, country, pageable);
                };
        AccountDetailsResponse response = new AccountDetailsResponse(accountEntities);
        response.getResources().forEach(account -> addProfileMetadata(account, accountId));
        return response;
    }

    @Override
    public AccountDetailsResponse updateBiography(String accountId, AccountEntity accountEntity) {
        LOGGER.info(
                "START SERVICE UPDATE BIOGRAPHY OF ACCOUNT with id {} AND BODY {}",
                accountId,
                accountEntity);
        if (!accountId.equals(accountEntity.getUuid())) {
            throw new ForbiddenException("Ce compte n'a pas les droits de modification");
        }
        AccountEntity account = getAccountEntity(accountId);
        account.setBiography(accountEntity.getBiography());
        accountRepository.save(account);
        AccountDetailsResponse accountDetailsResponse = new AccountDetailsResponse(account);
        // TODO: replace accountId by  the loggedAccount
        this.addProfileMetadata(accountDetailsResponse.getResource(), accountId);
        return accountDetailsResponse;
    }

    @Override
    public AccountDetailsResponse update(String accountId, AccountEntity accountEntity) {
        LOGGER.info(
                "START SERVICE UPDATE BIOGRAPHY ACCOUNT with id {} AND BODY {}", accountId, accountEntity);
        if (!accountId.equals(accountEntity.getUuid())) {
            throw new ForbiddenException("Ce compte n'a pas les droits de modification");
        }
        AccountEntity account = getAccountEntity(accountId);
        account.setAddress(accountEntity.getAddress());
        account.setCountry(accountEntity.getCountry());
        account.setCity(accountEntity.getCity());
        account.setStatus(accountEntity.getStatus());
        account.setBirthdate(accountEntity.getBirthdate());
        account.setPhoneNumber(accountEntity.getPhoneNumber());
        account.setGender(accountEntity.getGender());
        account.setHideBirthdate(accountEntity.isHideBirthdate());
        account.setFirstname(accountEntity.getFirstname());
        account.setLastname(accountEntity.getLastname());
        accountRepository.save(account);
        AccountDetailsResponse accountDetailsResponse = new AccountDetailsResponse(account);
        // TODO: replace accountId by  the loggedAccount
        this.addProfileMetadata(accountDetailsResponse.getResource(), accountId);
        return accountDetailsResponse;
    }

    @Override
    public AccountDeactivateResource deactivateAccount(String accountId) {
        LOGGER.info("START SERVICE DEACTIVATE ACCOUNT WITH ID {} ", accountId);
        AccountEntity accountEntity = getAccountEntity(accountId);
        accountEntity.setActive(false);
        accountEntity = accountRepository.save(accountEntity);
        return new AccountDeactivateResource(accountEntity);
    }

    @Override
    public AccountDeleteResponse deleteAccount(String accountId) {
        LOGGER.info("START SERVICE DELETE ACCOUNT WITH ID {} ", accountId);
        AccountEntity accountEntity = getAccountEntity(accountId);
        accountEntity.setDeleted(true);
        accountEntity = accountRepository.save(accountEntity);
        return new AccountDeleteResponse(accountEntity);
    }

    @Override
    public AccountAttributesResponse findAccountAttributes(String accountId) {
        LOGGER.info("START SERVICE SEARCH ACCOUNT ATTRIBUTE WITH ID {} ", accountId);
        AccountAttributesResource resource = new AccountAttributesResource();

        resource.setSkills(
                skillRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(accountId).stream()
                        .map(SkillResource::new)
                        .collect(Collectors.toList()));
        resource.setTotalSkills(resource.getSkills().size());

        resource.setTrainings(
                trainingRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(accountId).stream()
                        .map(TrainingResource::new)
                        .collect(Collectors.toList()));
        resource.setTotalTrainings(resource.getTrainings().size());

        resource.setDiplomas(
                diplomaRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(accountId).stream()
                        .map(DiplomaResource::new)
                        .collect(Collectors.toList()));
        resource.setTotalDiplomas(resource.getDiplomas().size());

        resource.setJobs(
                jobRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(accountId).stream()
                        .map(JobResource::new)
                        .collect(Collectors.toList()));
        resource.setTotalJobs(resource.getJobs().size());

        resource.setCertifications(
                certificationRepository
                        .findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(accountId)
                        .stream()
                        .map(CertificationResource::new)
                        .collect(Collectors.toList()));
        resource.setTotalCertifications(resource.getCertifications().size());

        return new AccountAttributesResponse(resource);
    }

    @Override
    public AccountDetailsResponse searchAccount(SearchParam param, Integer offset, Integer limit) {
        LOGGER.info("START SERVICE SEARCH ACCOUNT");
        Sort sort = Sort.by("createdAt");
        sort = param.direction() == SortDirection.ASC ? sort.descending() : sort.ascending();
        Page<AccountEntity> accountEntities =
                accountRepository.findAll(accountMatch(param), Pager.of(offset, limit, sort));
        AccountDetailsResponse response = new AccountDetailsResponse(accountEntities);
        response.getResources().forEach(account -> addProfileMetadata(account, param.account()));
        return response;
    }

    @Override
    public AccountStatusMetricResponse findAccountStatusMetric() {
        LOGGER.info("START SERVICE FIND ACCOUNT STATUS METRIC ");
        AccountStatusMetricResponse response = new AccountStatusMetricResponse();
        for (StatusEnum statusEnum : StatusEnum.values()) {
            AccountStatusMetricResource resource = new AccountStatusMetricResource();
            resource.setLabel(statusEnum.name());
            resource.setCode(statusEnum.name());
            resource.setTotal(accountRepository.countAccountsByStatus(statusEnum));
            response.getResources().add(resource);
        }

        return response;
    }

    @Override
    public AccountDetailsResponse initPasswordReset(String email)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        LOGGER.info("START SERVICE INIT PASSWORD OF ACCOUNT WITH EMAIL  {} ", email);
        AccountEntity accountEntity =
                accountRepository
                        .findByEmail(email)
                        .orElseThrow(
                                () -> new NotFoundException("Un compte avec cet adresse email n'existe pas"));
        sendInitPasswordResetEmail(accountEntity);
        return new AccountDetailsResponse(accountEntity);
    }

    @Override
    public AccountDetailsResponse resetPassword(String password, String token)
            throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException,
            BadPaddingException, InvalidKeyException, UnsupportedEncodingException,
            BadRequestException {
        LOGGER.info("START SERVICE RESET PASSWORD ");
        String uuid = cryptUtil.decrypt(token);
        AccountEntity accountEntity =
                accountRepository
                        .findByUuid(uuid)
                        .orElseThrow(() -> new NotFoundException("Un compte avec cet identifiant non trouvé"));
        accountEntity.setPassword(
                StringHelper.generateSecurePassword(password, accountEntity.getEmail()));
        accountRepository.save(accountEntity);
        sendPasswordResetEmail(accountEntity);
        return new AccountDetailsResponse(accountEntity);
    }

    @Override
    public AccountEntity saveAvatar(String accountId, MultipartFile file) throws ThirdPartyException {
        LOGGER.info("START SERVICE SAVE AVATAR OF ACCOUNT WITH ID {} ", accountId);
        AccountEntity accountEntity = getAccountEntity(accountId);
        DocumentCreationRequest request = new DocumentCreationRequest();
        try {
            request.setMediaType(null);
            request.setOrigin("ORIGIN_AVATAR");
            request.setAccount(accountEntity.getUuid());
            request.setUrl(null);
            request.setDocument(null);

            ResponseEntity<DocumentCreateResponse> responseEntity =
                    documentServiceHttp.saveDocument(request, file);
            if (responseEntity.getStatusCode().isError()) {
                LOGGER.error("Error occurred while saving document: {}", ObjectHelper.toString(request));
                throw new ThirdPartyException("Erreur lors de la sauvegarde du fichier");
            }
            DocumentCreateResponse response = responseEntity.getBody();
            if (response != null
                    && response.getResource() != null
                    && StringUtils.isNotBlank(response.getResource().getLink())) {
                accountEntity.setAvatar(response.getResource().getLink());
            }
            return accountRepository.save(accountEntity);
        } catch (Exception e) {
            LOGGER.error("Error occurred while saving document: {}", ObjectHelper.toString(request));
            throw new ThirdPartyException("Erreur lors de la sauvegarde du fichier");
        }
    }

    @Override
    public AccountDetailsResponse changePassword(String accountUuid, PasswordEdit passwordEditRequest)
            throws BadRequestException, NoSuchAlgorithmException {
        LOGGER.info("START SERVICE CHANGE PASSWORD OF ACCOUNT WITH ID {} ", accountUuid);
        if (!accountUuid.equals(passwordEditRequest.uuid())) {
            throw new ForbiddenException("Ce compte n'a pas les droits de modification");
        }
        if (!passwordEditRequest.confirmPasswordIsCorrect()) {
            throw new BadRequestException(
                    "le nouveau mot de passe et la confirmation sont différents", ExceptionTypes.BAD_REQUEST);
        }
        Optional<AccountEntity> optionalAccountEntity = accountRepository.findByUuid(accountUuid);
        if (optionalAccountEntity.isEmpty()) {
            throw new NotFoundException("Compte innexistant");
        }
        AccountEntity account = optionalAccountEntity.get();
        String cryptedOldPassword =
                StringHelper.generateSecurePassword(passwordEditRequest.oldPassword(), account.getEmail());
        if (!cryptedOldPassword.equals(account.getPassword())) {
            throw new ForbiddenException("L'ancien mot de passe est invalide");
        }
        account.setPassword(
                StringHelper.generateSecurePassword(passwordEditRequest.newPassword(), account.getEmail()));
        account = accountRepository.save(account);
        AccountDetailsResponse accountDetailsResponse = new AccountDetailsResponse(account);
        // TODO: replace accountUuid by loggedAccount
        this.addProfileMetadata(accountDetailsResponse.getResource(), accountUuid);
        return accountDetailsResponse;
    }

    private String surroundWithPercentage(String text) {
        String percentage = "%";
        return percentage.concat(text.trim().toLowerCase()).concat(percentage);
    }

    private void addProfileMetadata(AccountDetailsResource resource, String loggedAccount) {
        String accountId = resource.getUuid();
        resource.setAvatar(
                resource.getAvatar() != null ? mediaPath + accountId + "/" + resource.getAvatar() : null);
        resource.setPosts(accountRepository.countPostsByAccount(accountId));
        resource.setFollowers(accountRepository.countFollowersByAccount(accountId));
        resource.setFollowees(accountRepository.countFolloweesByAccount(accountId));
        resource.setSuggestions(accountRepository.countSuggestionsByAccount(accountId));
        resource.setMessages(0L);
        resource.setNotifications(
                interactionRepository.countAllByActiveTrueAndDeletedFalseAndReceiver(
                        getAccountEntity(accountId)));
        resource.setNetwork(
                interactionRepository.countAllByActiveTrueAndDeletedFalseAndReceiverAndInteractionType(
                        getAccountEntity(accountId),
                        getInteractionType(InteractionTypeEnum.FOLLOWER.getCode())));
        //        long followedInteractions =
        // interactionRepository.countInteractionsByEntityAndEmitterAndInteractionType(resource.getUuid(), loggedAccount, InteractionTypeEnum.FOLLOWER.getCode());
        //        resource.setFollowed(followedInteractions > 0);
        //        long favoriteInteractions =
        // interactionRepository.countInteractionsByEntityAndEmitterAndInteractionType(resource.getUuid(), loggedAccount, InteractionTypeEnum.FAVORITE.getCode());
        //        resource.setFollowed(favoriteInteractions > 0);
    }

    private InteractionTypeEntity getInteractionType(String code) {
        Optional<InteractionTypeEntity> interactionTypeEntity =
                interactionTypeRepository.findByDeletedFalseAndCode(code);
        if (interactionTypeEntity.isEmpty()) {
            LOGGER.error("This interaction does not exist: {}", code);
            throw new NotFoundException("This interaction does not exist");
        }
        return interactionTypeEntity.get();
    }
}
