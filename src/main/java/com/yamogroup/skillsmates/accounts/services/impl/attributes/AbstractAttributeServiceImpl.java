package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.AttributeEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.AttributeRepository;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.services.AttributeService;
import com.yamogroup.skillsmates.accounts.services.impl.AbstractAccountServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public abstract class AbstractAttributeServiceImpl<E extends AttributeEntity>
        extends AbstractAccountServiceImpl implements AttributeService<E> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractAttributeServiceImpl.class);

    protected final AttributeRepository<E> attributeRepository;

    @Autowired
    protected AbstractAttributeServiceImpl(
            AccountRepository accountRepository,
            CryptUtil cryptUtil,
            EmailServiceHttp emailServiceHttp,
            AttributeRepository<E> attributeRepository) {
        super(accountRepository, cryptUtil, emailServiceHttp);
        this.attributeRepository = attributeRepository;
    }

    public E findEntity(String id) {
        Optional<E> entity = attributeRepository.findByDeletedFalseAndUuid(id);
        if (entity.isEmpty()) {
            LOGGER.error("This entity does not exist: {}", id);
            throw new NotFoundException("Cette entité n'existe pas");
        }
        return entity.get();
    }
}
