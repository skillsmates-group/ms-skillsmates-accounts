package com.yamogroup.skillsmates.accounts.services;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ParameterEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ParameterService<E extends ParameterEntity> {

  /**
   * find or create entity, if entity is found, return it, otherwise create and return it
   *
   * @param entity entity
   * @return entity
   */
  E findOrCreate(E entity);

  /**
   * find all parameters
   *
   * @return list of parameters
   */
  Page<E> findAll();

  Page<E> findAll(Pageable pageable);
}
