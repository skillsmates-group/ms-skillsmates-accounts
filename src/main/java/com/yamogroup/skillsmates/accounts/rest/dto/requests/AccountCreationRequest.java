package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum;
import lombok.Builder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public record AccountCreationRequest (
    @NotBlank(message = "firstname is required")
    @JsonProperty(value = "firstname", required = true)
    String firstname,
    @NotBlank(message = "lastname is required")
    @JsonProperty(value = "lastname", required = true)
    String lastname,
    @Email(message = "must be a correct email address")
    @NotBlank(message = "email is required")
    @JsonProperty(value = "email", required = true)
    String email,
    @JsonProperty(value = "password")
    String password,
    @JsonProperty(value = "confirmPassword")
    String confirmPassword,
    @JsonProperty("status")
    StatusEnum status
){
    public AccountEntity toEntity(){
        AccountEntity entity = new AccountEntity();
        entity.setFirstname(this.firstname);
        entity.setLastname(this.lastname);
        entity.setEmail(this.email);
        entity.setPassword(this.password);
        entity.setStatus(this.status);
        return entity;
    }
}
