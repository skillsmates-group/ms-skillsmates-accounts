package com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.DiplomaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.DiplomaLevelEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.parameters.ParameterRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record DiplomaRequest(
        @NotBlank(message = "title is required")
        @JsonProperty(value = "title", required = true)
        String title,

        @JsonProperty(value = "description")
        String description,

        @NotBlank(message = "account is required")
        @JsonProperty(value = "account", required = true)
        String account,

        @JsonProperty(value = "education", required = true)
        EducationEnum education,

        @JsonProperty(value = "level")
        ParameterRequest level,

        @Valid
        @JsonProperty(value = "schoolType")
        ParameterRequest schoolType,

        @Valid
        @JsonProperty(value = "activityArea")
        ParameterRequest activityArea,

        @NotBlank(message = "schoolName is required")
        @JsonProperty(value = "schoolName", required = true)
        String schoolName,

        @JsonProperty(value = "city")
        String city,

        @JsonFormat(pattern = "yyyy-MM-dd")
        @JsonProperty(value = "startDate")
        Date startDate,

        @JsonFormat(pattern = "yyyy-MM-dd")
        @JsonProperty(value = "endDate")
        Date endDate
) {
    public DiplomaEntity toEntity() {
        DiplomaEntity entity = new DiplomaEntity();
        entity.setTitle(this.title);
        entity.setDescription(this.description);
        entity.setAccount(toAccountEntity());
        entity.setEducation(this.education);
        entity.setLevel(toDiplomaLevelEntity());
        entity.setSchoolType(toSchoolTypeEntity());
        entity.setActivityArea(toActivityAreaEntity());
        entity.setSchoolName(this.schoolName);
        entity.setCity(this.city);
        entity.setStartDate(this.startDate);
        entity.setEndDate(this.endDate);
        return entity;
    }

    public DiplomaLevelEntity toDiplomaLevelEntity() {
        DiplomaLevelEntity entity = null;
        if (this.level != null) {
            entity = new DiplomaLevelEntity();
            entity.setCode(this.level.code());
            entity.setLabel(this.level.label());
        }
        return entity;
    }

    public SchoolTypeEntity toSchoolTypeEntity() {
        SchoolTypeEntity entity = null;
        if (this.schoolType != null) {
            entity = new SchoolTypeEntity();
            entity.setCode(this.schoolType.code());
            entity.setLabel(this.schoolType.label());
        }
        return entity;
    }

    public ActivityAreaEntity toActivityAreaEntity() {
        ActivityAreaEntity entity = null;
        if (this.activityArea != null) {
            entity = new ActivityAreaEntity();
            entity.setCode(this.activityArea.code());
            entity.setLabel(this.activityArea.label());
        }
        return entity;
    }

    public AccountEntity toAccountEntity() {
        AccountEntity entity = new AccountEntity();
        entity.setUuid(this.account);
        return entity;
    }
}
