package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.JobEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.parameters.ParameterResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobResource extends AttributeResource {
    @JsonProperty(value = "activitySector")
    private ParameterResponse activitySector;

    @JsonProperty(value = "activityArea")
    private ParameterResponse activityArea;

    @JsonProperty(value = "company")
    private String company;

    @JsonProperty(value = "startDate")
    private Date startDate;

    @JsonProperty(value = "endDate")
    private Date endDate;

    @JsonProperty(value = "city")
    private String city;

    @JsonProperty(value = "currentJob")
    private boolean currentJob;

    public JobResource(JobEntity entity) {
        super(entity);
        if (entity.getActivitySector() != null) {
            this.activitySector = new ParameterResponse(entity.getActivitySector());
        }
        if (entity.getActivityArea() != null) {
            this.activityArea = new ParameterResponse(entity.getActivityArea());
        }
        this.company = entity.getCompany();
        this.startDate = entity.getStartDate();
        this.endDate = entity.getEndDate();
        this.city = entity.getCity();
        this.currentJob = entity.isCurrentJob();
    }
}
