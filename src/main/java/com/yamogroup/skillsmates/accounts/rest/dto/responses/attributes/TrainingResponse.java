package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.TrainingEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainingResponse extends AttributeResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private TrainingResource resource;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<TrainingResource> resources;

    public TrainingResponse(TrainingEntity entity) {
        super(AttributeEnum.TRAINING.getLabel());
        this.resource = new TrainingResource(entity);
    }

    public TrainingResponse(MetaResponse metaResponse, TrainingEntity entity) {
        super(AttributeEnum.TRAINING.getLabel(), metaResponse);
        this.resource = new TrainingResource(entity);
    }

    public TrainingResponse(Page<TrainingEntity> entities) {
        super(
                AttributeEnum.TRAINING.getLabel(),
                MetaResponse.builder()
                        .hasPrevious(entities.hasPrevious())
                        .hasNext(entities.hasNext())
                        .totalPages(entities.getTotalPages())
                        .total((int) entities.getTotalElements())
                        .build());
        this.resources = new ArrayList<>();
        entities.forEach(entity -> resources.add(new TrainingResource(entity)));
    }
}
