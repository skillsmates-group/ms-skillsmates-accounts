package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.helpers.Constants;
import com.yamogroup.skillsmates.accounts.rest.api.ParameterApi;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.parameters.ParameterResponse;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ParameterController implements ParameterApi {

  private static final Logger LOGGER = LoggerFactory.getLogger(ParameterController.class);

  private final ActivityAreaServiceImpl activityAreaService;
  private final ActivitySectorServiceImpl activitySectorService;
  private final DiplomaLevelServiceImpl diplomaLevelService;
  private final SchoolClassServiceImpl schoolClassService;
  private final SchoolTypeServiceImpl schoolTypeService;
  private final TeachingFieldServiceImpl teachingFieldService;
  private final TeachingNameServiceImpl teachingNameService;
  private final CountryServiceImpl countryService;

  @Override
  public ResponseEntity<ParameterResponse> findActivityAreas() {
    return ResponseEntity.ok(new ParameterResponse(activityAreaService.findAll()));
  }

  @Override
  public ResponseEntity<ParameterResponse> findActivitySectors() {
    return ResponseEntity.ok(new ParameterResponse(activitySectorService.findAll()));
  }

  @Override
  public ResponseEntity<ParameterResponse> findDiplomaLevels() {
    return ResponseEntity.ok(new ParameterResponse(diplomaLevelService.findAll()));
  }

  @Override
  public ResponseEntity<ParameterResponse> findSchoolClasses() {
    return ResponseEntity.ok(new ParameterResponse(schoolClassService.findAll()));
  }

  @Override
  public ResponseEntity<ParameterResponse> findSchoolTypes() {
    return ResponseEntity.ok(new ParameterResponse(schoolTypeService.findAll()));
  }

  @Override
  public ResponseEntity<ParameterResponse> findTeachingFields() {
    return ResponseEntity.ok(new ParameterResponse(teachingFieldService.findAll()));
  }

  @Override
  public ResponseEntity<ParameterResponse> findTeachingNames() {
    return ResponseEntity.ok(new ParameterResponse(teachingNameService.findAll()));
  }

  @Override
  public ResponseEntity<ParameterResponse> findCountries() {
    Pageable pageable =
        PageRequest.of(
            Constants.PAGE_DEFAULT_OFFSET, Constants.PAGE_MAX_LIMIT, Sort.by("id").descending());
    return ResponseEntity.ok(new ParameterResponse(countryService.findAll(pageable)));
  }
}
