package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.api.DiplomaApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.DiplomaRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.DiplomaResponse;
import com.yamogroup.skillsmates.accounts.services.impl.attributes.DiplomaServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class DiplomaController implements DiplomaApi {

    private final DiplomaServiceImpl diplomaService;

    @Override
    public ResponseEntity<DiplomaResponse> createDiploma(@Valid @RequestBody DiplomaRequest dto) {
        return ResponseEntity.ok(new DiplomaResponse(diplomaService.save(dto.toEntity())));
    }

    @Override
    public ResponseEntity<DiplomaResponse> findDiploma(@NotBlank @PathVariable String id) {
        return ResponseEntity.ok(new DiplomaResponse(diplomaService.find(id)));
    }

    @Override
    public ResponseEntity<DiplomaResponse> findDiplomaByAccount(@NotBlank @PathVariable String accountId) {
        return ResponseEntity.ok(new DiplomaResponse(diplomaService.findByAccount(accountId)));
    }

    @Override
    public ResponseEntity<DiplomaResponse> updateDiploma(
            @NotBlank @PathVariable String id, @Valid @RequestBody DiplomaRequest dto)
            throws BadRequestException {
        return ResponseEntity.ok(new DiplomaResponse(diplomaService.update(id, dto.toEntity())));
    }

    @Override
    public ResponseEntity<DiplomaResponse> deleteDiploma(@NotBlank @PathVariable String id) {
        return ResponseEntity.ok(new DiplomaResponse(diplomaService.delete(id)));
    }
}
