package com.yamogroup.skillsmates.accounts.rest.dto.responses.parameters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ParameterEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.BaseResponse;

public class ParameterResource extends BaseResponse {
    @JsonProperty(value = "label")
    String label;

    @JsonProperty(value = "code")
    String code;

    public ParameterResource(ParameterEntity entity) {
        super(entity);
        this.label = entity.getLabel();
        this.code = entity.getCode();
    }
}
