package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import lombok.Builder;

import javax.validation.constraints.NotBlank;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record AccountUpdateBiographyRequest(
    @NotBlank(message = "uuid is required")
    @JsonProperty(value = "uuid", required = true)
    String uuid,
    @JsonProperty(value = "biography")
    String biography
){
    public AccountEntity toEntity(){
        AccountEntity entity = new AccountEntity();
        entity.setUuid(this.uuid);
        entity.setBiography(this.biography);
        return entity;
    }
}
