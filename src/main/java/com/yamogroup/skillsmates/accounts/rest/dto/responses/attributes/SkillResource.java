package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.SkillEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.skill.CategoryEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.skill.SkillLevelEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SkillResource extends AttributeResource {
    @JsonProperty(value = "category")
    private CategoryEnum category;

    @JsonProperty(value = "discipline")
    private String discipline;

    @JsonProperty(value = "keywords")
    private String keywords;

    @JsonProperty(value = "level")
    private SkillLevelEnum level;

    public SkillResource(SkillEntity entity) {
        super(entity);
        this.category = entity.getCategory();
        this.discipline = entity.getDiscipline();
        this.keywords = entity.getKeywords();
        this.level = entity.getLevel();
    }
}
