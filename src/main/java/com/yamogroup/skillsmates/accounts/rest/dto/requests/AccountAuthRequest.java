package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import lombok.Builder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record AccountAuthRequest (
    @Email(message = "must be a correct email address")
    @NotBlank(message = "email is required")
    @JsonProperty(value = "email", required = true)
    String email,
    @NotBlank(message = "password is required")
    @JsonProperty(value = "password", required = true)
    String password
){
    public AccountEntity toEntity(){
        AccountEntity entity = new AccountEntity();
        entity.setEmail(this.email);
        entity.setPassword(this.password);
        return entity;
    }
}
