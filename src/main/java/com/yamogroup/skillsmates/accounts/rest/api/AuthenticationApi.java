package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.exceptions.AlreadyExistsException;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ThirdPartyException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.AccountAuthRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.AccountCreationRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.InitPasswordReset;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.PasswordReset;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Tag(name = "Authentication")
@RequestMapping(
        value = "/skillsmates",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface AuthenticationApi {
    @PostMapping(value = "/authenticate")
    ResponseEntity<AccountDetailsResponse> authenticateAccount(
            @Valid @RequestBody AccountAuthRequest request)
            throws BadRequestException, NoSuchAlgorithmException;

    @PostMapping(value = "/auth/authenticate")
    ResponseEntity<AccountDetailsResponse> authAuthenticateAccount(
            @Valid @RequestBody AccountAuthRequest request)
            throws BadRequestException, NoSuchAlgorithmException;

    @PostMapping
    ResponseEntity<AccountCreationResponse> createAuthAccount(
            @Valid @RequestBody AccountCreationRequest request)
            throws BadRequestException, AlreadyExistsException, NoSuchAlgorithmException,
            ThirdPartyException;

    @PutMapping(value = "/accounts/deactivate/{accountId}")
    ResponseEntity<AccountDeactivateResource> deactivateAccount(
            @PathVariable @NotBlank String accountId);

    @GetMapping(value = "/accounts/activate/{accountId}", produces = MediaType.TEXT_HTML_VALUE)
    String activateAccount(@PathVariable @NotBlank String accountId)
            throws BadRequestException, ThirdPartyException;

    @PutMapping(value = "/accounts/activate/{accountId}/{email}")
    ResponseEntity<AccountActivateResponse> activateAccount(
            @PathVariable @NotBlank String accountId, @PathVariable @NotBlank String email);

    @PutMapping(value = "/accounts/logout/{accountId}")
    ResponseEntity<AccountLogoutResponse> logoutAccount(@PathVariable @NotBlank String accountId);

    @PostMapping("password/init-reset")
    ResponseEntity<AccountDetailsResponse> initPasswordReset(
            @Valid @RequestBody InitPasswordReset initPasswordReset)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException;

    @PatchMapping("password/reset")
    ResponseEntity<AccountDetailsResponse> resetPassword(
            @Valid @RequestBody PasswordReset passwordReset)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException;
}
