package com.yamogroup.skillsmates.accounts.rest.dto.responses.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDeactivateResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MetaResponse meta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private AccountDeactivateResource resource;

    public AccountDeactivateResponse(AccountEntity entity) {
        this.resource = new AccountDeactivateResource(entity);
    }
}
