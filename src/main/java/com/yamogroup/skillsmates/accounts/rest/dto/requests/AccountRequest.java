package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.GenderEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.RoleEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record AccountRequest (
    @JsonProperty("active")
    boolean active,
    @NotBlank(message = "firstname is required")
    @JsonProperty(value = "firstname", required = true)
    String firstname,
    @NotBlank(message = "lastname is required")
    @JsonProperty(value = "lastname", required = true)
    String lastname,
    @JsonProperty("address")
    String address,
    @Email(message = "must be a correct email address")
    @NotBlank(message = "email is required")
    @JsonProperty(value = "email", required = true)
    String email,
    @JsonProperty(value = "phone_number")
    String phoneNumber,
    @JsonProperty(value = "birthdate")
    Date birthdate,
    @JsonProperty(value = "hideBirthdate")
    boolean hideBirthdate,
    @JsonProperty(value = "password")
    String password,
    @JsonProperty("gender")
    GenderEnum gender,
    @JsonProperty(value = "biography")
    String biography,
    @JsonProperty(value = "city")
    String city,
    @JsonProperty(value="country")
    String country,
    @NotNull(message = "type of status is required")
    @JsonProperty("status")
    StatusEnum status,
    @JsonProperty(value = "connected")
    boolean connected,
    @JsonProperty(value = "connectedAt")
    ZonedDateTime connectedAt,
    @NotNull(message = "type of role is required")
    @JsonProperty("role")
    RoleEnum role,
    @JsonProperty(value="avatar")
    String avatar
){
    public AccountEntity toEntity(){
        AccountEntity entity = new AccountEntity();
        entity.setFirstname(this.firstname);
        entity.setLastname(this.lastname);
        entity.setEmail(this.email);
        entity.setPassword(this.password);
        entity.setStatus(this.status);
        entity.setGender(this.gender);
//        entity.setBirthdate(this.birthdate);
        entity.setHideBirthdate(this.hideBirthdate);
        entity.setCity(this.city);
        entity.setCountry(this.country);
        entity.setPhoneNumber(this.phoneNumber);
        entity.setAddress(this.address);
        return entity;
    }
}
