package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.api.CertificationApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.CertificationRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.CertificationResponse;
import com.yamogroup.skillsmates.accounts.services.impl.attributes.CertificationServiceImpl;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class CertificationController implements CertificationApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(CertificationController.class);

    private final CertificationServiceImpl certificationService;

    @Override
    public ResponseEntity<CertificationResponse> createCertification(CertificationRequest requestDto) {
        return ResponseEntity.ok(
                new CertificationResponse(certificationService.save(requestDto.toEntity())));
    }

    @Override
    public ResponseEntity<CertificationResponse> findCertification(String id) {
        return ResponseEntity.ok(new CertificationResponse(certificationService.find(id)));
    }

    @Override
    public ResponseEntity<CertificationResponse> findCertificationByAccount(String accountId) {
        return ResponseEntity.ok(
                new CertificationResponse(certificationService.findByAccount(accountId)));
    }

    @Override
    public ResponseEntity<CertificationResponse> updateCertification(String id, CertificationRequest certificationDto)
            throws BadRequestException {
        return ResponseEntity.ok(
                new CertificationResponse(
                        certificationService.update(id, certificationDto.toEntity())));
    }

    @Override
    public ResponseEntity<CertificationResponse> deleteCertification(String id) {
        return ResponseEntity.ok(
                new CertificationResponse(certificationService.delete(id)));
    }
}
