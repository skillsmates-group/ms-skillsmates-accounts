package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.helpers.Pager;
import com.yamogroup.skillsmates.accounts.rest.api.NetworkApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.SearchParam;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.SortDirection;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.CountNetworkResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.NetworkResponse;
import com.yamogroup.skillsmates.accounts.services.NetworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class NetworkController implements NetworkApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkController.class);

    private final NetworkService networkService;

    @Autowired
    public NetworkController(NetworkService networkService) {
        this.networkService = networkService;
    }

    @Override
    public ResponseEntity<NetworkResponse> findSuggestions(String accountId) {
        return ResponseEntity.ok(new NetworkResponse(networkService.findSuggestions(accountId)));
    }

    @Override
    public ResponseEntity<NetworkResponse> findFollowers(String accountId) {
        return ResponseEntity.ok(new NetworkResponse(networkService.findFollowers(accountId)));
    }

    @Override
    public ResponseEntity<NetworkResponse> findFollowees(String accountId) {
        return ResponseEntity.ok(new NetworkResponse(networkService.findFollowees(accountId)));
    }

    @Override
    public ResponseEntity<NetworkResponse> findAroundYou(
            String accountId, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id").descending());
        return ResponseEntity.ok(
                new NetworkResponse(networkService.findAroundYou(accountId, pageable)));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> searchNetworkAccounts(
            SearchParam searchParam, Integer offset, Integer limit) {
        Sort sort = Sort.by("created_at");
        sort = searchParam.direction() == SortDirection.ASC ? sort.descending() : sort.ascending();
        Pageable pageable = Pager.of(offset, limit, sort);
//        networkService.searchNetworkAccounts(searchParam, pageable);
        return ResponseEntity.ok(networkService.searchNetworkAccounts(searchParam, pageable));
    }

    @Override
    public ResponseEntity<CountNetworkResponse> countNetworkAccounts(SearchParam searchParam) {
        return ResponseEntity.ok(networkService.countNetworkAccounts(searchParam));
    }
}
