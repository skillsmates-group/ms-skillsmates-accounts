package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.dao.enums.account.NetworkTypeEnum;
import com.yamogroup.skillsmates.accounts.exceptions.AlreadyExistsException;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ExceptionTypes;
import com.yamogroup.skillsmates.accounts.exceptions.ThirdPartyException;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.rest.api.AccountApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.*;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountCreationResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDeleteResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountStatusMetricResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.AccountAttributesResponse;
import com.yamogroup.skillsmates.accounts.services.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class AccountController implements AccountApi {

    private final AccountService accountService;

    @Override
    public ResponseEntity<AccountCreationResponse> createAccount(AccountCreationRequest request) throws BadRequestException, AlreadyExistsException, NoSuchAlgorithmException, ThirdPartyException {
        checkPassword(request);

        return new ResponseEntity<>(
                new AccountCreationResponse(accountService.create(request.toEntity())), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> findAccount(String accountId) {
        return ResponseEntity.ok(accountService.find(accountId));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> findAccountSuggestions(String accountId) {
        return ResponseEntity.ok(accountService.findSuggestions(accountId));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> findAccountOnline() {
        return ResponseEntity.ok(accountService.findOnline());
    }

    @Override
    public ResponseEntity<AccountAttributesResponse> findAccountAttributes(String accountId) {
        return ResponseEntity.ok(accountService.findAccountAttributes(accountId));
    }

    @Override
    public ResponseEntity<AccountDeleteResponse> deleteAccount(String accountId) {
        return ResponseEntity.ok(accountService.deleteAccount(accountId));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> loadNetwork(
            NetworkTypeEnum networkTypeEnum,
            String accountId,
            Integer offset,
            Integer limit,
            String status,
            String country,
            String name) {
        return ResponseEntity.ok(
                accountService.loadNetwork(
                        networkTypeEnum, accountId, offset, limit, status, country, name));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> updateBiography(
            String accountId, AccountUpdateBiographyRequest request) {
        return ResponseEntity.ok(accountService.updateBiography(accountId, request.toEntity()));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> updateAccount(
            String accountId, AccountUpdateDetailsRequest request) {
        return ResponseEntity.ok(accountService.update(accountId, request.toEntity()));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> searchAccount(
            SearchParam param, Integer offset, Integer limit) {
        return ResponseEntity.ok(accountService.searchAccount(param, offset, limit));
    }

    @Override
    public ResponseEntity<AccountStatusMetricResponse> findAccountStatusMetric() {
        return ResponseEntity.ok(accountService.findAccountStatusMetric());
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> changePassword(
            PasswordEdit passwordEdit, String accountUuid)
            throws BadRequestException, NoSuchAlgorithmException {
        return ResponseEntity.ok(accountService.changePassword(accountUuid, passwordEdit));
    }

    private void checkPassword(AccountCreationRequest request) throws BadRequestException {
        if (request.password() == null
                || !StringUtils.equals(request.password(), request.confirmPassword())) {
            log.error("Passwords are not the same : {}", ObjectHelper.toString(request));
            throw new BadRequestException("Passwords are not the same", ExceptionTypes.BAD_REQUEST);
        }
    }
}
