package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.dao.enums.account.NetworkTypeEnum;
import com.yamogroup.skillsmates.accounts.exceptions.AlreadyExistsException;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ThirdPartyException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.*;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountCreationResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDeleteResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountStatusMetricResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.AccountAttributesResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.security.NoSuchAlgorithmException;

@Tag(name = "Account")
@RequestMapping(
        value = "/skillsmates/accounts",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface AccountApi {
    @PostMapping
    ResponseEntity<AccountCreationResponse> createAccount(
            @Valid @RequestBody AccountCreationRequest request)
            throws BadRequestException, AlreadyExistsException, NoSuchAlgorithmException,
            ThirdPartyException;

    @GetMapping(value = "{accountId}")
    ResponseEntity<AccountDetailsResponse> findAccount(
            @NotBlank @PathVariable("accountId") String accountId);

    @GetMapping(value = "/suggestions/{accountId}")
    ResponseEntity<AccountDetailsResponse> findAccountSuggestions(
            @NotBlank @PathVariable("accountId") String accountId);

    @GetMapping(value = "/online")
    ResponseEntity<AccountDetailsResponse> findAccountOnline();

    @GetMapping(value = "/attributes/{accountId}")
    ResponseEntity<AccountAttributesResponse> findAccountAttributes(
            @NotBlank @PathVariable("accountId") String accountId);

    @DeleteMapping(value = "/{accountId}")
    ResponseEntity<AccountDeleteResponse> deleteAccount(@PathVariable @NotBlank String accountId);

    @GetMapping(value = "network/{networkCategory}/{accountId}")
    ResponseEntity<AccountDetailsResponse> loadNetwork(
            @PathVariable("networkCategory") NetworkTypeEnum networkTypeEnum,
            @PathVariable("accountId") String accountId,
            @RequestParam(value = "offset") Integer offset,
            @RequestParam(value = "limit") Integer limit,
            @RequestParam(value = "status") String status,
            @RequestParam(value = "country") String country,
            @RequestParam(value = "name") String name);

    @PutMapping(value = "/biography/{accountId}")
    ResponseEntity<AccountDetailsResponse> updateBiography(
            @NotBlank @PathVariable("accountId") String accountId,
            @Valid @RequestBody AccountUpdateBiographyRequest request);

    @PutMapping(value = "/{accountId}")
    ResponseEntity<AccountDetailsResponse> updateAccount(
            @NotBlank @PathVariable("accountId") String accountId,
            @Valid @RequestBody AccountUpdateDetailsRequest request);

    @PostMapping(value = "search")
    ResponseEntity<AccountDetailsResponse> searchAccount(
            @RequestBody SearchParam searchParam,
            @RequestParam(value = "offset") Integer offset,
            @RequestParam(value = "limit") Integer limit);

    @GetMapping(value = "/status/metric")
    ResponseEntity<AccountStatusMetricResponse> findAccountStatusMetric();

    @PutMapping(value = "/password/{accountUuid}")
    ResponseEntity<AccountDetailsResponse> changePassword(
            @Valid @RequestBody PasswordEdit passwordEdit, @NotBlank @PathVariable String accountUuid)
            throws BadRequestException, NoSuchAlgorithmException;
}
