package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.DiplomaRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.DiplomaResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Tag(name = "Diploma")
@RequestMapping(
        value = "/skillsmates/accounts/diplomas",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface DiplomaApi {
    @PostMapping
    ResponseEntity<DiplomaResponse> createDiploma(@Valid @RequestBody DiplomaRequest dto) throws BadRequestException;

    @GetMapping(value = "{id}")
    ResponseEntity<DiplomaResponse> findDiploma(@NotBlank @PathVariable String id);

    @GetMapping(value = "account/{accountId}")
    ResponseEntity<DiplomaResponse> findDiplomaByAccount(@NotBlank @PathVariable String accountId);

    @PutMapping(value = "{id}")
    ResponseEntity<DiplomaResponse> updateDiploma(@NotBlank @PathVariable String id, @Valid @RequestBody DiplomaRequest dto) throws BadRequestException;

    @DeleteMapping(value = "{id}")
    ResponseEntity<DiplomaResponse> deleteDiploma(@NotBlank @PathVariable String id);
}
