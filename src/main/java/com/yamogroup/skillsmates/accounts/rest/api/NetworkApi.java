package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.rest.dto.requests.SearchParam;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.CountNetworkResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.NetworkResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Network")
@RequestMapping(
        value = "/skillsmates/accounts/network",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface NetworkApi {

    @GetMapping(value = "/suggestions/{id}")
    ResponseEntity<NetworkResponse> findSuggestions(@PathVariable("id") String accountId);

    @GetMapping(value = "/followers/{id}")
    ResponseEntity<NetworkResponse> findFollowers(@PathVariable("id") String accountId);

    @GetMapping(value = "/followees/{id}")
    ResponseEntity<NetworkResponse> findFollowees(@PathVariable("id") String accountId);

    @GetMapping(value = "/aroundyou/{id}")
    ResponseEntity<NetworkResponse> findAroundYou(
            @PathVariable("id") String accountId,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size);

    @PostMapping
    ResponseEntity<AccountDetailsResponse> searchNetworkAccounts(
            @RequestBody SearchParam searchParam,
            @RequestParam(value = "offset") Integer offset,
            @RequestParam(value = "limit") Integer limit);

    @PostMapping(value = "/count")
    ResponseEntity<CountNetworkResponse> countNetworkAccounts(@RequestBody SearchParam searchParam);
}
