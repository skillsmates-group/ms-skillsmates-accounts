package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.rest.dto.responses.parameters.ParameterResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Tag(name = "Parameter")
@RequestMapping(
        value = "/skillsmates",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface ParameterApi {

    @GetMapping(value = "activityareas")
    ResponseEntity<ParameterResponse> findActivityAreas();

    @GetMapping(value = "activitysectors")
    ResponseEntity<ParameterResponse> findActivitySectors();

    @GetMapping(value = "diplomalevels")
    ResponseEntity<ParameterResponse> findDiplomaLevels();

    @GetMapping(value = "schoolclasses")
    ResponseEntity<ParameterResponse> findSchoolClasses();

    @GetMapping(value = "schooltypes")
    ResponseEntity<ParameterResponse> findSchoolTypes();

    @GetMapping(value = "teachingfields")
    ResponseEntity<ParameterResponse> findTeachingFields();

    @GetMapping(value = "teachingnames")
    ResponseEntity<ParameterResponse> findTeachingNames();

    @GetMapping(value = "countries")
    ResponseEntity<ParameterResponse> findCountries();
}
