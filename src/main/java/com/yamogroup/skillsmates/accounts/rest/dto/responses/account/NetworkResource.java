package com.yamogroup.skillsmates.accounts.rest.dto.responses.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.GenderEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.RoleEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.BaseResponse;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.Date;

@Setter
@Getter
public class NetworkResource extends BaseResponse {
    @JsonProperty(value = "active")
    boolean active;

    @JsonProperty(value = "firstname")
    String firstname;

    @JsonProperty(value = "lastname")
    String lastname;

    @JsonProperty(value = "email")
    String email;

    @JsonProperty(value = "address")
    String address;

    @JsonProperty(value = "phoneNumber")
    String phoneNumber;

    @JsonProperty(value = "birthdate")
    Date birthdate;

    @JsonProperty(value = "gender")
    GenderEnum gender;

    @JsonProperty(value = "biography")
    String biography;

    @JsonProperty(value = "city")
    String city;

    @JsonProperty(value = "country")
    String country;

    @JsonProperty(value = "status")
    StatusEnum status;

    @JsonProperty(value = "connected")
    boolean connected;

    @JsonProperty(value = "connectedAt")
    ZonedDateTime connectedAt;

    @JsonProperty(value = "role")
    RoleEnum role;

    public NetworkResource(AccountEntity entity) {
        super(entity);
        this.active = entity.isActive();
        this.firstname = entity.getFirstname();
        this.lastname = entity.getLastname();
        this.email = entity.getEmail();
        this.address = entity.getAddress();
        this.phoneNumber = entity.getPhoneNumber();
        this.birthdate = entity.getBirthdate();
        this.gender = entity.getGender();
        this.biography = entity.getBiography();
        this.city = entity.getCity();
        this.country = entity.getCountry();
        this.status = entity.getStatus();
        this.connected = entity.isConnected();
        this.connectedAt = entity.getConnectedAt();
        this.role = entity.getRole();
    }
}
