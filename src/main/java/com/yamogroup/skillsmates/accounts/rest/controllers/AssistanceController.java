package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.rest.api.AssistanceApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.AssistanceCreationRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.AssistanceCreationResponse;
import com.yamogroup.skillsmates.accounts.services.AssistanceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class AssistanceController implements AssistanceApi {

  private final AssistanceService assistanceService;

  @Override
  public ResponseEntity<AssistanceCreationResponse> createAssistance(
      AssistanceCreationRequest request) {
    return new ResponseEntity<>(
        new AssistanceCreationResponse(
            assistanceService.create(request.toEntity(), request.account())),
        HttpStatus.CREATED);
  }
}
