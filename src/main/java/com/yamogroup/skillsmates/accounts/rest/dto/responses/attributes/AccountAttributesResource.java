package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.BaseResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class AccountAttributesResource extends BaseResponse {
    @JsonProperty(value = "skills")
    List<SkillResource> skills;
    @JsonProperty(value = "totalSkills")
    long totalSkills;

    @JsonProperty(value = "diplomas")
    List<DiplomaResource> diplomas;
    @JsonProperty(value = "totalDiplomas")
    long totalDiplomas;

    @JsonProperty(value = "certifications")
    List<CertificationResource> certifications;
    @JsonProperty(value = "totalCertifications")
    long totalCertifications;

    @JsonProperty(value = "jobs")
    List<JobResource> jobs;
    @JsonProperty(value = "totalJobs")
    long totalJobs;

    @JsonProperty(value = "trainings")
    List<TrainingResource> trainings;
    @JsonProperty(value = "totalTrainings")
    long totalTrainings;
}
