package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.DiplomaEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiplomaResponse extends AttributeResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private DiplomaResource resource;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<DiplomaResource> resources;

    public DiplomaResponse(DiplomaEntity entity) {
        super(AttributeEnum.DIPLOMA.getLabel());
        this.resource = new DiplomaResource(entity);
    }

    public DiplomaResponse(MetaResponse metaResponse, DiplomaEntity entity) {
        super(AttributeEnum.DIPLOMA.getLabel(), metaResponse);
        this.resource = new DiplomaResource(entity);
    }

    public DiplomaResponse(Page<DiplomaEntity> entities) {
        super(
                AttributeEnum.DIPLOMA.getLabel(),
                MetaResponse.builder()
                        .hasPrevious(entities.hasPrevious())
                        .hasNext(entities.hasNext())
                        .totalPages(entities.getTotalPages())
                        .total((int) entities.getTotalElements())
                        .build());
        this.resources = new ArrayList<>();
        entities.forEach(entity -> resources.add(new DiplomaResource(entity)));
    }
}
