package com.yamogroup.skillsmates.accounts.rest.dto.responses.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MetaResponse meta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<NetworkResource> resources;

    public NetworkResponse(Page<AccountEntity> entities) {
        this.meta =
                MetaResponse.builder()
                        .hasPrevious(entities.hasPrevious())
                        .hasNext(entities.hasNext())
                        .totalPages(entities.getTotalPages())
                        .total((int) entities.getTotalElements())
                        .build();
        this.resources = new ArrayList<>();
        entities.forEach(entity -> this.resources.add(new NetworkResource(entity)));
    }
}
