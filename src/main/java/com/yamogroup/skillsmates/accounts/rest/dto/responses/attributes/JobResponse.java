package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.JobEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobResponse extends AttributeResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private JobResource resource;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<JobResource> resources;

    public JobResponse(JobEntity entity) {
        super(AttributeEnum.JOB.getLabel());
        this.resource = new JobResource(entity);
    }

    public JobResponse(MetaResponse metaResponse, JobEntity entity) {
        super(AttributeEnum.JOB.getLabel(), metaResponse);
        this.resource = new JobResource(entity);
    }

    public JobResponse(Page<JobEntity> entities) {
        super(
                AttributeEnum.JOB.getLabel(),
                MetaResponse.builder()
                        .hasPrevious(entities.hasPrevious())
                        .hasNext(entities.hasNext())
                        .totalPages(entities.getTotalPages())
                        .total((int) entities.getTotalElements())
                        .build());
        this.resources = new ArrayList<>();
        entities.forEach(entity -> resources.add(new JobResource(entity)));
    }
}
