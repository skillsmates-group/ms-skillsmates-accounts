package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.CertificationEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CertificationResponse extends AttributeResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private CertificationResource resource;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<CertificationResource> resources;

    public CertificationResponse(CertificationEntity entity) {
        super(AttributeEnum.CERTIFICATION.getLabel());
        this.resource = new CertificationResource(entity);
    }

    public CertificationResponse(MetaResponse metaResponse, CertificationEntity entity) {
        super(AttributeEnum.CERTIFICATION.getLabel(), metaResponse);
        this.resource = new CertificationResource(entity);
    }

    public CertificationResponse(Page<CertificationEntity> entities) {
        super(
                AttributeEnum.CERTIFICATION.getLabel(),
                MetaResponse.builder()
                        .hasPrevious(entities.hasPrevious())
                        .hasNext(entities.hasNext())
                        .totalPages(entities.getTotalPages())
                        .total((int) entities.getTotalElements())
                        .build());
        this.resources = new ArrayList<>();
        entities.forEach(entity -> resources.add(new CertificationResource(entity)));
    }
}
