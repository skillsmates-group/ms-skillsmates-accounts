package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.exceptions.AlreadyExistsException;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ExceptionTypes;
import com.yamogroup.skillsmates.accounts.exceptions.ThirdPartyException;
import com.yamogroup.skillsmates.accounts.helpers.ObjectHelper;
import com.yamogroup.skillsmates.accounts.rest.api.AuthenticationApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.AccountAuthRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.AccountCreationRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.InitPasswordReset;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.PasswordReset;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.*;
import com.yamogroup.skillsmates.accounts.services.AccountService;
import com.yamogroup.skillsmates.accounts.services.AuthenticateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class AuthenticationController implements AuthenticationApi {

    private final AuthenticateService authenticateService;
    private final AccountService accountService;

    @Override
    public ResponseEntity<AccountDetailsResponse> authenticateAccount(AccountAuthRequest request)
            throws BadRequestException, NoSuchAlgorithmException {
        AccountEntity accountEntity = authenticateService.authenticate(request.toEntity());
        return ResponseEntity.ok(accountService.find(accountEntity.getUuid()));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> authAuthenticateAccount(AccountAuthRequest request) throws BadRequestException, NoSuchAlgorithmException {
        AccountEntity accountEntity = authenticateService.authenticate(request.toEntity());
        return ResponseEntity.ok(accountService.find(accountEntity.getUuid()));
    }

    @Override
    public ResponseEntity<AccountCreationResponse> createAuthAccount(AccountCreationRequest request)
            throws BadRequestException, AlreadyExistsException, NoSuchAlgorithmException,
            ThirdPartyException {
        checkPassword(request);

        return new ResponseEntity<>(
                new AccountCreationResponse(accountService.create(request.toEntity())), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<AccountDeactivateResource> deactivateAccount(String accountId) {
        return ResponseEntity.ok(accountService.deactivateAccount(accountId));
    }

    @Override
    public String activateAccount(String accountId)
            throws BadRequestException, ThirdPartyException {
        String url = authenticateService.activateAccount(accountId);

        // TODO: to be put un static file
        return """
                   <html >
                     <head>
                       <title>Compte activé</title>
                       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                       <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
                     </head>
                     <body>
                         <div style="background-color: #FFFFFA; margin: auto; ">
                             <div style="height: 30%; width: 30%; max-width:75%; margin-left:15%;">
                                 <div>
                                     <img src="https://skillsmatesresources.s3.us-east-2.amazonaws.com/images/logo-skillsmates.svg" alt="logo skillsmates" style="max-width: 100%; max-height: 100%; margin-bottom: 0px" />
                                 </div>
                             </div>
                   
                             <div>
                                 <div style="font-weight: bold; color: #15162D; margin: auto; font-size: x-large; text-align: center; padding: 20px;">
                                     AMELIORE ET PARTAGE <br/> TES CONNAISSANCES AVEC <br/> LA COMMUNAUTE
                                 </div>
                             </div>
                         </div>
                   
                       <section>
                         <div style="-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%">
                           <div style="display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px; background-color: whitesmoke; height: 50px;">
                             <p style="font-size: x-large; margin: auto; color: #16152D; font-weight: bold;">Compte activé</p>
                           </div>
                   
                           <div style="display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px; background-color: white; height: 100px">
                             <p style="max-width:75%; margin-left:15%; margin-top: auto; margin-bottom: auto; color: #16152D">
                               Bonjour <span></span>  <span></span> <br>
                               Votre compte sur Skills Mates a été activé avec succés <br>
                             </p>
                           </div>
                           <div style="">
                             <p style="max-width:75%; margin-left:15%; margin-top: auto; margin-bottom: auto; color: #16152D">
                               <button type="button"
                                   style="display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;background-color:transparent;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5; margin: auto; background-color: green; border-radius: 5px; cursor: pointer;" >
                                 <a href="http://skills-mates.com" style="text-decoration: none; color: white;">Cliquez ici</a>
                               </button> pour acceder à la plateforme
                             </p>
                           </div>
                   
                           <div style="display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px;
                                     height: auto; margin-top: 10px">
                             <div style=" -ms-flex:0 0 75%;flex:0 0 75%;max-width:75%; margin-left:15%; color: #16152D">
                                <span style="">Si le bouton ci-dessus ne fonctionne pas, copier directement sur ce lien <br>
                                 <span>http://skills-mates.com</span><br><br> Cordialement.
                               </span>
                                 <div style="text-align: center; margin-top: 50px; font-size: small; background-color: #FFFFFA;">
                                     <div>
                                         <span>&copy;2021. Tous droits reservés</span>
                                     </div>
                                     <div>
                                         <span>SkillsMates - 86 Boulevard Jean Jaures 92110 Clichy France</span>
                                     </div>
                                     <div>
                                         <span>Contactez-nous en cas de besoin à l'adresse </span><span>contact@skillsmates.com</span>
                                     </div>
                                 </div>
                             </div>
                           </div>
                         </div>
                       </section>
                     </body>
                   </html>
                """;
    }

    @Override
    public ResponseEntity<AccountActivateResponse> activateAccount(String accountId, String email) {
        return null;
    }

    @Override
    public ResponseEntity<AccountLogoutResponse> logoutAccount(String accountId) {
        return null;
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> initPasswordReset(
            InitPasswordReset initPasswordReset)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return ResponseEntity.ok(accountService.initPasswordReset(initPasswordReset.email()));
    }

    @Override
    public ResponseEntity<AccountDetailsResponse> resetPassword(PasswordReset passwordReset)
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return ResponseEntity.ok(
                accountService.resetPassword(passwordReset.password(), passwordReset.token()));
    }

    private void checkPassword(AccountCreationRequest request) throws BadRequestException {
        if (request.password() == null
                || !StringUtils.equals(request.password(), request.confirmPassword())) {
            log.error("Passwords are not the same : {}", ObjectHelper.toString(request));
            throw new BadRequestException("Passwords are not the same", ExceptionTypes.BAD_REQUEST);
        }
    }
}
