package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public record PasswordEdit(
    @NotBlank(message = "uuid is required") @JsonProperty(value = "uuid", required = true)
        String uuid,
    @NotBlank(message = "oldPassword is required")
        @JsonProperty(value = "oldPassword", required = true)
        String oldPassword,
    @NotBlank(message = "newPassword is required")
        @JsonProperty(value = "newPassword", required = true)
        String newPassword,
    @NotBlank(message = "confirmPassword is required")
        @JsonProperty(value = "confirmPassword", required = true)
        String confirmPassword) {

  public boolean confirmPasswordIsCorrect() {
    return this.newPassword.equals(this.confirmPassword);
  }
}
