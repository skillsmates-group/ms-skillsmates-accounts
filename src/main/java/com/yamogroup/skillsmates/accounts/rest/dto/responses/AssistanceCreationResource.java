package com.yamogroup.skillsmates.accounts.rest.dto.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.AssistanceEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AssistanceCreationResource extends BaseResponse {
    @JsonProperty(value = "topic")
    String topic;

    @JsonProperty(value = "message")
    String message;

    @JsonProperty(value = "account")
    String account;

    public AssistanceCreationResource(AssistanceEntity entity) {
        super(entity);
        this.topic = entity.getTopic();
        this.message = entity.getMessage();
        this.account = entity.getAccount().getUuid();
    }
}
