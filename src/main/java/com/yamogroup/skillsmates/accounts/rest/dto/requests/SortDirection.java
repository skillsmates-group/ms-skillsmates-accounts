package com.yamogroup.skillsmates.accounts.rest.dto.requests;

public enum SortDirection {
  ASC,
  DESC
}
