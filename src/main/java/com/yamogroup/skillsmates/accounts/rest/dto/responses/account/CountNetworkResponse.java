package com.yamogroup.skillsmates.accounts.rest.dto.responses.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class CountNetworkResponse {
    @JsonProperty(value = "suggestions")
    int suggestions;

    @JsonProperty(value = "followers")
    int followers;

    @JsonProperty(value = "followees")
    int followees;

    @JsonProperty(value = "favorites")
    int favorites;

    @JsonProperty(value = "onlines")
    int onlines;
}
