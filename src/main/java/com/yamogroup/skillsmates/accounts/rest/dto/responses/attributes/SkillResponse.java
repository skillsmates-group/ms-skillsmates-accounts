package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.SkillEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SkillResponse extends AttributeResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private SkillResource resource;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<SkillResource> resources;

    public SkillResponse(SkillEntity entity) {
        super(AttributeEnum.SKILL.getLabel());
        this.resource = new SkillResource(entity);
    }

    public SkillResponse(MetaResponse metaResponse, SkillEntity entity) {
        super(AttributeEnum.SKILL.getLabel(), metaResponse);
        this.resource = new SkillResource(entity);
    }

    public SkillResponse(Page<SkillEntity> entities) {
        super(
                AttributeEnum.SKILL.getLabel(),
                MetaResponse.builder()
                        .hasPrevious(entities.hasPrevious())
                        .hasNext(entities.hasNext())
                        .totalPages(entities.getTotalPages())
                        .total((int) entities.getTotalElements())
                        .build());
        this.resources = new ArrayList<>();
        entities.forEach(entity -> resources.add(new SkillResource(entity)));
    }
}
