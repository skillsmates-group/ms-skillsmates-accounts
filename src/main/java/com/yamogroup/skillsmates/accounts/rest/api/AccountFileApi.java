package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.exceptions.ThirdPartyException;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import io.micrometer.core.lang.Nullable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;

@Tag(name = "AccountFile")
@RequestMapping(
        value = "/skillsmates/accounts",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8",
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
public interface AccountFileApi {
    @PostMapping(value = "/avatar/{accountId}")
    ResponseEntity<AccountDetailsResponse> saveAccountAvatar(
            @PathVariable @NotBlank String accountId, @Nullable @RequestPart("file") MultipartFile file)
            throws ThirdPartyException;
}
