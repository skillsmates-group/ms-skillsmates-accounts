package com.yamogroup.skillsmates.accounts.rest.dto.requests.parameters;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ParameterEntity;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record ParameterRequest(
   @JsonProperty(value = "uuid", required = true)
   String uuid,

    @JsonProperty(value = "label", required = true)
     String label,

    @JsonProperty(value = "code", required = true)
     String code
){
     public ParameterEntity toEntity(){
          ParameterEntity entity = new ParameterEntity();
          entity.setUuid(this.uuid);
          entity.setCode(this.code);
          entity.setLabel(this.label);
          return entity;
     }
}
