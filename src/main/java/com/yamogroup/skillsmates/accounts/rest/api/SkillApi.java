package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.SkillRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.SkillResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Tag(name = "Skill")
@RequestMapping(value = "/skillsmates/accounts/skills", produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface SkillApi {
    @PostMapping
    ResponseEntity<SkillResponse> createSkill(@Valid @RequestBody SkillRequest dto) throws BadRequestException;

    @GetMapping(value = "{id}")
    ResponseEntity<SkillResponse> findSkill(@NotBlank @PathVariable String id);

    @GetMapping(value = "account/{accountId}")
    ResponseEntity<SkillResponse> findSkillByAccount(@NotBlank @PathVariable String accountId);

    @PutMapping(value = "{id}")
    ResponseEntity<SkillResponse> updateSkill(@NotBlank @PathVariable String id, @Valid @RequestBody SkillRequest dto) throws BadRequestException;

    @DeleteMapping(value = "{id}")
    ResponseEntity<SkillResponse> deleteSkill(@NotBlank @PathVariable String id);
}
