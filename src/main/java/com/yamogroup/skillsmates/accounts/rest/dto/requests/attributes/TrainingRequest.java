package com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.TrainingEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.*;
import com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.parameters.ParameterRequest;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record TrainingRequest(

        @JsonProperty(value = "title")
        String title,

        @JsonProperty(value = "description")
        String description,

        @NotBlank(message = "account is required")
        @JsonProperty(value = "account", required = true)
        String account,

        @JsonProperty(value = "education", required = true)
        EducationEnum education,

        @JsonProperty(value = "schoolType")
        ParameterRequest schoolType,

        @NotBlank(message = "schoolName is required")
        @JsonProperty(value = "schoolName", required = true)
        String schoolName,

        @JsonProperty(value = "city")
        String city,

        @JsonProperty(value = "schoolClass")
        ParameterRequest schoolClass,

        @JsonProperty(value = "activityArea")
        ParameterRequest activityArea,

        @JsonProperty(value = "level")
        ParameterRequest level,

        @JsonProperty(value = "teachingField")
        ParameterRequest teachingField,

        @JsonProperty(value = "teachingName")
        ParameterRequest teachingName
) {
    public TrainingEntity toEntity() {
        TrainingEntity entity = new TrainingEntity();
        entity.setTitle(this.title);
        entity.setDescription(this.description);
        entity.setAccount(toAccountEntity());
        entity.setEducation(this.education);
        entity.setSchoolType(toSchoolTypeEntity());
        entity.setSchoolName(this.schoolName);
        entity.setCity(this.city);
        entity.setSchoolClass(toSchoolClassEntity());
        entity.setLevel(toDiplomaLevelEntity());
        TeachingFieldEntity teachingFieldEntity = toTeachingFieldEntity();
        TeachingNameEntity teachingNameEntity = toTeachingNameEntity();
        if (teachingNameEntity != null) {
            teachingNameEntity.setTeachingField(teachingFieldEntity);
        }
        entity.setTeachingName(teachingNameEntity);
        entity.setActivityArea(toActivityAreaEntity());
        return entity;
    }

    public TeachingNameEntity toTeachingNameEntity() {
        TeachingNameEntity entity = null;
        if (this.teachingName != null) {
            entity = new TeachingNameEntity();
            entity.setCode(this.teachingName.code());
            entity.setLabel(this.teachingName.label());
        }
        return entity;
    }

    public TeachingFieldEntity toTeachingFieldEntity() {
        TeachingFieldEntity entity = null;
        if (this.teachingField != null) {
            entity = new TeachingFieldEntity();
            entity.setCode(this.teachingField.code());
            entity.setLabel(this.teachingField.label());
        }
        return entity;
    }

    public DiplomaLevelEntity toDiplomaLevelEntity() {
        DiplomaLevelEntity entity = null;
        if (this.level != null) {
            entity = new DiplomaLevelEntity();
            entity.setCode(this.level.code());
            entity.setLabel(this.level.label());
        }
        return entity;
    }

    public SchoolClassEntity toSchoolClassEntity() {
        SchoolClassEntity entity = null;
        if (this.schoolClass != null) {
            entity = new SchoolClassEntity();
            entity.setCode(this.schoolClass.code());
            entity.setLabel(this.schoolClass.label());
        }
        return entity;
    }

    public SchoolTypeEntity toSchoolTypeEntity() {
        SchoolTypeEntity entity = null;
        if (this.schoolType != null) {
            entity = new SchoolTypeEntity();
            entity.setCode(this.schoolType.code());
            entity.setLabel(this.schoolType.label());
        }
        return entity;
    }

    public ActivityAreaEntity toActivityAreaEntity() {
        ActivityAreaEntity entity = null;
        if (this.schoolType != null) {
            entity = new ActivityAreaEntity();
            entity.setCode(this.schoolType.code());
            entity.setLabel(this.schoolType.label());
        }
        return entity;
    }

    public AccountEntity toAccountEntity() {
        AccountEntity entity = new AccountEntity();
        entity.setUuid(this.account);
        return entity;
    }
}
