package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AttributeResponse {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private MetaResponse meta;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonProperty(value = "type")
  private String type;

  public AttributeResponse(String type){
    this.type = type;
  }

  public AttributeResponse(String type, MetaResponse meta){
    this.type = type;
    this.meta = meta;
  }
}
