package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.GenderEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum;
import com.yamogroup.skillsmates.accounts.helpers.DateHelper;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record AccountUpdateDetailsRequest(
        @NotBlank(message = "uuid is required")
        @JsonProperty(value = "uuid", required = true)
        String uuid,
        @JsonProperty(value = "firstname")
        String firstname,
        @JsonProperty(value = "lastname")
        String lastname,
        @JsonProperty("address")
        String address,
        @JsonProperty(value = "phoneNumber")
        String phoneNumber,
        @JsonProperty(value = "birthdate")
        String birthdate,
        @JsonProperty(value = "hideBirthdate")
        boolean hideBirthdate,
        @JsonProperty("gender")
        GenderEnum gender,
        @JsonProperty(value = "city")
        String city,
        @JsonProperty(value = "country")
        String country,
        @JsonProperty("status")
        StatusEnum status
) {
    public AccountEntity toEntity() {
        AccountEntity entity = new AccountEntity();
        entity.setUuid(this.uuid);
        entity.setFirstname(this.firstname);
        entity.setLastname(this.lastname);
        entity.setStatus(this.status);
        entity.setGender(this.gender);
        entity.setBirthdate(StringUtils.isNotBlank(this.birthdate) ? DateHelper.toDate(this.birthdate) : null);
        entity.setHideBirthdate(this.hideBirthdate);
        entity.setCity(this.city);
        entity.setCountry(this.country);
        entity.setPhoneNumber(this.phoneNumber);
        entity.setAddress(this.address);
        return entity;
    }
}
