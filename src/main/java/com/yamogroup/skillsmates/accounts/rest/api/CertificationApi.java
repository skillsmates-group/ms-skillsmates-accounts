package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.CertificationRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.CertificationResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Tag(name = "Certification")
@RequestMapping(
        value = "/skillsmates/accounts/certifications",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface CertificationApi {
    @PostMapping
    ResponseEntity<CertificationResponse> createCertification(@Valid @RequestBody CertificationRequest dto) throws BadRequestException;

    @GetMapping(value = "{id}")
    ResponseEntity<CertificationResponse> findCertification(@NotBlank @PathVariable String id);

    @GetMapping(value = "account/{accountId}")
    ResponseEntity<CertificationResponse> findCertificationByAccount(@NotBlank @PathVariable String accountId);

    @PutMapping(value = "{id}")
    ResponseEntity<CertificationResponse> updateCertification(@NotBlank @PathVariable String id, @Valid @RequestBody CertificationRequest dto) throws BadRequestException;

    @DeleteMapping(value = "{id}")
    ResponseEntity<CertificationResponse> deleteCertification(@NotBlank @PathVariable String id);
}
