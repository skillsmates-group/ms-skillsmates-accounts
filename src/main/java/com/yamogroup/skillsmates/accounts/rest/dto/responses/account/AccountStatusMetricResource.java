package com.yamogroup.skillsmates.accounts.rest.dto.responses.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.BaseResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class AccountStatusMetricResource extends BaseResponse {
  @JsonProperty(value = "label")
  String label;
  @JsonProperty(value = "code")
  String code;
  @JsonProperty(value = "total")
  long total;
}
