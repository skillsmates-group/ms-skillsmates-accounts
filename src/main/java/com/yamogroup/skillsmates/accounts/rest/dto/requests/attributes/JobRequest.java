package com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.JobEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivitySectorEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.parameters.ParameterRequest;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record JobRequest(
        @NotBlank(message = "title is required")
        @JsonProperty(value = "title", required = true)
        String title,

        @JsonProperty(value = "description")
        String description,

        @NotBlank(message = "account is required")
        @JsonProperty(value = "account", required = true)
        String account,

        @JsonProperty(value = "activityArea")
        ParameterRequest activityArea,

        @JsonProperty(value = "activitySector")
        ParameterRequest activitySector,

        @JsonProperty(value = "company", required = true)
        String company,

        @JsonProperty(value = "startDate", required = true)
        @JsonFormat(pattern = "yyyy-MM-dd")
        Date startDate,

        @JsonFormat(pattern = "yyyy-MM-dd")
        @JsonProperty(value = "endDate")
        Date endDate,

        @JsonProperty(value = "city")
        String city,

        @JsonProperty(value = "currentJob")
        boolean currentJob
) {
    public JobEntity toEntity() {
        JobEntity entity = new JobEntity();
        entity.setTitle(this.title);
        entity.setDescription(this.description);
        entity.setAccount(toAccountEntity());
        entity.setActivitySector(toActivitySectorEntity());
        entity.setActivityArea(toActivityAreaEntity());
        entity.setCompany(this.company);
        entity.setStartDate(this.startDate);
        entity.setEndDate(this.endDate);
        entity.setCity(this.city);
        entity.setCurrentJob(this.currentJob);
        return entity;
    }

    public AccountEntity toAccountEntity() {
        AccountEntity entity = new AccountEntity();
        entity.setUuid(this.account);
        return entity;
    }

    public ActivitySectorEntity toActivitySectorEntity() {
        ActivitySectorEntity entity = null;
        if (this.activitySector != null) {
            entity = new ActivitySectorEntity();
            entity.setCode(this.activitySector.code());
            entity.setLabel(this.activitySector.label());
        }
        return entity;
    }

    public ActivityAreaEntity toActivityAreaEntity() {
        ActivityAreaEntity entity = null;
        if (this.activityArea != null) {
            entity = new ActivityAreaEntity();
            entity.setCode(this.activityArea.code());
            entity.setLabel(this.activityArea.label());
        }
        return entity;
    }
}
