package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.api.JobApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.JobRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.JobResponse;
import com.yamogroup.skillsmates.accounts.services.impl.attributes.JobServiceImpl;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class JobController implements JobApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobController.class);

    private final JobServiceImpl jobService;

    @Override
    public ResponseEntity<JobResponse> createJob(JobRequest requestDto) {
        return ResponseEntity.ok(new JobResponse(jobService.save(requestDto.toEntity())));
    }

    @Override
    public ResponseEntity<JobResponse> findJob(String id) {
        return ResponseEntity.ok(new JobResponse(jobService.find(id)));
    }

    @Override
    public ResponseEntity<JobResponse> findJobByAccount(String accountId) {
        return ResponseEntity.ok(new JobResponse(jobService.findByAccount(accountId)));
    }

    @Override
    public ResponseEntity<JobResponse> updateJob(String id, JobRequest jobDto)
            throws BadRequestException {
        return ResponseEntity.ok(new JobResponse(jobService.update(id, jobDto.toEntity())));
    }

    @Override
    public ResponseEntity<JobResponse> deleteJob(String id) {
        return ResponseEntity.ok(new JobResponse(jobService.delete(id)));
    }
}
