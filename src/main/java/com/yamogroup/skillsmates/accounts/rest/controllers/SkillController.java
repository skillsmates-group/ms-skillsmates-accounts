package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.api.SkillApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.SkillRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.SkillResponse;
import com.yamogroup.skillsmates.accounts.services.impl.attributes.SkillServiceImpl;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class SkillController implements SkillApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(SkillController.class);

    private final SkillServiceImpl skillService;

    @Override
    public ResponseEntity<SkillResponse> createSkill(SkillRequest requestDto)
            throws BadRequestException {
        return ResponseEntity.ok(new SkillResponse(skillService.save(requestDto.toEntity())));
    }

    @Override
    public ResponseEntity<SkillResponse> findSkill(@NotBlank @PathVariable String id) {
        return ResponseEntity.ok(new SkillResponse(skillService.find(id)));
    }

    @Override
    public ResponseEntity<SkillResponse> findSkillByAccount(@NotBlank @PathVariable String accountId) {
        return ResponseEntity.ok(new SkillResponse(skillService.findByAccount(accountId)));
    }

    @Override
    public ResponseEntity<SkillResponse> updateSkill(
            @NotBlank @PathVariable String id, @Valid @RequestBody SkillRequest skillDto)
            throws BadRequestException {
        return ResponseEntity.ok(new SkillResponse(skillService.update(id, skillDto.toEntity())));
    }

    @Override
    public ResponseEntity<SkillResponse> deleteSkill(@NotBlank @PathVariable String id) {
        return ResponseEntity.ok(new SkillResponse(skillService.delete(id)));
    }
}
