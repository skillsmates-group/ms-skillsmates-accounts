package com.yamogroup.skillsmates.accounts.rest.dto.responses.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountStatusMetricResponse {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private MetaResponse meta;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private List<AccountStatusMetricResource> resources = new ArrayList<>();
}
