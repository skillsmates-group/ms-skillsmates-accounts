package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountAttributesResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MetaResponse meta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private AccountAttributesResource resource;

    public AccountAttributesResponse(AccountAttributesResource resource) {
        this.resource = resource;
    }
}
