package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public record PasswordReset(
  @NotBlank(message = "token is required")
  @JsonProperty(value = "token", required = true)
  String token,

  @NotBlank(message = "email is required")
  @JsonProperty(value = "password", required = true)
  String password
  )
{}
