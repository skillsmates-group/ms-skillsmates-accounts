package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.rest.dto.requests.AssistanceCreationRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.AssistanceCreationResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Tag(name = "Assistance")
@RequestMapping(
        value = "/skillsmates/accounts/assistance",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface AssistanceApi {
    @PostMapping
    ResponseEntity<AssistanceCreationResponse> createAssistance(
            @Valid @RequestBody AssistanceCreationRequest request);
}
