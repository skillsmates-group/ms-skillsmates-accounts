package com.yamogroup.skillsmates.accounts.rest.dto.responses.parameters;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ParameterEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.MetaResponse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParameterResponse {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MetaResponse meta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ParameterResource resource;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ParameterResource> resources;

    public ParameterResponse(ParameterEntity entity) {
        if (entity != null) {
            this.resource = new ParameterResource(entity);
        }
    }

    public ParameterResponse(Page<? extends ParameterEntity> entities) {
        this.meta =
                MetaResponse.builder()
                        .hasPrevious(entities.hasPrevious())
                        .hasNext(entities.hasNext())
                        .totalPages(entities.getTotalPages())
                        .total((int) entities.getTotalElements())
                        .build();
        this.resources = new ArrayList<>();
        entities.forEach(entity -> this.resources.add(new ParameterResource(entity)));
    }
}
