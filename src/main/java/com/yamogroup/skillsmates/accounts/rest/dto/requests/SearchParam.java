package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public record SearchParam(
    @JsonProperty(value = "account")
    String account,
    @JsonProperty(value = "content")
    String content,
    @JsonProperty(value = "city")
    String city,
    @JsonProperty(value = "country")
    String country,
    @JsonProperty(value = "documentTypes")
    List<String> documentTypes,
    @JsonProperty(value = "statuses")
    List<String> statuses,
    @JsonProperty(value = "direction")
    SortDirection direction,
    @JsonProperty(value = "networkType")
    String networkType){
}
