package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.CertificationEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.parameters.ParameterResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CertificationResource extends AttributeResource {
    @JsonProperty(value = "schoolType")
    private ParameterResponse schoolType;

    @JsonProperty(value = "schoolName")
    private String schoolName;

    @JsonProperty(value = "activityArea")
    private ParameterResponse activityArea;

    @JsonProperty(value = "permanent")
    private boolean permanent;

    @JsonProperty(value = "startDate")
    private Date startDate;

    @JsonProperty(value = "endDate")
    private Date endDate;

    public CertificationResource(CertificationEntity entity) {
        super(entity);
        if (entity.getSchoolType() != null) {
            this.schoolType = new ParameterResponse(entity.getSchoolType());
        }
        this.schoolName = entity.getSchoolName();
        if (entity.getActivityArea() != null) {
            this.activityArea = new ParameterResponse(entity.getActivityArea());
        }
        this.permanent = entity.isPermanent();
        this.startDate = entity.getStartDate();
        this.endDate = entity.getEndDate();
    }
}
