package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.api.TrainingApi;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.TrainingRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.TrainingResponse;
import com.yamogroup.skillsmates.accounts.services.impl.attributes.TrainingServiceImpl;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class TrainingController implements TrainingApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainingController.class);

    private final TrainingServiceImpl trainingService;

    @Override
    public ResponseEntity<TrainingResponse> createTraining(@Valid @RequestBody TrainingRequest dto)
            throws BadRequestException {
        return ResponseEntity.ok(new TrainingResponse(trainingService.save(dto.toEntity())));
    }

    @Override
    public ResponseEntity<TrainingResponse> findTraining(@NotBlank @PathVariable String id) {
        return ResponseEntity.ok(new TrainingResponse(trainingService.find(id)));
    }

    @Override
    public ResponseEntity<TrainingResponse> findTrainingByAccount(@NotBlank @PathVariable String accountId) {
        return ResponseEntity.ok(new TrainingResponse(trainingService.findByAccount(accountId)));
    }

    @Override
    public ResponseEntity<TrainingResponse> updateTraining(
            @NotBlank @PathVariable String id, @Valid @RequestBody TrainingRequest dto)
            throws BadRequestException {
        return ResponseEntity.ok(new TrainingResponse(trainingService.update(id, dto.toEntity())));
    }

    @Override
    public ResponseEntity<TrainingResponse> deleteTraining(@NotBlank @PathVariable String id) {
        return ResponseEntity.ok(new TrainingResponse(trainingService.delete(id)));
    }
}
