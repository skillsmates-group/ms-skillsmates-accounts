package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.AttributeEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.BaseResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AttributeResource extends BaseResponse {
    @JsonProperty(value = "title")
    private String title;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "account")
    private String account;

    public AttributeResource(AttributeEntity entity) {
        super(entity);
        this.title = entity.getTitle();
        this.description = entity.getDescription();
        this.account = entity.getAccount().getUuid();
    }
}
