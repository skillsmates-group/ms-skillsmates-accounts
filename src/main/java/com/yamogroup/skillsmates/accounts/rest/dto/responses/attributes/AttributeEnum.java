package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import lombok.Getter;

@Getter
public enum AttributeEnum {
  CERTIFICATION("certification"),
  DIPLOMA("diploma"),
  JOB("job"),
  SKILL("skill"),
  TRAINING("training");

  private final String label;

  AttributeEnum(String label) {
    this.label = label;
  }
}
