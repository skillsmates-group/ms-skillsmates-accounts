package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.JobRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.JobResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Tag(name = "Job")
@RequestMapping(value = "/skillsmates/accounts/jobs", produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface JobApi {
    @PostMapping
    ResponseEntity<JobResponse> createJob(@Valid @RequestBody JobRequest dto) throws BadRequestException;

    @GetMapping(value = "{id}")
    ResponseEntity<JobResponse> findJob(@NotBlank @PathVariable String id);

    @GetMapping(value = "account/{accountId}")
    ResponseEntity<JobResponse> findJobByAccount(@NotBlank @PathVariable String accountId);

    @PutMapping(value = "{id}")
    ResponseEntity<JobResponse> updateJob(@NotBlank @PathVariable String id, @Valid @RequestBody JobRequest dto) throws BadRequestException;

    @DeleteMapping(value = "{id}")
    ResponseEntity<JobResponse> deleteJob(@NotBlank @PathVariable String id);
}
