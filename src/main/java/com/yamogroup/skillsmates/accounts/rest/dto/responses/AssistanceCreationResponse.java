package com.yamogroup.skillsmates.accounts.rest.dto.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamogroup.skillsmates.accounts.dao.entities.AssistanceEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssistanceCreationResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MetaResponse meta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private AssistanceCreationResource resource;

    public AssistanceCreationResponse(AssistanceEntity entity) {
        this.resource = new AssistanceCreationResource(entity);
    }
}
