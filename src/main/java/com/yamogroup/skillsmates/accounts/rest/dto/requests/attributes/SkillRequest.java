package com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.SkillEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.skill.CategoryEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.skill.SkillLevelEnum;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public record SkillRequest(

        @NotBlank(message = "title is required")
        @JsonProperty(value = "title", required = true)
        String title,

        @JsonProperty(value = "description")
        String description,

        @JsonProperty(value = "keywords")
        String keywords,

        @NotBlank(message = "account is required")
        @JsonProperty(value = "account", required = true)
        String account,

        @JsonProperty(value = "category")
        CategoryEnum category,

        @JsonProperty(value = "discipline")
        String discipline,

        @JsonProperty(value = "level")
        SkillLevelEnum level
) {
    public SkillEntity toEntity() {
        SkillEntity entity = new SkillEntity();
        entity.setTitle(this.title);
        entity.setDescription(this.description);
        entity.setKeywords(this.keywords);
        entity.setAccount(toAccountEntity());
        entity.setCategory(this.category);
        entity.setDiscipline(this.discipline);
        entity.setLevel(this.level);
        return entity;
    }

    public AccountEntity toAccountEntity() {
        AccountEntity entity = new AccountEntity();
        entity.setUuid(this.account);
        return entity;
    }

}
