package com.yamogroup.skillsmates.accounts.rest.api;

import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.attributes.TrainingRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.TrainingResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Tag(name = "Training")
@RequestMapping(
        value = "/skillsmates/accounts/trainings",
        produces = MediaType.APPLICATION_JSON_VALUE + "; charset=utf-8")
public interface TrainingApi {
    @PostMapping
    ResponseEntity<TrainingResponse> createTraining(@Valid @RequestBody TrainingRequest dto) throws BadRequestException;

    @GetMapping(value = "{id}")
    ResponseEntity<TrainingResponse> findTraining(@NotBlank @PathVariable String id);

    @GetMapping(value = "account/{accountId}")
    ResponseEntity<TrainingResponse> findTrainingByAccount(@NotBlank @PathVariable String accountId);

    @PutMapping(value = "{id}")
    ResponseEntity<TrainingResponse> updateTraining(@NotBlank @PathVariable String id, @Valid @RequestBody TrainingRequest dto) throws BadRequestException;

    @DeleteMapping(value = "{id}")
    ResponseEntity<TrainingResponse> deleteTraining(@NotBlank @PathVariable String id);
}
