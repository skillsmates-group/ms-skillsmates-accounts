package com.yamogroup.skillsmates.accounts.rest.dto.responses.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.BaseResponse;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountAuthResource extends BaseResponse {
    @JsonProperty(value = "firstname")
    String firstname;

    @JsonProperty(value = "lastname")
    String lastname;

    @JsonProperty(value = "email")
    String email;

    public AccountAuthResource(AccountEntity entity) {
        super(entity);
        this.firstname = entity.getFirstname();
        this.lastname = entity.getLastname();
        this.email = entity.getEmail();
    }
}
