package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.exceptions.ThirdPartyException;
import com.yamogroup.skillsmates.accounts.rest.api.AccountFileApi;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.services.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class AccountFileController implements AccountFileApi {

    private final AccountService accountService;

    @Override
    public ResponseEntity<AccountDetailsResponse> saveAccountAvatar(
            String accountId, MultipartFile file) throws ThirdPartyException {
        return ResponseEntity.ok(new AccountDetailsResponse(accountService.saveAvatar(accountId, file)));
    }
}
