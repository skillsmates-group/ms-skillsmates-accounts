package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.TrainingEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.parameters.ParameterResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainingResource extends AttributeResource {
    @JsonProperty(value = "education")
    private EducationEnum education;

    @JsonProperty(value = "schoolType")
    private ParameterResponse schoolType;

    @JsonProperty(value = "schoolName")
    private String schoolName;

    @JsonProperty(value = "city")
    private String city;

    @JsonProperty(value = "schoolClass")
    private ParameterResponse schoolClass;

    @JsonProperty(value = "activityArea")
    private ParameterResponse activityArea;

    @JsonProperty(value = "level")
    private ParameterResponse level;

    @JsonProperty(value = "teachingName")
    private ParameterResponse teachingName;

    public TrainingResource(TrainingEntity entity) {
        super(entity);
        this.education = entity.getEducation();
        if (entity.getSchoolType() != null) {
            this.schoolType = new ParameterResponse(entity.getSchoolType());
        }
        this.schoolName = entity.getSchoolName();
        this.city = entity.getCity();

        this.schoolClass = new ParameterResponse(entity.getSchoolClass());
        this.level = entity.getLevel() != null ? new ParameterResponse(entity.getLevel()) : null;
        this.teachingName = entity.getTeachingName() != null ? new ParameterResponse(entity.getTeachingName()) : null;
    }
}
