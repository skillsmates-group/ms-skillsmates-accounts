package com.yamogroup.skillsmates.accounts.rest.dto.responses;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class MetaResponse {
  private boolean hasPrevious;
  private boolean hasNext;
  private int total;
  private int totalPages;
}
