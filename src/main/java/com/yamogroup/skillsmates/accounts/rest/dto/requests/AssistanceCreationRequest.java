package com.yamogroup.skillsmates.accounts.rest.dto.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.AssistanceEntity;
import lombok.Builder;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public record AssistanceCreationRequest(
    @NotBlank(message = "topic is required")
    @JsonProperty(value = "topic", required = true)
    String topic,
    @NotBlank(message = "message is required")
    @JsonProperty(value = "message", required = true)
    String message,
    @NotBlank(message = "account is required")
    @JsonProperty(value = "account")
    String account
){
    public AssistanceEntity toEntity(){
        AssistanceEntity entity = new AssistanceEntity();
        entity.setTopic(this.topic);
        entity.setMessage(this.message);
        return entity;
    }
}
