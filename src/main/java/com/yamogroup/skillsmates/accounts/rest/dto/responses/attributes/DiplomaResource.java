package com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.DiplomaEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.parameters.ParameterResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiplomaResource extends AttributeResource {
    @JsonProperty(value = "education")
    private EducationEnum education;

    @JsonProperty(value = "level")
    private ParameterResponse level;

    @JsonProperty(value = "schoolType")
    private ParameterResponse schoolType;

    @JsonProperty(value = "activityArea")
    private ParameterResponse activityArea;

    @JsonProperty(value = "schoolName")
    private String schoolName;

    @JsonProperty(value = "city")
    private String city;

    @JsonProperty(value = "startDate")
    private Date startDate;

    @JsonProperty(value = "endDate")
    private Date endDate;

    public DiplomaResource(DiplomaEntity entity) {
        super(entity);
        this.education = entity.getEducation();
        if (entity.getLevel() != null) {
            this.level = new ParameterResponse(entity.getLevel());
        }
        if (entity.getSchoolType() != null) {
            this.schoolType = new ParameterResponse(entity.getSchoolType());
        }
        if (entity.getActivityArea() != null) {
            this.activityArea = new ParameterResponse(entity.getActivityArea());
        }
        this.schoolName = entity.getSchoolName();
        this.city = entity.getCity();
        this.startDate = entity.getStartDate();
        this.endDate = entity.getEndDate();
    }
}
