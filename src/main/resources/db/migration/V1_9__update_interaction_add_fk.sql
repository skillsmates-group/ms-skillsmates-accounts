SET GLOBAL FOREIGN_KEY_CHECKS=0;
SET GLOBAL collation_server = 'utf8mb4_general_ci';

ALTER TABLE z_interaction ADD COLUMN post int(11) default null;
ALTER TABLE z_interaction ADD CONSTRAINT FK_interaction_post FOREIGN KEY (post) REFERENCES z_post(id);
ALTER TABLE z_interaction ADD COLUMN account int(11) default null;
ALTER TABLE z_interaction ADD CONSTRAINT FK_interaction_account FOREIGN KEY (account) REFERENCES z_account(id);
