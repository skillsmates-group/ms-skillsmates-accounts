DROP TABLE IF EXISTS `z_dashboard_post`;
CREATE TABLE IF NOT EXISTS `z_dashboard_post`
(
    `id`          INT(11)  NOT NULL AUTO_INCREMENT,
    `created_at`  DATETIME NOT NULL,
    `deleted`     BIT(1)   DEFAULT NULL,
    `updated_at`  DATETIME DEFAULT NULL,
    `uuid`        varchar(255),
    `post`        INT(11),
    `interaction` INT(11),
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_dashboard_post_post` FOREIGN KEY (`post`) REFERENCES `z_post` (`id`),
    CONSTRAINT `FK_dashboard_post_interaction` FOREIGN KEY (`interaction`) REFERENCES `z_interaction` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = UTF8;

INSERT INTO z_dashboard_post
    (`created_at`, `deleted`, `updated_at`, `post`)
SELECT p.created_at, p.deleted, p.updated_at, p.id
FROM z_post p;

INSERT INTO z_dashboard_post
    (`created_at`, `deleted`, `updated_at`, `interaction`)
SELECT i.created_at, i.deleted, i.updated_at, i.id
FROM z_interaction i
WHERE i.post IS NOT NULL
  AND i.interaction_type =
      (SELECT it.id FROM z_interaction_type it WHERE it.label = 'SHARE');