ALTER TABLE z_diploma ADD COLUMN activity_area int(11) default null;
ALTER TABLE z_diploma ADD CONSTRAINT FOREIGN KEY (activity_area) REFERENCES z_activity_area (id);
ALTER TABLE z_training ADD COLUMN activity_area int(11) default null;
ALTER TABLE z_training ADD CONSTRAINT FOREIGN KEY (activity_area) REFERENCES z_activity_area (id);
