SET GLOBAL FOREIGN_KEY_CHECKS=0;
SET GLOBAL collation_server = 'utf8mb4_general_ci';

DROP TABLE IF EXISTS `z_country`;
CREATE TABLE IF NOT EXISTS `z_country` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_country_uuid` (`uuid`),
  UNIQUE KEY `UK_country_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=34750 DEFAULT CHARSET=utf8mb3;

UNLOCK TABLES;
INSERT INTO z_country (id, created_at, deleted, updated_at, uuid, code, label) VALUES
	(34509, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'a484b992-dae0-4c43-889d-081504f4018d', 'AFG', 'Afghanistan'),
	(34510, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '7f80b087-7cf3-48c3-80be-74c995be821a', 'ALB', 'Albanie'),
	(34511, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '09441e63-b8b9-4649-b997-f7cf5d5ea8fb', 'ATA', 'Antarctique'),
	(34512, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '82685c9a-7a48-4b46-81a8-3577d4b0d6b8', 'DZA', 'Algérie'),
	(34513, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9d4950b2-5cab-438d-a418-8c7e0b8cdcbe', 'ASM', 'Samoa Américaines'),
	(34514, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9a4211a6-c7da-4900-9da0-ba591cc7c33d', 'AND', 'Andorre'),
	(34515, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '5bdded7f-c5ee-494d-995b-4c611aae39f5', 'AGO', 'Angola'),
	(34516, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '745a1098-f914-4573-9d7e-39aee8ca4e06', 'ATG', 'Antigua-et-Barbuda'),
	(34517, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'b02ff670-dd91-49a9-a916-1ee48a93fd6a', 'AZE', 'Azerbaïdjan'),
	(34518, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd06b9d0d-2882-44bc-8bc5-e91911410b5a', 'ARG', 'Argentine'),
	(34519, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4edeb36d-6823-416d-ba77-1206f3fa9c88', 'AUS', 'Australie'),
	(34520, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2e9da665-db42-45b6-8e56-266e93c4eb0f', 'AUT', 'Autriche'),
	(34521, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd55a4c0c-4dbe-4134-82c9-bda3b5719e58', 'BHS', 'Bahamas'),
	(34522, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9f02a2eb-57a9-4b84-bfd1-a7444638ba93', 'BHR', 'Bahreïn'),
	(34523, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '71bafcd7-fa5e-4730-bb94-63cf0fa5d0b5', 'BGD', 'Bangladesh'),
	(34524, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '72ef847f-2881-449f-9bef-263c86dc7e91', 'ARM', 'Arménie'),
	(34525, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c3367ab6-e81b-4378-9c49-8271670a86ee', 'BRB', 'Barbade'),
	(34526, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '7228d3cc-98ad-4d5a-8e45-4ffca76452f9', 'BEL', 'Belgique'),
	(34527, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '30954e36-3174-435a-a582-f1a34ea46ea8', 'BMU', 'Bermudes'),
	(34528, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '695854d3-d4d6-48eb-9ea1-901ac8aad871', 'BTN', 'Bhoutan'),
	(34529, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0df88c71-3650-4987-a57d-774807a3fe1d', 'BOL', 'Bolivie'),
	(34530, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '889d465d-84da-4716-8a9c-a320b4dcd0ef', 'BIH', 'Bosnie-Herzégovine'),
	(34531, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '34171233-32a2-4180-8ae1-48285a4af317', 'BWA', 'Botswana'),
	(34532, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '561e61bc-c60f-495b-a4bb-362d4287b1e2', 'BVT', 'Île Bouvet'),
	(34533, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2be454f8-e8e5-4cdb-9635-8594c84d21c3', 'BRA', 'Brésil'),
	(34534, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'f3a44dcd-9717-4a73-bc99-3edaad5bcc11', 'BLZ', 'Belize'),
	(34535, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '463a0dcf-7663-4d03-ac4f-7ab4244f0301', 'IOT', 'Territoire Britannique de l\'Océan Indien'),
	(34536, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '33dda7cf-13b2-41d3-838b-fea01dd3c3bb', 'SLB', 'Îles Salomon'),
	(34537, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ebf627a7-36a5-42b3-a29b-dc2faed5f743', 'VGB', 'Îles Vierges Britanniques'),
	(34538, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2bb8dcbf-a408-4896-b659-b4d877643fd2', 'BRN', 'Brunéi Darussalam'),
	(34539, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '211f23c7-b6d8-4fb5-9bed-e0f9297dc584', 'BGR', 'Bulgarie'),
	(34540, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '95ef95bb-3760-4dba-8a9a-d9381ab907e1', 'MMR', 'Myanmar'),
	(34541, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4cf4f0d9-c979-471e-a196-22c2551fd40b', 'BDI', 'Burundi'),
	(34542, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd0b3e842-1e86-4618-b81c-942c9b0ae1f8', 'BLR', 'Bélarus'),
	(34543, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2ad47623-496b-4819-95df-1c9cf5977f50', 'KHM', 'Cambodge'),
	(34544, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '7deeb654-528c-4d0c-9c12-a3bc159c13d3', 'CMR', 'Cameroun'),
	(34545, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd9eabe87-eed2-4e9f-9ede-c6157e2190e4', 'CAN', 'Canada'),
	(34546, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '5ccc3d08-a7c7-4d7f-a744-9d4b1d558795', 'CPV', 'Cap-vert'),
	(34547, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '24c83cdd-b7bd-45f8-b29d-f7a6a86ff4d6', 'CYM', 'Îles Caïmanes'),
	(34548, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd17d0f5f-3e02-4e14-9267-77d10fbb6f40', 'CAF', 'République Centrafricaine'),
	(34549, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '54e019fa-4907-4d44-a86a-bb74dc80ab28', 'LKA', 'Sri Lanka'),
	(34550, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '53e351f8-ede0-44bd-85e8-60e254dca978', 'TCD', 'Tchad'),
	(34551, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8636ea5a-d45a-4ecd-ab86-baa95b5a2e09', 'CHL', 'Chili'),
	(34552, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '1ee6dab2-aff6-4cfa-84fc-7fd933a2ebd1', 'CHN', 'Chine'),
	(34553, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ba1be598-2c76-428c-9a6d-2383e524edbd', 'TWN', 'Taïwan'),
	(34554, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'f2d324d5-2114-4061-ba89-97ea48d24a82', 'CXR', 'Île Christmas'),
	(34555, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd09ca98a-3479-4fdb-b960-35dae71350a4', 'CCK', 'Îles Cocos (Keeling)'),
	(34556, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '426074b6-2b03-413e-a10e-0559579b0115', 'COL', 'Colombie'),
	(34557, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4483c389-8244-4621-b79b-c24fe6236b0f', 'COM', 'Comores'),
	(34558, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '40ad7d10-0907-4db1-a7bc-4538f7f32aa8', 'MYT', 'Mayotte'),
	(34559, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4782fb73-76d6-44ba-883d-c7c6a7f98268', 'COG', 'République du Congo'),
	(34560, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'a6acbafa-fa3e-4640-93aa-604365cecf4c', 'COD', 'République Démocratique du Congo'),
	(34561, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'dac978b4-f4d0-4a9f-b639-1a343b14f7a4', 'COK', 'Îles Cook'),
	(34562, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '48724ab3-5daf-47ff-b2ae-4b729b49f0b4', 'CRI', 'Costa Rica'),
	(34563, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ab4f13c7-f96c-4ad4-9b31-596755c9a79b', 'HRV', 'Croatie'),
	(34564, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '75eb2c8e-917f-48a3-81bf-2d94ac211c4a', 'CUB', 'Cuba'),
	(34565, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'a448ef6d-4c05-42d2-8971-1c39f22c6f8b', 'CYP', 'Chypre'),
	(34566, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'f7d6d43f-4458-43f7-9c1f-4973ba93c77f', 'CZE', 'République Tchèque'),
	(34567, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '50f77745-8337-4ef6-840f-5c7145134d3f', 'BEN', 'Bénin'),
	(34568, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4cdceb23-7623-4244-9baf-ee254def7db3', 'DNK', 'Danemark'),
	(34569, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '32284228-4b6b-4fc2-9f8f-2243dfc7eb7d', 'DMA', 'Dominique'),
	(34570, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '5aed545c-3ba0-4c89-bf98-6ca41fe901fb', 'DOM', 'République Dominicaine'),
	(34571, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '21cd2727-b718-4e4b-83d6-f857508c825f', 'ECU', 'Équateur'),
	(34572, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '16614aa7-ecc2-4a3d-ae43-08da36ff7b18', 'SLV', 'El Salvador'),
	(34573, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '26446f9a-b799-457a-91f9-c666e2234efd', 'GNQ', 'Guinée Équatoriale'),
	(34574, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ac6e2b3d-d100-4e16-bf29-a6fa6fda8355', 'ETH', 'Éthiopie'),
	(34575, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2862185a-f07f-47dc-9737-a2f01fce7672', 'ERI', 'Érythrée'),
	(34576, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9aa3112a-85d9-44d3-9d60-e35a2c689103', 'EST', 'Estonie'),
	(34577, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2f0c277b-5d63-4702-a931-8596fb90066e', 'FRO', 'Îles Féroé'),
	(34578, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'bfb95254-d3f7-4e9d-b240-ff1f3c7d71b7', 'FLK', 'Îles (malvinas) Falkland'),
	(34579, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'bd97036c-4713-4898-afee-fc0c5a32c63f', 'SGS', 'Géorgie du Sud et les Îles Sandwich du Sud'),
	(34580, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'e4f3a74b-f33e-4578-a170-ca1b0b120f29', 'FJI', 'Fidji'),
	(34581, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '644f1b3a-4f98-434a-8cff-41a5d3892dc0', 'FIN', 'Finlande'),
	(34582, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0317bfd8-cc0f-4018-99a6-a5c09193eecb', 'ALA', 'Îles Åland'),
	(34583, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '1ff49070-9ff6-4d8d-9bbd-65df0e99dace', 'FRA', 'France'),
	(34584, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '1c9a51e0-e396-4c80-bce7-acba9581006f', 'GUF', 'Guyane Française'),
	(34585, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0f110b16-0876-43af-83ef-0e866c7ecf29', 'PYF', 'Polynésie Française'),
	(34586, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9830c440-9bed-499b-8719-492270380af4', 'ATF', 'Terres Australes Françaises'),
	(34587, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0e59fa25-4b6d-4c2b-8817-3221d933e32d', 'DJI', 'Djibouti'),
	(34588, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'f4e5df2b-5e9e-44a3-8458-341c7d5f6bf4', 'GAB', 'Gabon'),
	(34589, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c5b2d830-a3ca-42f3-9cba-8b043569ad21', 'GEO', 'Géorgie'),
	(34590, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '5b16172f-99fd-4722-90c1-bf096caf1038', 'GMB', 'Gambie'),
	(34591, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd3fc997e-34cd-44d9-a232-f12a822d5ccc', 'PSE', 'Territoire Palestinien Occupé'),
	(34592, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ae67c10e-72a5-43ec-b3d7-99a45ad834a5', 'DEU', 'Allemagne'),
	(34593, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '3f4b8a6b-9e21-4bdc-a7ed-aa29b6ab509e', 'GHA', 'Ghana'),
	(34594, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'b0cb9c35-df06-4274-bed6-7ae6d3ffc359', 'GIB', 'Gibraltar'),
	(34595, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'e01ad790-2cb4-4661-8da3-0e38137b64f4', 'KIR', 'Kiribati'),
	(34596, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '53efa766-287c-4418-bb28-71b218d67a15', 'GRC', 'Grèce'),
	(34597, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'b5fb5049-f08a-44fb-b99b-6d729481aa7a', 'GRL', 'Groenland'),
	(34598, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'cd8520d4-2900-45b5-a83c-c9840a6d4c53', 'GRD', 'Grenade'),
	(34599, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'dca4c851-91ec-411d-a754-827b34ff3988', 'GLP', 'Guadeloupe'),
	(34600, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8cc25dc9-a817-44b1-84af-994d126fd64c', 'GUM', 'Guam'),
	(34601, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'b5a747d4-5f95-4e51-9f5a-2ff18eecc6cd', 'GTM', 'Guatemala'),
	(34602, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '88b212e2-ba7a-4d3d-92d8-b1c9af6346a6', 'GIN', 'Guinée'),
	(34603, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '72fac365-494e-41ae-9031-f5615bc3e6f1', 'GUY', 'Guyana'),
	(34604, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '20498879-f9f9-4167-889b-4b2b3e96f295', 'HTI', 'Haïti'),
	(34605, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'efb6495a-b442-4b23-a659-27fd55fe2f07', 'HMD', 'Îles Heard et Mcdonald'),
	(34606, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd32b3ca5-6c27-4718-bcfd-c124b34e3db5', 'VAT', 'Saint-Siège (état de la Cité du Vatican)'),
	(34607, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '6ddcad85-e7b8-4c01-9a81-3a9fedb00aba', 'HND', 'Honduras'),
	(34608, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'cad4fb7c-b46b-4742-b316-b788d8ad3df3', 'HKG', 'Hong-Kong'),
	(34609, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9a3e0fc5-5a7a-4eb2-a837-f437e84cd6a9', 'HUN', 'Hongrie'),
	(34610, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '6fb64b07-b0b3-4462-9c16-8ae3781529d1', 'ISL', 'Islande'),
	(34611, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '17e21bd0-05a8-4445-9de6-f8fa6fba41f3', 'IND', 'Inde'),
	(34612, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '21f9b639-26f6-4625-b89d-5875fef37fe1', 'IDN', 'Indonésie'),
	(34613, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '29d7b935-df5e-49f0-8ef7-63fea5a12341', 'IRN', 'République Islamique d\'Iran'),
	(34614, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'db671289-09e0-4f0d-816d-fb12d38b4d04', 'IRQ', 'Iraq'),
	(34615, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'a1fee6bc-e938-4aa9-b369-6f2805ea6964', 'IRL', 'Irlande'),
	(34616, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '1127eafa-3600-4812-83a9-6a64f79d3180', 'ISR', 'Israël'),
	(34617, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '76f899de-f911-4bec-80e4-47f571f2152a', 'ITA', 'Italie'),
	(34618, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'dd1c06b9-b327-4952-8770-15d29cb0a2d8', 'CIV', 'Côte d\'Ivoire'),
	(34619, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'f47afb15-d993-43c4-a6cf-e35ccdbecb2e', 'JAM', 'Jamaïque'),
	(34620, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '97dc3acc-dae3-4d60-9032-6b0fb674422e', 'JPN', 'Japon'),
	(34621, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '885deb73-f2bb-41f2-bec6-6a58b692c87f', 'KAZ', 'Kazakhstan'),
	(34622, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'be63ad59-8aee-4252-9c6b-90b28bdaef3e', 'JOR', 'Jordanie'),
	(34623, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4c8f23d5-ff10-41f2-81ca-829ec921cdab', 'KEN', 'Kenya'),
	(34624, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8ba03518-5da2-45d6-92d3-d1bb4f0ebb46', 'PRK', 'République Populaire Démocratique de Corée'),
	(34625, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '5eb68bd0-a84b-472c-8ebb-26bed170c376', 'KOR', 'République de Corée'),
	(34626, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'a10d6751-8a10-4427-bc46-f97c6bab12e9', 'KWT', 'Koweït'),
	(34627, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '64d4c897-b562-4039-80fb-2009a8b05f1a', 'KGZ', 'Kirghizistan'),
	(34628, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '027870a0-2a02-47d2-aaba-27d7ac507d93', 'LAO', 'République Démocratique Populaire Lao'),
	(34629, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '1e236b3a-7bf7-4203-8e21-c828983e318e', 'LBN', 'Liban'),
	(34630, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'eac593d3-7f01-462c-a3cc-8d486c234329', 'LSO', 'Lesotho'),
	(34631, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'caf47743-e814-4362-95a6-84b3c9ad9ecf', 'LVA', 'Lettonie'),
	(34632, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0b27a074-a762-4f9f-bf1e-ce63c9ec85d8', 'LBR', 'Libéria'),
	(34633, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '30298873-053b-488c-b36a-82e1166253ce', 'LBY', 'Jamahiriya Arabe Libyenne'),
	(34634, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '34df0d0c-4b75-449a-961f-5f87a139cd3e', 'LIE', 'Liechtenstein'),
	(34635, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c0384147-498f-4b99-be4c-f2cf43464c62', 'LTU', 'Lituanie'),
	(34636, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '5f9e80d4-0a79-4fd5-8e4f-2c33c5a819dd', 'LUX', 'Luxembourg'),
	(34637, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '111e5f3c-5303-497d-8ee8-7f855f6512ad', 'MAC', 'Macao'),
	(34638, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c8b02eb7-4a80-4348-b950-53dd96a89985', 'MDG', 'Madagascar'),
	(34639, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '83fc7bf5-0221-412c-9067-2c634f85318d', 'MWI', 'Malawi'),
	(34640, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c5b912ca-71e9-400e-80fa-a10c7e3603cd', 'MYS', 'Malaisie'),
	(34641, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '052a8ee6-63cc-4367-a1a8-3d36f37e6ba4', 'MDV', 'Maldives'),
	(34642, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '01578544-b9d9-4c87-90c3-03bcc9568bd4', 'MLI', 'Mali'),
	(34643, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd506e26d-1905-4505-bf41-d412a16e5d59', 'MLT', 'Malte'),
	(34644, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0f3de21d-fa17-4a4e-94ee-269dc8e53481', 'MTQ', 'Martinique'),
	(34645, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2e270117-dd63-4bdb-a69d-5ad4497efa94', 'MRT', 'Mauritanie'),
	(34646, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '24f8bd64-c715-4434-a222-14a9596c37be', 'MUS', 'Maurice'),
	(34647, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'cf4bfd78-a95b-4945-ba34-9906070d3af2', 'MEX', 'Mexique'),
	(34648, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '47d0082b-ae24-4f61-8803-b7eeb6a093e3', 'MCO', 'Monaco'),
	(34649, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9f9f865b-cac4-4238-9ec3-ddbc7ac230d6', 'MNG', 'Mongolie'),
	(34650, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '44c4ba44-050a-4929-b28d-01859aed1415', 'MDA', 'République de Moldova'),
	(34651, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '45f1a1a7-9286-442e-b8d5-2b523c3d02e0', 'MSR', 'Montserrat'),
	(34652, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2244cedf-94ea-4c55-884a-ec1f340ccf17', 'MAR', 'Maroc'),
	(34653, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '66f8b8d5-a091-4566-8455-85dd80dc9b17', 'MOZ', 'Mozambique'),
	(34654, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c10dec62-2c88-4842-a293-41e8ccd9608c', 'OMN', 'Oman'),
	(34655, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2e201aec-7f06-4000-b0ae-58177a23d0cc', 'NAM', 'Namibie'),
	(34656, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0f8539c6-3584-47ea-829d-db58f1b38513', 'NRU', 'Nauru'),
	(34657, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '31607d05-3b02-4139-85cc-7d0d091d83fc', 'NPL', 'Népal'),
	(34658, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8c05021c-eb16-42a0-8832-f76ad15cbe45', 'NLD', 'Pays-Bas'),
	(34659, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '59e1c8b9-176d-4cbd-a62d-7ed466872205', 'ANT', 'Antilles Néerlandaises'),
	(34660, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'bb911072-7ea6-493f-bf74-fd61991f9ea2', 'ABW', 'Aruba'),
	(34661, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd0c46a5d-34af-41c8-8b73-5e3e08c1365d', 'NCL', 'Nouvelle-Calédonie'),
	(34662, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '7c3eaa5c-eacc-4852-897b-51d61c0b8aab', 'VUT', 'Vanuatu'),
	(34663, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'f83db8ff-5c01-413f-8619-3588cb043577', 'NZL', 'Nouvelle-Zélande'),
	(34664, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '860429ce-c405-40cc-9868-ec3c1bf6c145', 'NIC', 'Nicaragua'),
	(34665, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '7172005a-10a0-44b5-a06c-a22742df76d1', 'NER', 'Niger'),
	(34666, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'af2ec45d-633f-4b6f-9b84-2fc84029c47d', 'NGA', 'Nigéria'),
	(34667, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8554214b-44ee-4252-9b00-d6764c18c570', 'NIU', 'Niué'),
	(34668, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2b151d94-d84b-4452-bc34-74703b4390d7', 'NFK', 'Île Norfolk'),
	(34669, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '85c837dc-fb47-49ab-812c-4b8bbea57061', 'NOR', 'Norvège'),
	(34670, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ada13bce-5c4f-4dbf-ae4b-562474b9c5c0', 'MNP', 'Îles Mariannes du Nord'),
	(34671, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '11cf8918-a213-4699-9778-002a9b557610', 'UMI', 'Îles Mineures Éloignées des États-Unis'),
	(34672, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd7e19276-83af-41bb-aa92-48c3810fa8cd', 'FSM', 'États Fédérés de Micronésie'),
	(34673, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'e2ef506e-210c-4991-8951-bf4f4a917a83', 'MHL', 'Îles Marshall'),
	(34674, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '81c1a132-8517-415a-85a7-ce7645028356', 'PLW', 'Palaos'),
	(34675, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '008539a0-1f52-4862-aa07-fe92b3881d1a', 'PAK', 'Pakistan'),
	(34676, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '6a5eadca-7d0b-4f39-bff5-2b4d5ba8e678', 'PAN', 'Panama'),
	(34677, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '22713723-e89a-4b39-84ed-397ab9175ea6', 'PNG', 'Papouasie-Nouvelle-Guinée'),
	(34678, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'a399d3fe-2d17-4701-849f-32511f4689a3', 'PRY', 'Paraguay'),
	(34679, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'aa62fcbb-6d0d-4149-b923-912db5557c57', 'PER', 'Pérou'),
	(34680, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'b47d2a3f-a533-42ba-a0e8-82fc29f35a93', 'PHL', 'Philippines'),
	(34681, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '58777eae-64dc-4ed7-a69d-25075fec0953', 'PCN', 'Pitcairn'),
	(34682, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'af85f3b1-6782-43e0-8794-c14af9e3c33e', 'POL', 'Pologne'),
	(34683, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'cc8692ec-5a7c-43d5-b986-ebc498f23a20', 'PRT', 'Portugal'),
	(34684, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '54caeae5-bad7-45bd-ad48-c4378f40b44e', 'GNB', 'Guinée-Bissau'),
	(34685, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '6138319c-6fbe-407d-8609-825a0a4f62d1', 'TLS', 'Timor-Leste'),
	(34686, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ac8f6a2d-1108-4843-b82a-a7c3ca0b389e', 'PRI', 'Porto Rico'),
	(34687, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4f8cd6de-d24c-4d71-a010-e4f4750a8cf6', 'QAT', 'Qatar'),
	(34688, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8ed5e05e-2611-43e4-ad9b-e0e4acb4ea6e', 'REU', 'Réunion'),
	(34689, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '5dba611c-1a58-4a17-b75c-2f28d10d0d3b', 'ROU', 'Roumanie'),
	(34690, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'e8c37353-1e0e-464e-9e6c-919dc8cffd29', 'RUS', 'Fédération de Russie'),
	(34691, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '16c3330b-eda7-4415-9b92-20eac2c56da1', 'RWA', 'Rwanda'),
	(34692, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '03dd7a8f-a71b-43a8-aff1-d5609936b4bd', 'SHN', 'Sainte-Hélène'),
	(34693, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4a3af587-0c2f-4ba4-8e3d-b6b19557492b', 'KNA', 'Saint-Kitts-et-Nevis'),
	(34694, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0b7cd2d6-69fb-4cf7-969a-bb3e11216509', 'AIA', 'Anguilla'),
	(34695, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'eeaee24d-62b8-4599-9b39-32de7e87b346', 'LCA', 'Sainte-Lucie'),
	(34696, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ccfdbf20-4176-4108-ae6f-d770e9ea5f31', 'SPM', 'Saint-Pierre-et-Miquelon'),
	(34697, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '3722abc4-358a-4632-859c-d63fee199a3f', 'VCT', 'Saint-Vincent-et-les Grenadines'),
	(34698, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'f70e3e3c-c4c1-47e2-8f93-f86ced9ebb3a', 'SMR', 'Saint-Marin'),
	(34699, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '81782bc8-26fc-4c92-86b3-4808b2ab96a2', 'STP', 'Sao Tomé-et-Principe'),
	(34700, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '510870ba-24d9-40ee-9f98-726da2582987', 'SAU', 'Arabie Saoudite'),
	(34701, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'aa25c409-2603-4f23-af6b-03b27b17a7d6', 'SEN', 'Sénégal'),
	(34702, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '60dc9b33-ea0d-4b87-9ac0-9ab4c37b6ce2', 'SYC', 'Seychelles'),
	(34703, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9c224182-aa50-4267-8daf-d39497ef0ad9', 'SLE', 'Sierra Leone'),
	(34704, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2eb94014-5004-4860-89b4-4e2239ab8ea8', 'SGP', 'Singapour'),
	(34705, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '975cd0ca-39d0-4526-88aa-825fffb375ae', 'SVK', 'Slovaquie'),
	(34706, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '3245766c-2cb9-4032-bb83-e981b271882b', 'VNM', 'Viet Nam'),
	(34707, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '51835b6c-e081-4c61-8942-ffbf1596ea2e', 'SVN', 'Slovénie'),
	(34708, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'b2a2aea9-4ed8-4ddd-821b-26b119e2970d', 'SOM', 'Somalie'),
	(34709, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'ab662da0-4b44-49be-a0ea-0d8240b98954', 'ZAF', 'Afrique du Sud'),
	(34710, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '76412f99-a1d6-4d71-b7b5-f7f4cca1ec5f', 'ZWE', 'Zimbabwe'),
	(34711, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '2fe7ad30-a2bd-429e-8736-3834a3e1cde3', 'ESP', 'Espagne'),
	(34712, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8a05f31c-f8f8-4713-9c9e-d8d55c70f98b', 'ESH', 'Sahara Occidental'),
	(34713, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '66ab8c89-a164-47ad-8370-588af7da1f2f', 'SDN', 'Soudan'),
	(34714, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'e4ce385f-b792-466f-8226-b36ac39e700f', 'SUR', 'Suriname'),
	(34715, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '05f38fc1-da7c-4692-9f6a-3fe84d37953c', 'SJM', 'Svalbard etÎle Jan Mayen'),
	(34716, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'd04807e9-2f41-45a8-9e3d-7018c9d439f1', 'SWZ', 'Swaziland'),
	(34717, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '906390c4-4538-4cca-b959-647a11315c3b', 'SWE', 'Suède'),
	(34718, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4ca51a2f-06bd-40c9-9ff2-b4d358f4561b', 'CHE', 'Suisse'),
	(34719, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c2fab299-d188-43cf-8692-26bc21d2f7f4', 'SYR', 'République Arabe Syrienne'),
	(34720, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'a283837e-43e1-402a-886d-b07c089ebede', 'TJK', 'Tadjikistan'),
	(34721, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4f39e70d-6e38-48b9-babf-88cbf2113990', 'THA', 'Thaïlande'),
	(34722, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8b0dd34e-5c83-4928-b7dc-fb37546fb171', 'TGO', 'Togo'),
	(34723, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c73edf86-bcca-462f-bc57-8c1d18b4fd5f', 'TKL', 'Tokelau'),
	(34724, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '7860fec5-b882-4993-8bca-d936e1f6625c', 'TON', 'Tonga'),
	(34725, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '8758ac23-0769-4176-9b20-5738a4b85903', 'TTO', 'Trinité-et-Tobago'),
	(34726, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '0d9b8b20-06ba-4fd7-9737-d5abc9246182', 'ARE', 'Émirats Arabes Unis'),
	(34727, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4ad41262-ee47-43b6-99cf-687f88d6da10', 'TUN', 'Tunisie'),
	(34728, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '714d641f-bb55-4b8c-bc9a-10c6e53e8ea8', 'TUR', 'Turquie'),
	(34729, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '7a43e039-4b96-4450-ba3c-8156015dd8c6', 'TKM', 'Turkménistan'),
	(34730, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'af41e1fc-7030-498a-a97e-c0d098743c67', 'TCA', 'Îles Turks et Caïques'),
	(34731, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '54d43fcb-80d2-4285-84b5-662ab86567bb', 'TUV', 'Tuvalu'),
	(34732, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '9db1382f-6938-474a-93bf-7d17520a721d', 'UGA', 'Ouganda'),
	(34733, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'b8ccbd38-5e98-4ca0-b50f-728c7140ca23', 'UKR', 'Ukraine'),
	(34734, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'e2a805fa-04ba-4693-b42a-046046d133a4', 'MKD', 'L\'ex-République Yougoslave de Macédoine'),
	(34735, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '130c6b49-982d-4007-a795-e4d91c5bff96', 'EGY', 'Égypte'),
	(34736, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'b074de13-3c26-45cf-ad38-014a296689b4', 'GBR', 'Royaume-Uni'),
	(34737, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'cf737c78-acbe-4107-ab52-db73475cab40', 'IMN', 'Île de Man'),
	(34738, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'fecd54da-567e-4041-83e7-7dfd864769f6', 'TZA', 'République-Unie de Tanzanie'),
	(34739, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '83469aa3-d791-4e7e-8dee-fa71bd5333d7', 'USA', 'États-Unis'),
	(34740, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'cb202927-900a-4570-a250-9559a5ef5ef0', 'VIR', 'Îles Vierges des États-Unis'),
	(34741, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'f68e993e-fb3f-4b25-85a2-35ff72a296ba', 'BFA', 'Burkina Faso'),
	(34742, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '94190fda-ac3f-40ba-8e9e-0e5af9a2f2ff', 'URY', 'Uruguay'),
	(34743, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '7de6602f-3f65-432a-925a-b9249f681a19', 'UZB', 'Ouzbékistan'),
	(34744, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'c202b6e8-6080-4fbb-b50a-0ae0382f4c6f', 'VEN', 'Venezuela'),
	(34745, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'e88b665e-f7b2-4483-a589-3ea0ac2249d4', 'WLF', 'Wallis et Futuna'),
	(34746, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '4fc27386-45db-47a3-a0de-a6fa4dd5e2cc', 'WSM', 'Samoa'),
	(34747, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', 'e8a3c10d-48c9-48a6-9688-7e8e2be0a940', 'YEM', 'Yémen'),
	(34748, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '26138000-24b5-4dfb-88e5-d564629d6222', 'SCG', 'Serbie-et-Monténégro'),
	(34749, '2023-02-23 11:26:42', b'0', '2023-02-23 11:26:42', '724e0c41-72ef-488d-883f-c0a4366016d5', 'ZMB', 'Zambie');
