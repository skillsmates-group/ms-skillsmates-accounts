SET GLOBAL FOREIGN_KEY_CHECKS=0;
SET GLOBAL collation_server = 'utf8mb4_general_ci';

DROP TABLE IF EXISTS `z_assistance`;
CREATE TABLE IF NOT EXISTS `z_assistance` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_assistance_uuid` (`uuid`),
  CONSTRAINT `FK_assistance_account` FOREIGN KEY (`account`) REFERENCES `z_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3;
