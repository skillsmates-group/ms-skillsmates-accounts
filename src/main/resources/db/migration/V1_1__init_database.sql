SET GLOBAL FOREIGN_KEY_CHECKS=0;
SET GLOBAL collation_server = 'utf8mb4_general_ci';

-- Dumping structure for table skillsmatesdb.z_activity_area
DROP TABLE IF EXISTS `z_activity_area`;
CREATE TABLE IF NOT EXISTS `z_activity_area` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `created_at` datetime NOT NULL,
     `deleted` bit(1) DEFAULT NULL,
     `updated_at` datetime DEFAULT NULL,
     `uuid` varchar(255) NOT NULL,
     `code` varchar(255) NOT NULL,
     `label` varchar(255) NOT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `UK_activity_area_uuid` (`uuid`),
     UNIQUE KEY `UK_activity_area_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_activity_area(id, uuid, created_at, updated_at, deleted, code, label)
SELECT id, id_server, now(), now(), false, upper(UUID()), label FROM activity_area;

-- Dumping structure for table skillsmatesdb.z_activity_sector
DROP TABLE IF EXISTS `z_activity_sector`;
CREATE TABLE IF NOT EXISTS `z_activity_sector` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `created_at` datetime NOT NULL,
     `deleted` bit(1) DEFAULT NULL,
     `updated_at` datetime DEFAULT NULL,
     `uuid` varchar(255) NOT NULL,
     `code` varchar(255) NOT NULL,
     `label` varchar(255) NOT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `UK_activity_sector_uuid` (`uuid`),
     UNIQUE KEY `UK_activity_sector_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_activity_sector(id, uuid, created_at, updated_at, deleted, code, label)
SELECT id, id_server, now(), now(), false, upper(UUID()), label FROM activity_sector;

-- Dumping structure for table skillsmatesdb.z_diploma_level
DROP TABLE IF EXISTS `z_diploma_level`;
CREATE TABLE IF NOT EXISTS `z_diploma_level` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `created_at` datetime NOT NULL,
     `deleted` bit(1) DEFAULT NULL,
     `updated_at` datetime DEFAULT NULL,
     `uuid` varchar(255) NOT NULL,
     `code` varchar(255) NOT NULL,
     `label` varchar(255) NOT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `UK_diploma_level_uuid` (`uuid`),
     UNIQUE KEY `UK_diploma_level_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_diploma_level(id, uuid, created_at, updated_at, deleted, code, label)
SELECT id, id_server, now(), now(), false, upper(UUID()), label FROM diploma;

-- Dumping structure for table skillsmatesdb.z_school_class
DROP TABLE IF EXISTS `z_school_class`;
CREATE TABLE IF NOT EXISTS `z_school_class` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `created_at` datetime NOT NULL,
     `deleted` bit(1) DEFAULT NULL,
     `updated_at` datetime DEFAULT NULL,
     `uuid` varchar(255) NOT NULL,
     `code` varchar(255) NOT NULL,
     `label` varchar(255) NOT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `UK_school_class_uuid` (`uuid`),
     UNIQUE KEY `UK_school_class_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_school_class(id, uuid, created_at, updated_at, deleted, code, label)
SELECT id, id_server, now(), now(), false, upper(UUID()), label FROM study_level;

-- Dumping structure for table skillsmatesdb.z_school_type
DROP TABLE IF EXISTS `z_school_type`;
CREATE TABLE IF NOT EXISTS `z_school_type` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `created_at` datetime NOT NULL,
     `deleted` bit(1) DEFAULT NULL,
     `updated_at` datetime DEFAULT NULL,
     `uuid` varchar(255) NOT NULL,
     `code` varchar(255) NOT NULL,
     `label` varchar(255) NOT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `UK_school_type_uuid` (`uuid`),
     UNIQUE KEY `UK_school_type_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_school_type(id, uuid, created_at, updated_at, deleted, code, label)
SELECT id, id_server, now(), now(), false, upper(UUID()), label FROM establishment_type;
INSERT INTO z_school_type(id, uuid, created_at, updated_at, deleted, code, label)
SELECT id+13, UUID(), now(), now(), false, upper(UUID()), label FROM establishment_certification_type;

-- Dumping structure for table skillsmatesdb.z_teaching_field
DROP TABLE IF EXISTS `z_teaching_field`;
CREATE TABLE IF NOT EXISTS `z_teaching_field` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `created_at` datetime NOT NULL,
     `deleted` bit(1) DEFAULT NULL,
     `updated_at` datetime DEFAULT NULL,
     `uuid` varchar(255) NOT NULL,
     `code` varchar(255) NOT NULL,
     `label` varchar(255) NOT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `UK_teaching_field_uuid` (`uuid`),
     UNIQUE KEY `UK_teaching_field_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_teaching_field(id, uuid, created_at, updated_at, deleted, code, label)
SELECT id, id_server, now(), now(), false, upper(UUID()), label FROM teaching_area_group;

-- Dumping structure for table skillsmatesdb.z_teaching_name
DROP TABLE IF EXISTS `z_teaching_name`;
CREATE TABLE IF NOT EXISTS `z_teaching_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `teaching_field` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_teaching_name_uuid` (`uuid`),
  UNIQUE KEY `UK_teaching_name_code` (`code`),
  KEY `FK_teaching_name_teaching_field` (`teaching_field`),
  CONSTRAINT `FK_teaching_name_teaching_field` FOREIGN KEY (`teaching_field`) REFERENCES `z_teaching_field` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_teaching_name(id, uuid, created_at, updated_at, deleted, code, label, teaching_field)
SELECT id, id_server, now(), now(), false, upper(UUID()), label, teaching_area_group FROM teaching_area;

-- Dumping structure for table skillsmatesdb.z_account
DROP TABLE IF EXISTS `z_account`;
CREATE TABLE IF NOT EXISTS `z_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `biography` longtext DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `connected` tinyint(1) DEFAULT 0,
  `country` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `last_connection_date` datetime DEFAULT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `current_company` varchar(255) DEFAULT NULL,
  `current_job` varchar(255) DEFAULT NULL,
  `hide_birthdate` bit(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_account_uuid` (`uuid`),
  UNIQUE KEY `UK_account_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_account (id, uuid, created_at, deleted, updated_at, active, address, biography, birthdate, city, connected, country, email, firstname, lastname, password, gender, phone_number, role, status, last_connection_date, avatar, current_company, current_job)
SELECT id, id_server, created_at, deleted, modified_at, active, address, biography, birthdate, city, connected, country, email, firstname, lastname, password, gender, phone_number, role, status, created_at, profile_picture, current_establishment_name, current_job_title FROM account;

UPDATE z_account SET gender = 'MALE' WHERE gender = '0';
UPDATE z_account SET gender = 'FEMALE' WHERE gender = '1';

UPDATE z_account SET status = 'STUDENT' WHERE status = '0';
UPDATE z_account SET status = 'TEACHER' WHERE status = '1';
UPDATE z_account SET status = 'PROFESSIONAL' WHERE status = '2';

-- Dumping structure for table skillsmatesdb.z_certification
DROP TABLE IF EXISTS `z_certification`;
CREATE TABLE IF NOT EXISTS `z_certification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `permanent` bit(1) DEFAULT NULL,
  `school_name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `account` int(11) DEFAULT NULL,
  `activity_area` int(11) DEFAULT NULL,
  `school_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`, `uuid`),
  UNIQUE KEY `UK_certification_uuid` (`uuid`),
  KEY `FK_certification_account` (`account`),
  KEY `FK_certification_activity_area` (`activity_area`),
  KEY `FK_certification_school_type` (`school_type`),
  CONSTRAINT `FK_certification_account` FOREIGN KEY (`account`) REFERENCES `z_account` (`id`),
  CONSTRAINT `FK_certification_activity_area` FOREIGN KEY (`activity_area`) REFERENCES `z_activity_area` (`id`),
  CONSTRAINT `FK_certification_school_type` FOREIGN KEY (`school_type`) REFERENCES `z_school_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_certification(id, uuid, created_at, updated_at, deleted, description, title, end_date, permanent, school_name, start_date, account, activity_area, school_type)
SELECT id, id_server, created_at, modified_at, deleted, description, entitled_certification, expiration_date, is_permanent, issuing_institution_name, obtained_date, account, activity_area, establishment_certification_type+13 FROM certification;

-- Dumping structure for table skillsmatesdb.z_diploma
DROP TABLE IF EXISTS `z_diploma`;
CREATE TABLE IF NOT EXISTS `z_diploma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `school_name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `account` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `school_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_diploma_uuid` (`uuid`),
  KEY `FK_diploma_account` (`account`),
  KEY `FK_diploma_level` (`level`),
  KEY `FK_diploma_school_type` (`school_type`),
  CONSTRAINT `FK_diploma_school_type` FOREIGN KEY (`school_type`) REFERENCES `z_school_type` (`id`),
  CONSTRAINT `FK_diploma_account` FOREIGN KEY (`account`) REFERENCES `z_account` (`id`),
  CONSTRAINT `FK_diploma_level` FOREIGN KEY (`level`) REFERENCES `z_diploma_level` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_diploma(id, uuid, created_at, updated_at, deleted, description, title, end_date, city, school_name, start_date, account, education, school_type, level)
SELECT id, id_server, created_at, modified_at, deleted, description, specialty, end_date, city, establishment_name, start_date, account, 'HIGHER', establishment_type, (SELECT id FROM z_diploma_level WHERE label=(SELECT label FROM diploma WHERE id = diploma)) FROM degree_obtained;

-- Dumping structure for table skillsmatesdb.z_job
DROP TABLE IF EXISTS `z_job`;
CREATE TABLE IF NOT EXISTS `z_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `company` varchar(255) NOT NULL,
  `current_job` bit(1) DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `account` int(11) DEFAULT NULL,
  `activity_area` int(11) DEFAULT NULL,
  `activity_sector` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_job_uuid` (`uuid`),
  KEY `FK_job_account` (`account`),
  KEY `FK_job_activity_area` (`activity_area`),
  KEY `FK_job_activity_sector` (`activity_sector`),
  CONSTRAINT `FK_job_activity_area` FOREIGN KEY (`activity_area`) REFERENCES `z_activity_area` (`id`),
  CONSTRAINT `FK_job_activity_sector` FOREIGN KEY (`activity_sector`) REFERENCES `z_activity_sector` (`id`),
  CONSTRAINT `FK_job_account` FOREIGN KEY (`account`) REFERENCES `z_account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_job(id, uuid, created_at, updated_at, deleted, description, title, end_date, city, company, start_date, account, current_job, activity_area, activity_sector)
SELECT id, id_server, created_at, modified_at, deleted, description, job_title, end_date, city, establishment_name, start_date, account, current_position, activity_area, activity_sector FROM professional;

-- Dumping structure for table skillsmatesdb.z_skill
DROP TABLE IF EXISTS `z_skill`;
CREATE TABLE IF NOT EXISTS `z_skill` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `discipline` varchar(255) DEFAULT NULL,
  `keywords` longtext DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_skill_uuid` (`uuid`),
  KEY `FK_skill_uuid` (`account`),
  CONSTRAINT `FK_skill_uuid` FOREIGN KEY (`account`) REFERENCES `z_account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_skill(id, uuid, created_at, updated_at, deleted, description, title, category, discipline, keywords, level, account)
SELECT id, id_server, created_at, modified_at, deleted, NULL, label, skill_mastered, discipline, keywords, level, account FROM skill;
UPDATE z_skill SET level = 'LEVEL_1' WHERE level = '1103';
UPDATE z_skill SET level = 'LEVEL_2' WHERE level = '1104';
UPDATE z_skill SET level = 'LEVEL_3' WHERE level = '1102';
UPDATE z_skill SET level = 'LEVEL_4' WHERE level = '1101';

UPDATE z_skill SET category = 'CATEGORY_1' WHERE category = '0';
UPDATE z_skill SET category = 'CATEGORY_2' WHERE category = '1';

-- Dumping structure for table skillsmatesdb.z_training
CREATE TABLE IF NOT EXISTS `z_training` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `school_class` int(11) DEFAULT NULL,
  `school_type` int(11) DEFAULT NULL,
  `teaching_name` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_training_uuid` (`uuid`),
  KEY `FK_training_account` (`account`),
  KEY `FK_training_level` (`level`),
  KEY `FK_training_school_class` (`school_class`),
  KEY `FK_training_school_type` (`school_type`),
  KEY `FK_training_teaching_name` (`teaching_name`),
  CONSTRAINT `FK_training_school_type` FOREIGN KEY (`school_type`) REFERENCES `z_school_type` (`id`),
  CONSTRAINT `FK_training_teaching_name` FOREIGN KEY (`teaching_name`) REFERENCES `z_teaching_name` (`id`),
  CONSTRAINT `FK_training_level` FOREIGN KEY (`level`) REFERENCES `z_diploma_level` (`id`),
  CONSTRAINT `FK_training_school_class` FOREIGN KEY (`school_class`) REFERENCES `z_school_class` (`id`),
  CONSTRAINT `FK_training_account` FOREIGN KEY (`account`) REFERENCES `z_account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_training(id, uuid, created_at, updated_at, deleted, description, title, city, school_name, account, education, level, school_class, school_type, teaching_name)
SELECT id, id_server, created_at, modified_at, deleted, description, specialty, city, establishment_name, account, 'HIGHER', targeted_diploma, study_level, establishment_type, teaching_area FROM higher_education;

INSERT INTO z_training(id, uuid, created_at, updated_at, deleted, description, title, city, school_name, account, education, level, school_class, school_type, teaching_name)
SELECT id, id_server, created_at, modified_at, deleted, description, specialty, city, establishment_name, account, 'SECONDARY', prepared_diploma, study_level, establishment_type, NULL FROM secondary_education;

-- Dumping structure for table skillsmatesdb.z_media_type
CREATE TABLE IF NOT EXISTS `z_media_type` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_media_type_uuid` (`uuid`),
  UNIQUE KEY `UK_media_type_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

UNLOCK TABLES;
INSERT INTO z_media_type(id, created_at, deleted, updated_at, uuid, code, label)
VALUES(1, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT', 'DOCUMENT'),
        (2, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_IMAGE', 'IMAGE'),
        (3, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_VIDEO', 'VIDEO'),
        (4, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_LINK', 'LINK'),
        (5, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_AUDIO', 'AUDIO');

-- Dumping structure for table skillsmatesdb.z_post_type
CREATE TABLE IF NOT EXISTS `z_post_type` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_post_type_uuid` (`uuid`),
  UNIQUE KEY `UK_post_type_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

UNLOCK TABLES;
INSERT INTO z_post_type(id, created_at, deleted, updated_at, uuid, code, label)
VALUES(1, NOW(), false, NOW(), UUID(), 'POST_TYPE_ARTICLE', 'ARTICLE'),
        (2, NOW(), false, NOW(), UUID(), 'POST_TYPE_STATUS', 'STATUS');

-- Dumping structure for table skillsmatesdb.z_interaction_type
CREATE TABLE IF NOT EXISTS `z_interaction_type` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_interaction_type_uuid` (`uuid`),
  UNIQUE KEY `UK_interaction_type_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

UNLOCK TABLES;
INSERT INTO z_interaction_type(id, created_at, deleted, updated_at, uuid, code, label)
VALUES(1, NOW(), false, NOW(), UUID(), 'INTERACTION_TYPE_LIKE', 'LIKE'),
        (2, NOW(), false, NOW(), UUID(), 'INTERACTION_TYPE_COMMENT', 'COMMENT'),
        (3, NOW(), false, NOW(), UUID(), 'INTERACTION_TYPE_SHARE', 'SHARE'),
        (4, NOW(), false, NOW(), UUID(), 'INTERACTION_TYPE_FOLLOWER', 'FOLLOWER'),
        (5, NOW(), false, NOW(), UUID(), 'INTERACTION_TYPE_FAVORITE', 'FAVORITE'),
        (6, NOW(), false, NOW(), UUID(), 'INTERACTION_TYPE_POST', 'POST');

-- Dumping structure for table skillsmatesdb.z_document_type
CREATE TABLE IF NOT EXISTS `z_document_type` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `media_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_document_type_uuid` (`uuid`),
  UNIQUE KEY `UK_document_type_code` (`code`),
  KEY `FK_document_type_media_type` (`media_type`),
  CONSTRAINT `FK_document_type_media_type` FOREIGN KEY (`media_type`) REFERENCES `z_media_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

UNLOCK TABLES;
INSERT INTO z_document_type(id, created_at, deleted, updated_at, uuid, code, label, media_type)
VALUES(1, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_01', 'Cours', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),
        (2, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_02', 'Exercices et corrections', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),
        (3, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_03', 'Sujets d\'examens et corrections', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),
        (4, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_04', 'Fiches et astuces de revisions', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),
        (5, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_05', 'Mémoires, Exposés, présentation', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),
        (6, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_06', 'Diplômes, certification, CV et lettre de motivation', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),
        (7, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_07', 'Réalisations professionnelles et fiches métiers', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),
        (8, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_08', 'Travaux et articles de recherches / livres', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),
        (9, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_DOCUMENT_09', 'Autres', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_DOCUMENT')),

        (10, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_IMAGE_01', 'Documents photographiés', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_IMAGE')),
        (11, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_IMAGE_02', 'Mes photos', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_IMAGE')),
        (12, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_IMAGE_03', 'Graphiques, images et dessins', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_IMAGE')),
        (13, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_IMAGE_04', 'Affiches, flyers, évènements', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_IMAGE')),
        (14, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_IMAGE_05', 'Autres', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_IMAGE')),

        (15, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_VIDEO_01', 'Vidéos éducatives et pédagogiques', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_VIDEO')),
        (16, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_VIDEO_02', 'Conférences', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_VIDEO')),
        (17, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_VIDEO_03', 'Vidéos d\'actualités et dédats', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_VIDEO')),
        (18, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_VIDEO_04', 'Ma chaine vidéo', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_VIDEO')),
        (19, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_VIDEO_05', 'Mes vidéos', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_VIDEO')),
        (20, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_VIDEO_06', 'Autres', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_VIDEO')),

        (21, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_LINK_01', 'Articles d\'actualité et éducatifs', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_LINK')),
        (22, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_LINK_02', 'Outils', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_LINK')),
        (23, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_LINK_03', 'Blogs et revues scientifiques', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_LINK')),
        (24, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_LINK_04', 'Sites web et plateformes', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_LINK')),
        (25, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_LINK_05', 'Questionnaires et jeux', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_LINK')),
        (26, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_LINK_06', 'Livres', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_LINK')),
        (27, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_LINK_07', 'Autres', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_LINK')),

        (28, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_AUDIO_01', 'Audios éducatifs et pédagogiques', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_AUDIO')),
        (29, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_AUDIO_02', 'Conférences', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_AUDIO')),
        (30, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_AUDIO_03', 'Audios d\'actualités et débats', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_AUDIO')),
        (31, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_AUDIO_04', 'Podcasts', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_AUDIO')),
        (32, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_AUDIO_05', 'Mes audios', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_AUDIO')),
        (33, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_AUDIO_06', 'Autres', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_AUDIO'));

-- Dumping structure for table skillsmatesdb.z_interaction
CREATE TABLE IF NOT EXISTS `z_interaction` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `entity` varchar(255) NOT NULL,
  `emitter` int(11) DEFAULT NULL,
  `interaction_type` int(11) DEFAULT NULL,
  `receiver` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_interaction_uuid` (`uuid`),
  KEY `FK_interaction_emitter` (`emitter`),
  KEY `FK_interaction_interaction_type` (`interaction_type`),
  KEY `FK_interaction_receiver` (`receiver`),
  CONSTRAINT `FK_interaction_emitter` FOREIGN KEY (`emitter`) REFERENCES `z_account` (`id`),
  CONSTRAINT `FK_interaction_interaction_type` FOREIGN KEY (`interaction_type`) REFERENCES `z_interaction_type` (`id`),
  CONSTRAINT `FK_interaction_receiver` FOREIGN KEY (`receiver`) REFERENCES `z_account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

UNLOCK TABLES;
INSERT INTO z_interaction (id, uuid, created_at, deleted, updated_at, active, content, emitter, entity, receiver, interaction_type)
SELECT DISTINCT(si.id), si.id_server, si.created_at, si.deleted, si.modified_at, si.active, si.content, (SELECT id FROM z_account WHERE id = si.emitter_account), sim.element, (SELECT id FROM z_account WHERE id = si.receiver_account), si.social_interaction_type FROM social_interaction si JOIN social_interaction_map sim ON si.id = sim.social_interaction;

-- Create tmp_multimedia table
DROP TABLE IF EXISTS tmp_multimedia;
CREATE TABLE tmp_multimedia LIKE multimedia;
INSERT INTO tmp_multimedia SELECT * FROM multimedia;
UPDATE tmp_multimedia SET type = "PICTURE" WHERE type = "IMAGE";
UPDATE tmp_multimedia SET checksum = CONCAT(LOWER(TYPE), "_", checksum);

-- Dumping structure for table skillsmatesdb.z_post
CREATE TABLE IF NOT EXISTS `z_post` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `discipline` varchar(255) DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `account` int(11) DEFAULT NULL,
  `document_type` int(11) DEFAULT NULL,
  `post_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_post_uuid` (`uuid`),
  KEY `FK_post_account` (`account`),
  KEY `FK_post_document_type` (`document_type`),
  KEY `FK_post_post_type` (`post_type`),
  CONSTRAINT `FK_post_document_type` FOREIGN KEY (`document_type`) REFERENCES `z_document_type` (`id`),
  CONSTRAINT `FK_post_post_type` FOREIGN KEY (`post_type`) REFERENCES `z_post_type` (`id`),
  CONSTRAINT `FK_post_account` FOREIGN KEY (`account`) REFERENCES `z_account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

UNLOCK TABLES;
INSERT INTO z_post(id, uuid, created_at, updated_at, deleted, description, title, discipline, keywords, document, link, thumbnail, document_type, account, post_type)
SELECT p.id, p.id_server, p.created_at, p.modified_at, p.deleted, p.description, p.title, p.discipline, p.keywords, m.id_server, CONCAT(m.checksum, ".", m.extension),CONCAT(m.checksum, ".", m.extension), p.media_subtype, p.account, (SELECT id FROM z_post_type WHERE label = p.type)
FROM post p
LEFT JOIN post_multimedia pm ON p.id = pm.post_id
LEFT JOIN tmp_multimedia m ON m.id = pm.multimedia_id
WHERE m.metadata IS NULL;

INSERT INTO z_post(id, uuid, created_at, updated_at, deleted, description, title, discipline, keywords, document, link, thumbnail, document_type, account, post_type)
SELECT p.id, p.id_server, p.created_at, p.modified_at, p.deleted, p.description, p.title, p.discipline, p.keywords, m.id_server, m.url, md.image, p.media_subtype, p.account, (SELECT id FROM z_post_type WHERE label = p.type)
FROM post p
LEFT JOIN post_multimedia pm ON p.id = pm.post_id
LEFT JOIN tmp_multimedia m ON m.id = pm.multimedia_id
LEFT JOIN metadata md ON md.id = m.metadata
WHERE m.metadata IS NOT NULL;

UPDATE z_post SET document_type = 1 WHERE document_type = 1467;
UPDATE z_post SET document_type = 2 WHERE document_type = 1468;
UPDATE z_post SET document_type = 3 WHERE document_type = 1469;
UPDATE z_post SET document_type = 4 WHERE document_type = 1470;
UPDATE z_post SET document_type = 5 WHERE document_type = 1471;
UPDATE z_post SET document_type = 6 WHERE document_type = 1472;
UPDATE z_post SET document_type = 7 WHERE document_type = 1473;
UPDATE z_post SET document_type = 8 WHERE document_type = 1474;
UPDATE z_post SET document_type = 9 WHERE document_type = 1475;

UPDATE z_post SET document_type = 10 WHERE document_type = 1495;
UPDATE z_post SET document_type = 11 WHERE document_type = 1496;
UPDATE z_post SET document_type = 12 WHERE document_type = 1497;
UPDATE z_post SET document_type = 13 WHERE document_type = 1498;
UPDATE z_post SET document_type = 14 WHERE document_type = 1499;

UPDATE z_post SET document_type = 15 WHERE document_type = 1476;
UPDATE z_post SET document_type = 16 WHERE document_type = 1477;
UPDATE z_post SET document_type = 17 WHERE document_type = 1478;
UPDATE z_post SET document_type = 18 WHERE document_type = 1479;
UPDATE z_post SET document_type = 19 WHERE document_type = 1480;
UPDATE z_post SET document_type = 20 WHERE document_type = 1481;

UPDATE z_post SET document_type = 28 WHERE document_type = 1482;
UPDATE z_post SET document_type = 29 WHERE document_type = 1483;
UPDATE z_post SET document_type = 30 WHERE document_type = 1484;
UPDATE z_post SET document_type = 31 WHERE document_type = 1485;
UPDATE z_post SET document_type = 32 WHERE document_type = 1486;
UPDATE z_post SET document_type = 33 WHERE document_type = 1487;

UPDATE z_post SET document_type = 21 WHERE document_type = 1488;
UPDATE z_post SET document_type = 22 WHERE document_type = 1489;
UPDATE z_post SET document_type = 23 WHERE document_type = 1490;
UPDATE z_post SET document_type = 24 WHERE document_type = 1491;
UPDATE z_post SET document_type = 25 WHERE document_type = 1492;
UPDATE z_post SET document_type = 26 WHERE document_type = 1493;
UPDATE z_post SET document_type = 27 WHERE document_type = 1494;

-- Dumping structure for table skillsmatesdb.z_origin
CREATE TABLE IF NOT EXISTS `z_origin` (
    `id` int(11) NOT NULL,
    `created_at` datetime NOT NULL,
    `deleted` bit(1) DEFAULT NULL,
    `updated_at` datetime DEFAULT NULL,
    `uuid` varchar(255) NOT NULL,
    `label` varchar(255) NOT NULL,
    `code` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UK_origin_uuid` (`uuid`),
    UNIQUE KEY `UK_origin_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;

INSERT INTO z_origin (id, created_at, deleted, uuid, label, code) VALUES (1, now(), false, UUID() , 'AVATAR', 'ORIGIN_AVATAR');
INSERT INTO z_origin (id, created_at, deleted, uuid, label, code) VALUES (2, now(), false, UUID() , 'CHAT', 'ORIGIN_CHAT');
INSERT INTO z_origin (id, created_at, deleted, uuid, label, code) VALUES (3, now(), false, UUID() , 'POST', 'ORIGIN_POST');
INSERT INTO z_origin (id, created_at, deleted, uuid, label, code) VALUES (4, now(), false, UUID() , 'STATUS', 'ORIGIN_STATUS');

-- Dumping structure for table skillsmatesdb.z_link_metadata
CREATE TABLE IF NOT EXISTS `z_link_metadata` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `availability` varchar(255) DEFAULT NULL,
  `canonical` varchar(255) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_link_metadata_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;
INSERT INTO z_link_metadata (id, created_at, deleted, updated_at, uuid, audio, author, availability, canonical, description, image, keywords, title, url, video)
SELECT id, created_at, deleted, modified_at, id_server, audio, author, availability, canonical, description, image, keywords, title, url, video FROM metadata;

-- Dumping structure for table skillsmatesdb.z_file_metadata
CREATE TABLE IF NOT EXISTS `z_file_metadata` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `customer_file_name` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `size` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_file_metadata_uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;

INSERT INTO z_file_metadata (id, created_at, deleted, updated_at, uuid, customer_file_name, extension, mime_type, name, path, checksum, size)
SELECT id, created_at, deleted, modified_at, id_server, name, extension, mime_type, name, directory, checksum, 0 FROM tmp_multimedia WHERE metadata IS NULL;

-- Dumping structure for table skillsmatesdb.z_document
CREATE TABLE IF NOT EXISTS `z_document` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `media_type` int(11) DEFAULT NULL,
  `origin` int(11) DEFAULT NULL,
  `file_metadata` int(11) DEFAULT NULL,
  `link_metadata` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_document_uuid` (`uuid`),
  KEY `FK_document_file_metadata` (`file_metadata`),
  KEY `FK_document_link_metadata` (`link_metadata`),
  KEY `FK_document_origin` (`origin`),
  KEY `FK_document_media_type` (`media_type`),
  CONSTRAINT `FK_document_origin` FOREIGN KEY (`origin`) REFERENCES `z_origin` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_document_link_metadata` FOREIGN KEY (`link_metadata`) REFERENCES `z_link_metadata` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_document_file_metadata` FOREIGN KEY (`file_metadata`) REFERENCES `z_file_metadata` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_document_media_type` FOREIGN KEY (`media_type`) REFERENCES `z_media_type` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UNLOCK TABLES;

INSERT INTO z_document (id, created_at, deleted, updated_at, uuid, account, media_type, origin, file_metadata, link_metadata)
SELECT id, created_at, deleted, modified_at, id_server, (SELECT uuid FROM z_account WHERE id = account), (SELECT id FROM z_media_type WHERE label = type), (SELECT id FROM z_origin WHERE label = origin), id, NULL FROM tmp_multimedia WHERE metadata IS NULL;

INSERT INTO z_document (id, created_at, deleted, updated_at, uuid, account, media_type, origin, file_metadata, link_metadata)
SELECT id, created_at, deleted, modified_at, id_server, (SELECT uuid FROM z_account WHERE id = account), (SELECT id FROM z_media_type WHERE label = type), (SELECT id FROM z_origin WHERE label = origin), NULL, metadata  FROM tmp_multimedia WHERE metadata IS NOT NULL;


