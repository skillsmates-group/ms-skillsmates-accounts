SET GLOBAL FOREIGN_KEY_CHECKS=0;
SET GLOBAL collation_server = 'utf8mb4_general_ci';

CREATE INDEX idx_account_uuid ON z_account (uuid);
CREATE INDEX idx_post_uuid ON z_post (uuid);
CREATE INDEX idx_interaction_uuid ON z_interaction (uuid);
CREATE INDEX idx_document_uuid ON z_document (uuid);
CREATE INDEX idx_activity_area_uuid ON z_activity_area (uuid);
CREATE INDEX idx_activity_sector_uuid ON z_activity_sector (uuid);
CREATE INDEX idx_assistance_uuid ON z_assistance (uuid);
CREATE INDEX idx_country_uuid ON z_country (uuid);
CREATE INDEX idx_certification_uuid ON z_certification (uuid);
CREATE INDEX idx_diploma_uuid ON z_diploma (uuid);
CREATE INDEX idx_job_uuid ON z_job (uuid);
CREATE INDEX idx_skill_uuid ON z_skill (uuid);
CREATE INDEX idx_training_uuid ON z_training (uuid);
CREATE INDEX idx_post_type_uuid ON z_post_type (uuid);
CREATE INDEX idx_media_type_uuid ON z_media_type (uuid);
CREATE INDEX idx_document_type_uuid ON z_document_type (uuid);