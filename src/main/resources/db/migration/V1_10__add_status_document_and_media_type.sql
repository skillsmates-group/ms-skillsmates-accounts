UNLOCK TABLES;
INSERT INTO z_media_type(id, created_at, deleted, updated_at, uuid, code, label)
VALUES (6, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_STATUS', 'STATUS');

UNLOCK TABLES;
INSERT INTO z_document_type(id, created_at, deleted, updated_at, uuid, code, label, media_type)
VALUES(34, NOW(), false, NOW(), UUID(), 'MEDIA_TYPE_STATUS_01', 'Status', (SELECT id FROM z_media_type WHERE code = 'MEDIA_TYPE_STATUS'));
