package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.SchoolTypeRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class SchoolTypeServiceImplTest
    extends CommonParameterTest<SchoolTypeEntity, SchoolTypeServiceImpl, SchoolTypeRepository> {

  @InjectMocks private SchoolTypeServiceImpl objectToTest;
  @Mock private SchoolTypeRepository repository;

  @Override
  protected SchoolTypeEntity newInstance() {
    return new SchoolTypeEntity();
  }

  @Override
  protected SchoolTypeServiceImpl getObjectToTest() {
    return objectToTest;
  }

  @Override
  protected SchoolTypeRepository getRepository() {
    return repository;
  }
}
