package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivitySectorEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.ActivitySectorRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class ActivitySectorServiceImplTest
    extends CommonParameterTest<
        ActivitySectorEntity, ActivitySectorServiceImpl, ActivitySectorRepository> {

  @InjectMocks private ActivitySectorServiceImpl objectToTest;
  @Mock private ActivitySectorRepository repository;

  @Override
  protected ActivitySectorEntity newInstance() {
    return new ActivitySectorEntity();
  }

  @Override
  protected ActivitySectorServiceImpl getObjectToTest() {
    return objectToTest;
  }

  @Override
  protected ActivitySectorRepository getRepository() {
    return repository;
  }
}
