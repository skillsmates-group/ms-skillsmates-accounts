package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.attributes.TrainingEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.TeachingFieldEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.TeachingNameEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.TeachingNameRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TeachingNameServiceImplTest
    extends CommonParameterTest<
        TeachingNameEntity, TeachingNameServiceImpl, TeachingNameRepository> {

  @InjectMocks private TeachingNameServiceImpl objectToTest;
  @Mock private TeachingNameRepository repository;
  @Mock private TeachingFieldServiceImpl teachingFieldService;

  @Override
  protected TeachingNameEntity newInstance() {
    return new TeachingNameEntity();
  }

  @Override
  protected TeachingNameServiceImpl getObjectToTest() {
    return objectToTest;
  }

  @Override
  protected TeachingNameRepository getRepository() {
    return repository;
  }

  @Test
  void test_find_create_using_training() {
    TrainingEntity training = new TrainingEntity();
    TeachingNameEntity teachingName = new TeachingNameEntity();
    TeachingNameEntity savedTeachingName = new TeachingNameEntity();
    TeachingFieldEntity teachingField = new TeachingFieldEntity();
    TeachingFieldEntity savedTeachingField = new TeachingFieldEntity();

    String code = "CODE";
    teachingName.setTeachingField(teachingField);
    teachingName.setCode(code);
    training.setTeachingName(teachingName);

    when(teachingFieldService.findOrCreate(teachingField)).thenReturn(savedTeachingField);
    when(repository.findByDeletedFalseAndCode(code)).thenReturn(Optional.of(savedTeachingName));

    objectToTest.findOrCreate(training);

    verify(repository)
        .save(
            argThat(
                teachingNameEntity -> {
                  assertSame(savedTeachingName, teachingNameEntity);
                  assertSame(savedTeachingField, teachingNameEntity.getTeachingField());
                  return true;
                }));
    verifyNoMoreInteractions(teachingFieldService);
  }
}
