package com.yamogroup.skillsmates.accounts.services;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.GenderEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.account.RoleEnum;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountCreationResource;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResource;

import java.util.UUID;

public class AccountFixtures {
    public static final String NOT_EXISTED_ACCOUNT_UUID = "555f90a7-3a4b-4510-a45z-df5b2905d3a8";
    public static final String ACCOUNT_UUID = "555f90a7-3a4b-4510-a45f-df5b2905d3a9";
    public static final String ACCOUNT_FIRSTNAME = "Samantha";
    public static final String ACCOUNT_LASTNAME = "EDIMA";
    public static final String EMAIL = "samantha.edima@yopmail.com";


    public static AccountEntity generateAccountEntity() {
        AccountEntity entity = new AccountEntity();
        entity.setFirstname(ACCOUNT_FIRSTNAME);
        entity.setLastname(ACCOUNT_LASTNAME);
        entity.setEmail(EMAIL);
        entity.setPassword(EMAIL);
        return entity;
    }

    public static AccountCreationResource generateCreateAccountDataResponse() {
        AccountCreationResource response = new AccountCreationResource();
        response.setFirstname(ACCOUNT_FIRSTNAME);
        response.setLastname(ACCOUNT_LASTNAME);
        response.setEmail(EMAIL);
        response.setDeleted(false);
        response.setUuid(UUID.randomUUID().toString());
        return response;
    }

    public static AccountDetailsResource generateDetailAccountDataResponse() {
        AccountDetailsResource response = new AccountDetailsResource();
        response.setUuid(ACCOUNT_UUID);
        response.setActive(true);
        response.setFirstname(ACCOUNT_FIRSTNAME);
        response.setLastname(ACCOUNT_LASTNAME);
        response.setEmail(EMAIL);
        response.setDeleted(false);
        response.setAddress("Mimboman");
        response.setCity("Yaounde");
        response.setCountry("Cameroun");
        response.setGender(GenderEnum.FEMALE);
        response.setPhoneNumber("695 458 632");
        response.setRole(RoleEnum.USER);
        return response;
    }
}
