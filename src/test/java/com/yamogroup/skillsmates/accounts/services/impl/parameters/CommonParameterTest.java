package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ParameterEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.ParameterRepository;
import com.yamogroup.skillsmates.accounts.helpers.Pager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public abstract class CommonParameterTest<
    ENTITY extends ParameterEntity,
    SERVICE extends AbstractParameterServiceImpl<ENTITY>,
    REPOSITORY extends ParameterRepository<ENTITY>> {

  @AfterEach
  void checkNoMoreInteraction() {
    verifyNoMoreInteractions(getRepository());
  }

  public static String PARAMETER_UUID = "6a98bee3-6745-491c-bb24-cefe8be856ec";

  @Test
  void test_find_or_create_case_find() {
    String code = "CODE";
    ENTITY request = newInstance();
    ENTITY existing = newInstance();
    request.setCode(code);
    when(getRepository().findByDeletedFalseAndCode(code)).thenReturn(Optional.of(existing));

    ENTITY response = getObjectToTest().findOrCreate(request);

    assertNotSame(request, response);
    assertSame(existing, response);
  }

  @Test
  void test_find_or_create_case_create() {
    String code = "CODE";
    ENTITY request = newInstance();
    request.setCode(code);
    request.setUuid(PARAMETER_UUID);
    when(getRepository().findByDeletedFalseAndCode(code)).thenReturn(Optional.empty());

    getObjectToTest().findOrCreate(request);
    verify(getRepository())
        .save(
            argThat(
                activityArea -> {
                  assertEquals(
                      PARAMETER_UUID.replace("-", "").toUpperCase(), activityArea.getCode());
                  return true;
                }));
  }

  @Test
  void test_find_all() {
    ENTITY entity = newInstance();

    when(getRepository().findByDeletedFalse(Pager.of()))
        .thenReturn(Optional.of(new PageImpl<>(List.of(entity))));

    Page<ENTITY> response = getObjectToTest().findAll();

    assertEquals(1, response.getNumberOfElements());
    assertEquals(List.of(entity), response.getContent());
  }

  @Test
  void test_find_all_with_pageable() {
    Pageable pageable = PageRequest.of(1, 10);
    ENTITY entity = newInstance();

    when(getRepository().findByDeletedFalse(pageable))
        .thenReturn(Optional.of(new PageImpl<>(List.of(entity))));

    Page<ENTITY> response = getObjectToTest().findAll(pageable);

    assertEquals(1, response.getNumberOfElements());
    assertEquals(List.of(entity), response.getContent());
  }

  protected abstract ENTITY newInstance();

  protected abstract SERVICE getObjectToTest();

  protected abstract REPOSITORY getRepository();
}
