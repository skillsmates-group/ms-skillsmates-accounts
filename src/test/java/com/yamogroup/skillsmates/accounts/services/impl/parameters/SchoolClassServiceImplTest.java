package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolClassEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.SchoolClassRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class SchoolClassServiceImplTest
    extends CommonParameterTest<SchoolClassEntity, SchoolClassServiceImpl, SchoolClassRepository> {

  @InjectMocks private SchoolClassServiceImpl objectToTest;
  @Mock private SchoolClassRepository repository;

  @Override
  protected SchoolClassEntity newInstance() {
    return new SchoolClassEntity();
  }

  @Override
  protected SchoolClassServiceImpl getObjectToTest() {
    return objectToTest;
  }

  @Override
  protected SchoolClassRepository getRepository() {
    return repository;
  }
}
