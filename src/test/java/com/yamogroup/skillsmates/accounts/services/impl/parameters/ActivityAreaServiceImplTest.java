package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.ActivityAreaRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class ActivityAreaServiceImplTest
    extends CommonParameterTest<
        ActivityAreaEntity, ActivityAreaServiceImpl, ActivityAreaRepository> {

  @InjectMocks private ActivityAreaServiceImpl objectToTest;
  @Mock private ActivityAreaRepository repository;

  @Override
  protected ActivityAreaEntity newInstance() {
    return new ActivityAreaEntity();
  }

  @Override
  protected ActivityAreaServiceImpl getObjectToTest() {
    return objectToTest;
  }

  @Override
  protected ActivityAreaRepository getRepository() {
    return repository;
  }
}
