package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.DiplomaLevelEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.DiplomaLevelRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class DiplomaLevelServiceImplTest
    extends CommonParameterTest<
        DiplomaLevelEntity, DiplomaLevelServiceImpl, DiplomaLevelRepository> {

  @InjectMocks private DiplomaLevelServiceImpl objectToTest;
  @Mock private DiplomaLevelRepository repository;

  @Override
  protected DiplomaLevelEntity newInstance() {
    return new DiplomaLevelEntity();
  }

  @Override
  protected DiplomaLevelServiceImpl getObjectToTest() {
    return objectToTest;
  }

  @Override
  protected DiplomaLevelRepository getRepository() {
    return repository;
  }
}
