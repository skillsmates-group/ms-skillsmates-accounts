package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.CountryEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.CountryRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class CountryServiceImplTest
    extends CommonParameterTest<CountryEntity, CountryServiceImpl, CountryRepository> {

  @InjectMocks private CountryServiceImpl objectToTest;
  @Mock private CountryRepository repository;

  @Override
  protected CountryEntity newInstance() {
    return new CountryEntity();
  }

  @Override
  protected CountryServiceImpl getObjectToTest() {
    return objectToTest;
  }

  @Override
  protected CountryRepository getRepository() {
    return repository;
  }
}
