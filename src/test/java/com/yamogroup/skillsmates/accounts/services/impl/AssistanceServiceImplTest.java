package com.yamogroup.skillsmates.accounts.services.impl;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.AssistanceEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.AssistanceRepository;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.yamogroup.skillsmates.accounts.services.AccountFixtures.ACCOUNT_UUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AssistanceServiceImplTest {

  @InjectMocks private AssistanceServiceImpl objectToTest;

  @Mock private AssistanceRepository assistanceRepository;
  @Mock private AccountRepository accountRepository;
  @Mock private EmailServiceHttp emailServiceHttp;
  @Mock private CryptUtil cryptUtil;

  @Test
  void test_create_fails_when_account_not_found() {
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(
            NotFoundException.class,
            () -> objectToTest.create(new AssistanceEntity(), ACCOUNT_UUID));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
    verify(accountRepository).findByUuid(ACCOUNT_UUID);
  }

  @Test
  void test_create() {
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));

    objectToTest.create(new AssistanceEntity(), ACCOUNT_UUID);
    verify(accountRepository).findByUuid(ACCOUNT_UUID);
    verify(assistanceRepository)
        .save(
            argThat(
                assistanceEntity -> {
                  assertSame(account, assistanceEntity.getAccount());
                  return true;
                }));
  }
}
