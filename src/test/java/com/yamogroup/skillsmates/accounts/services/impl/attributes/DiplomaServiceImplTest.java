package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.DiplomaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.DiplomaLevelEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.DiplomaRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.helpers.Constants;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.ActivityAreaServiceImpl;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.DiplomaLevelServiceImpl;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.SchoolTypeServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.yamogroup.skillsmates.accounts.services.AccountFixtures.ACCOUNT_UUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class DiplomaServiceImplTest {

  @InjectMocks private DiplomaServiceImpl objectToTest;
  @Mock private AccountRepository accountRepository;
  @Mock private DiplomaRepository diplomaRepository;
  @Mock private SchoolTypeServiceImpl schoolTypeService;
  @Mock private ActivityAreaServiceImpl activityAreaService;
  @Mock private DiplomaLevelServiceImpl diplomaLevelService;
  public static String DIPLOMA_UUID = "b89f4f96-0858-4f04-a202-dddd3bfaa9b6";

  @AfterEach
  void checkNoMoreInteraction() {
    verifyNoMoreInteractions(
        accountRepository,
        diplomaRepository,
        schoolTypeService,
        activityAreaService,
        diplomaLevelService);
  }

  @Test
  void test_save_fails_when_owner_not_exist() {
    DiplomaEntity diplomaEntity = new DiplomaEntity();
    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    DiplomaLevelEntity diplomaLevel = new DiplomaLevelEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);

    diplomaEntity.setSchoolType(schoolType);
    diplomaEntity.setLevel(diplomaLevel);
    diplomaEntity.setActivityArea(activityArea);
    diplomaEntity.setAccount(account);

    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(diplomaLevelService.findOrCreate(diplomaLevel)).thenReturn(diplomaLevel);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.save(diplomaEntity));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_save() {
    DiplomaEntity diplomaEntity = new DiplomaEntity();
    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    DiplomaLevelEntity diplomaLevel = new DiplomaLevelEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);

    diplomaEntity.setSchoolType(schoolType);
    diplomaEntity.setLevel(diplomaLevel);
    diplomaEntity.setActivityArea(activityArea);
    diplomaEntity.setAccount(account);

    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(diplomaLevelService.findOrCreate(diplomaLevel)).thenReturn(diplomaLevel);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));

    objectToTest.save(diplomaEntity);
    verify(diplomaRepository)
        .save(
            argThat(
                diploma -> {
                  assertNotNull(diploma.getUuid());
                  assertSame(diplomaEntity.getSchoolType(), diploma.getSchoolType());
                  assertSame(diplomaEntity.getLevel(), diploma.getLevel());
                  assertSame(diplomaEntity.getActivityArea(), diploma.getActivityArea());
                  return true;
                }));
  }

  @Test
  void test_update_fails_when_diploma_not_exist() {
    DiplomaEntity newDiploma = new DiplomaEntity();
    when(diplomaRepository.findByDeletedFalseAndUuid(DIPLOMA_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.update(DIPLOMA_UUID, newDiploma));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_update_throws_exception_when_not_authorize() {
    DiplomaEntity newDiploma = new DiplomaEntity();
    DiplomaEntity oldDiploma = new DiplomaEntity();
    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid("test");
    oldOwner.setUuid("test1");
    newDiploma.setAccount(newOwner);
    oldDiploma.setAccount(oldOwner);

    when(diplomaRepository.findByDeletedFalseAndUuid(DIPLOMA_UUID))
        .thenReturn(Optional.of(oldDiploma));

    ForbiddenException expectedException =
        assertThrows(ForbiddenException.class, () -> objectToTest.update(DIPLOMA_UUID, newDiploma));
    assertEquals(
        "Ce compte n'a pas les droit de modifier ce diplome", expectedException.getMessage());
  }

  @Test
  void test_update() throws BadRequestException {
    DiplomaEntity newDiploma = new DiplomaEntity();
    DiplomaEntity oldDiploma = new DiplomaEntity();

    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid(ACCOUNT_UUID);
    oldOwner.setUuid(ACCOUNT_UUID);

    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    DiplomaLevelEntity diplomaLevel = new DiplomaLevelEntity();

    newDiploma.setAccount(newOwner);
    oldDiploma.setAccount(oldOwner);
    newDiploma.setLevel(diplomaLevel);
    newDiploma.setSchoolType(schoolType);
    newDiploma.setActivityArea(activityArea);
    newDiploma.setSchoolName("schoolName");
    newDiploma.setCity("Yaoundé");
    newDiploma.setDescription("new Description");
    newDiploma.setEducation(EducationEnum.SECONDARY);
    newDiploma.setStartDate(new Date());
    newDiploma.setEndDate(new Date());

    when(diplomaRepository.findByDeletedFalseAndUuid(DIPLOMA_UUID))
        .thenReturn(Optional.of(oldDiploma));
    when(diplomaLevelService.findOrCreate(diplomaLevel)).thenReturn(diplomaLevel);
    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);

    objectToTest.update(DIPLOMA_UUID, newDiploma);
    verify(diplomaRepository)
        .save(
            argThat(
                diploma -> {
                  assertSame(newDiploma.getLevel(), diploma.getLevel());
                  assertSame(newDiploma.getSchoolType(), diploma.getSchoolType());
                  assertSame(newDiploma.getActivityArea(), diploma.getActivityArea());
                  assertEquals(newDiploma.getSchoolName(), diploma.getSchoolName());
                  assertEquals(newDiploma.getCity(), diploma.getCity());
                  assertEquals(newDiploma.getEducation(), diploma.getEducation());
                  assertSame(newDiploma.getStartDate(), diploma.getStartDate());
                  assertSame(newDiploma.getEndDate(), diploma.getEndDate());
                  assertEquals(newDiploma.getDescription(), diploma.getDescription());
                  return true;
                }));
  }

  @Test
  void test_delete_fails_when_diploma_not_exist() {
    when(diplomaRepository.findByDeletedFalseAndUuid(DIPLOMA_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.delete(DIPLOMA_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_delete() {
    DiplomaEntity diplomaEntity = new DiplomaEntity();
    when(diplomaRepository.findByDeletedFalseAndUuid(DIPLOMA_UUID))
        .thenReturn(Optional.of(diplomaEntity));

    objectToTest.delete(DIPLOMA_UUID);
    verify(diplomaRepository)
        .save(
            argThat(
                diploma -> {
                  assertTrue(diploma.isDeleted());
                  return true;
                }));
  }

  @Test
  void test_find_fails_when_diploma_not_found() {
    when(diplomaRepository.findByDeletedFalseAndUuid(DIPLOMA_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.find(DIPLOMA_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find() {
    DiplomaEntity diplomaEntity = new DiplomaEntity();
    when(diplomaRepository.findByDeletedFalseAndUuid(DIPLOMA_UUID))
        .thenReturn(Optional.of(diplomaEntity));

    DiplomaEntity response = objectToTest.find(DIPLOMA_UUID);
    assertSame(diplomaEntity, response);
  }

  @Test
  void test_find_by_account_fails_when_account_not_exist() {
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.findByAccount(ACCOUNT_UUID));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find_by_account() {
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
    when(diplomaRepository.findByDeletedFalseAndAccount(
            any(AccountEntity.class), any(Pageable.class)))
        .thenReturn(Optional.of(new PageImpl<>(List.of(new DiplomaEntity()))));

    objectToTest.findByAccount(ACCOUNT_UUID);
    verify(diplomaRepository)
        .findByDeletedFalseAndAccount(
            argThat(
                accountEntity -> {
                  assertSame(account, accountEntity);
                  return true;
                }),
            argThat(
                pageable -> {
                  assertEquals(Constants.PAGE_DEFAULT_OFFSET, pageable.getPageNumber());
                  assertEquals(Constants.PAGE_DEFAULT_LIMIT, pageable.getPageSize());
                  return true;
                }));
  }
}
