package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.SkillEntity;
import com.yamogroup.skillsmates.accounts.dao.enums.skill.CategoryEnum;
import com.yamogroup.skillsmates.accounts.dao.enums.skill.SkillLevelEnum;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.SkillRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.helpers.Constants;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static com.yamogroup.skillsmates.accounts.services.AccountFixtures.ACCOUNT_UUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SkillServiceImplTest {

  @InjectMocks private SkillServiceImpl objectToTest;
  @Mock private SkillRepository skillRepository;
  @Mock private AccountRepository accountRepository;

  public static String SKILL_UUID = "25155d22-620a-40b3-90a8-8b89eb86469a";

  @AfterEach
  void checkNoMoreInteraction() {
    verifyNoMoreInteractions(accountRepository, skillRepository);
  }

  @Test
  void test_save_fails_when_level_is_null() {
    SkillEntity skillEntity = new SkillEntity();
    skillEntity.setCategory(CategoryEnum.CATEGORY_1);

    BadRequestException expectedException =
        assertThrows(BadRequestException.class, () -> objectToTest.save(skillEntity));
    assertEquals(
        "Le niveau de competence ou la categorie sont introuvables",
        expectedException.getMessage());
  }

  @Test
  void test_save_fails_when_category_is_null() {
    SkillEntity skillEntity = new SkillEntity();
    skillEntity.setLevel(SkillLevelEnum.LEVEL_1);

    BadRequestException expectedException =
        assertThrows(BadRequestException.class, () -> objectToTest.save(skillEntity));
    assertEquals(
        "Le niveau de competence ou la categorie sont introuvables",
        expectedException.getMessage());
  }

  @Test
  void test_save_fails_when_owner_not_found() {
    SkillEntity skillEntity = new SkillEntity();
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    skillEntity.setCategory(CategoryEnum.CATEGORY_1);
    skillEntity.setLevel(SkillLevelEnum.LEVEL_1);
    skillEntity.setAccount(account);

    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.save(skillEntity));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_save() throws BadRequestException {
    SkillEntity skillEntity = new SkillEntity();
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    skillEntity.setCategory(CategoryEnum.CATEGORY_1);
    skillEntity.setLevel(SkillLevelEnum.LEVEL_1);
    skillEntity.setAccount(account);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(new AccountEntity()));

    objectToTest.save(skillEntity);
    verify(skillRepository)
        .save(
            argThat(
                skill -> {
                  assertNotSame(account, skill.getAccount());
                  return true;
                }));
  }

  @Test
  void test_update_fails_when_level_is_null() {
    SkillEntity newSkillEntity = new SkillEntity();
    newSkillEntity.setCategory(CategoryEnum.CATEGORY_1);

    BadRequestException expectedException =
        assertThrows(
            BadRequestException.class, () -> objectToTest.update(SKILL_UUID, newSkillEntity));
    assertEquals(
        "Le niveau de competence ou la categorie sont introuvables",
        expectedException.getMessage());
  }

  @Test
  void test_update_fails_when_category_is_null() {
    SkillEntity newSkillEntity = new SkillEntity();
    newSkillEntity.setLevel(SkillLevelEnum.LEVEL_1);

    BadRequestException expectedException =
        assertThrows(
            BadRequestException.class, () -> objectToTest.update(SKILL_UUID, newSkillEntity));
    assertEquals(
        "Le niveau de competence ou la categorie sont introuvables",
        expectedException.getMessage());
  }

  @Test
  void test_update_fails_when_skill_not_found() {
    SkillEntity newSkillEntity = new SkillEntity();
    newSkillEntity.setLevel(SkillLevelEnum.LEVEL_1);
    newSkillEntity.setCategory(CategoryEnum.CATEGORY_1);

    when(skillRepository.findByDeletedFalseAndUuid(SKILL_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(
            NotFoundException.class, () -> objectToTest.update(SKILL_UUID, newSkillEntity));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_update_fails_when_not_authorize() {
    SkillEntity newSkillEntity = new SkillEntity();
    SkillEntity oldSkillEntity = new SkillEntity();
    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid("test");
    oldOwner.setUuid("test1");
    newSkillEntity.setLevel(SkillLevelEnum.LEVEL_1);
    newSkillEntity.setCategory(CategoryEnum.CATEGORY_1);
    newSkillEntity.setAccount(newOwner);
    oldSkillEntity.setAccount(oldOwner);

    when(skillRepository.findByDeletedFalseAndUuid(SKILL_UUID))
        .thenReturn(Optional.of(oldSkillEntity));

    ForbiddenException expectedException =
        assertThrows(
            ForbiddenException.class, () -> objectToTest.update(SKILL_UUID, newSkillEntity));
    assertEquals(
        "Ce compte n'a pas les droit de modifier cette competence", expectedException.getMessage());
  }

  @Test
  void test_update() throws BadRequestException {
    SkillEntity newSkillEntity = new SkillEntity();
    SkillEntity oldSkillEntity = new SkillEntity();
    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid(ACCOUNT_UUID);
    oldOwner.setUuid(ACCOUNT_UUID);
    newSkillEntity.setLevel(SkillLevelEnum.LEVEL_1);
    newSkillEntity.setCategory(CategoryEnum.CATEGORY_1);
    newSkillEntity.setAccount(newOwner);
    oldSkillEntity.setAccount(oldOwner);

    newSkillEntity.setDiscipline("new discipline");
    newSkillEntity.setKeywords("java");
    newSkillEntity.setTitle("skill title");
    newSkillEntity.setDescription("new sample description");
    newSkillEntity.setCategory(CategoryEnum.CATEGORY_2);
    newSkillEntity.setLevel(SkillLevelEnum.LEVEL_3);

    when(skillRepository.findByDeletedFalseAndUuid(SKILL_UUID))
        .thenReturn(Optional.of(oldSkillEntity));

    objectToTest.update(SKILL_UUID, newSkillEntity);

    verify(skillRepository)
        .save(
            argThat(
                skill -> {
                  assertEquals(newSkillEntity.getDiscipline(), skill.getDiscipline());
                  assertEquals(newSkillEntity.getKeywords(), skill.getKeywords());
                  assertEquals(newSkillEntity.getTitle(), skill.getTitle());
                  assertEquals(newSkillEntity.getDescription(), skill.getDescription());
                  assertSame(newSkillEntity.getCategory(), skill.getCategory());
                  assertSame(newSkillEntity.getLevel(), skill.getLevel());
                  return true;
                }));
  }

  @Test
  void test_delete_fails_when_skill_not_exist() {
    when(skillRepository.findByDeletedFalseAndUuid(SKILL_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.delete(SKILL_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_delete() {
    SkillEntity skillEntity = new SkillEntity();
    when(skillRepository.findByDeletedFalseAndUuid(SKILL_UUID))
        .thenReturn(Optional.of(skillEntity));

    objectToTest.delete(SKILL_UUID);
    verify(skillRepository)
        .save(
            argThat(
                skill -> {
                  assertTrue(skill.isDeleted());
                  return true;
                }));
  }

  @Test
  void test_find_fails_when_skill_not_found() {
    when(skillRepository.findByDeletedFalseAndUuid(SKILL_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.find(SKILL_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find() {
    SkillEntity skillEntity = new SkillEntity();
    when(skillRepository.findByDeletedFalseAndUuid(SKILL_UUID))
        .thenReturn(Optional.of(skillEntity));

    SkillEntity response = objectToTest.find(SKILL_UUID);
    assertSame(skillEntity, response);
  }

  @Test
  void test_find_by_account_fails_when_account_not_exist() {
    when(accountRepository.findByActiveTrueAndUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.findByAccount(ACCOUNT_UUID));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find_by_account() {
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    when(accountRepository.findByActiveTrueAndUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
    when(skillRepository.findByDeletedFalseAndAccount(
            any(AccountEntity.class), any(Pageable.class)))
        .thenReturn(Optional.of(new PageImpl<>(List.of(new SkillEntity()))));

    objectToTest.findByAccount(ACCOUNT_UUID);
    verify(skillRepository)
        .findByDeletedFalseAndAccount(
            argThat(
                accountEntity -> {
                  assertSame(account, accountEntity);
                  return true;
                }),
            argThat(
                pageable -> {
                  assertEquals(Constants.PAGE_DEFAULT_OFFSET, pageable.getPageNumber());
                  assertEquals(Constants.PAGE_DEFAULT_LIMIT, pageable.getPageSize());
                  return true;
                }));
  }
}
