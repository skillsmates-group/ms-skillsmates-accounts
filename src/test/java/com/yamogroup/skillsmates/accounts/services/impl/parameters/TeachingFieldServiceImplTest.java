package com.yamogroup.skillsmates.accounts.services.impl.parameters;

import com.yamogroup.skillsmates.accounts.dao.entities.parameters.TeachingFieldEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.parameters.TeachingFieldRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class TeachingFieldServiceImplTest
    extends CommonParameterTest<
        TeachingFieldEntity, TeachingFieldServiceImpl, TeachingFieldRepository> {

  @InjectMocks private TeachingFieldServiceImpl objectToTest;
  @Mock private TeachingFieldRepository repository;

  @Override
  protected TeachingFieldEntity newInstance() {
    return new TeachingFieldEntity();
  }

  @Override
  protected TeachingFieldServiceImpl getObjectToTest() {
    return objectToTest;
  }

  @Override
  protected TeachingFieldRepository getRepository() {
    return repository;
  }
}
