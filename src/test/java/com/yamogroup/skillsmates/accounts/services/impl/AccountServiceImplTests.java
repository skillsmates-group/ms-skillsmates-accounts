package com.yamogroup.skillsmates.accounts.services.impl;

import com.yamogroup.skillsmates.accounts.config.CryptUtil;
import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.*;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.*;
import com.yamogroup.skillsmates.accounts.dao.repositories.posts.InteractionRepository;
import com.yamogroup.skillsmates.accounts.exceptions.*;
import com.yamogroup.skillsmates.accounts.helpers.StringHelper;
import com.yamogroup.skillsmates.accounts.http.DocumentServiceHttp;
import com.yamogroup.skillsmates.accounts.http.EmailServiceHttp;
import com.yamogroup.skillsmates.accounts.http.dto.EmailResponse;
import com.yamogroup.skillsmates.accounts.http.dto.requests.DocumentCreationRequest;
import com.yamogroup.skillsmates.accounts.http.dto.responses.DocumentCreateResponse;
import com.yamogroup.skillsmates.accounts.http.dto.responses.DocumentCreateResponseResource;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.PasswordEdit;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDeactivateResource;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDeleteResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountStatusMetricResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.attributes.AccountAttributesResponse;
import com.yamogroup.skillsmates.accounts.services.AccountFixtures;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

import static com.yamogroup.skillsmates.accounts.dao.enums.account.StatusEnum.*;
import static com.yamogroup.skillsmates.accounts.services.AccountFixtures.ACCOUNT_UUID;
import static com.yamogroup.skillsmates.accounts.services.AccountFixtures.EMAIL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceImplTests {
    @InjectMocks
    private AccountServiceImpl objectToTest;
    @Mock
    private SkillRepository skillRepository;
    @Mock
    private TrainingRepository trainingRepository;
    @Mock
    private DiplomaRepository diplomaRepository;
    @Mock
    private JobRepository jobRepository;
    @Mock
    private CertificationRepository certificationRepository;
    @Mock
    private InteractionRepository interactionRepository;
    @Mock
    private DocumentServiceHttp documentServiceHttp;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private EmailServiceHttp emailServiceHttp;
    @Mock
    private CryptUtil cryptUtil;

    @Test
    public void test_find_when_account_not_found() {
        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(NotFoundException.class, () -> objectToTest.find(ACCOUNT_UUID));
        assertEquals("Ce compte n'existe pas", expectedException.getMessage());
        verify(accountRepository).findByUuid(ACCOUNT_UUID);
    }

    @Test
    void should_throw_already_exists_exception_when_account_already_exists_in_db() {
        AccountEntity entity = AccountFixtures.generateAccountEntity();
        entity.setActive(true);
        when(accountRepository.findByEmail(AccountFixtures.EMAIL)).thenReturn(Optional.of(entity));

        AlreadyExistsException expectedException =
                assertThrows(AlreadyExistsException.class, () -> objectToTest.create(entity));
        assertEquals("Un compte avec cet adresse email existe deja", expectedException.getMessage());
        verify(accountRepository).findByEmail(entity.getEmail());
    }

    @Test
    void should_throw_not_activated_exception_when_account_not_activated_in_db() {
        AccountEntity entity = AccountFixtures.generateAccountEntity();
        entity.setActive(false);
        when(accountRepository.findByEmail(AccountFixtures.EMAIL)).thenReturn(Optional.of(entity));

        AlreadyExistsException expectedException =
                assertThrows(AlreadyExistsException.class, () -> objectToTest.create(entity));
        assertEquals("Un compte avec cet adresse email est inactif", expectedException.getMessage());
        verify(accountRepository).findByEmail(entity.getEmail());
    }

    @Test
    void should_save_account_when_input_is_correct()
            throws AlreadyExistsException, NoSuchAlgorithmException, ThirdPartyException {
        AccountEntity entity = AccountFixtures.generateAccountEntity();

        when(accountRepository.findByEmail(AccountFixtures.EMAIL)).thenReturn(Optional.empty());
        when(accountRepository.save(any(AccountEntity.class))).thenAnswer(i -> i.getArguments()[0]);
        when(emailServiceHttp.sendEmail(any(EmailResponse.class))).thenAnswer(i -> i.getArguments()[0]);

        AccountEntity data = objectToTest.create(entity);

        assertThat(data).isNotNull();
        assertThat(data.getFirstname()).isEqualTo(entity.getFirstname());
        assertThat(data.getLastname()).isEqualTo(entity.getLastname());
        assertThat(data.getEmail()).isEqualTo(entity.getEmail());
        org.assertj.core.api.Assertions.assertThat(data.getUuid()).isNotBlank();
        org.assertj.core.api.Assertions.assertThat(data.isDeleted()).isFalse();
    }

    @Test
    void test_update_biography_throws_Exception_when_user_not_allowed() {
        AccountEntity account = new AccountEntity();
        account.setUuid("ACCOUNT_UUID");
        ForbiddenException expectedException =
                assertThrows(
                        ForbiddenException.class, () -> objectToTest.updateBiography(ACCOUNT_UUID, account));
        assertEquals("Ce compte n'a pas les droits de modification", expectedException.getMessage());
    }

    @Test
    void test_update_biography_throws_Exception_when_user_not_exist() {
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        when(accountRepository.findByActiveTrueAndUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(
                        NotFoundException.class, () -> objectToTest.updateBiography(ACCOUNT_UUID, account));
        assertEquals("Ce compte n'existe pas", expectedException.getMessage());
        verify(accountRepository).findByActiveTrueAndUuid(ACCOUNT_UUID);
    }

    @Test
    void test_update_throws_Exception_when_user_not_allowed() {
        AccountEntity account = new AccountEntity();
        account.setUuid("ACCOUNT_UUID");
        ForbiddenException expectedException =
                assertThrows(ForbiddenException.class, () -> objectToTest.update(ACCOUNT_UUID, account));
        assertEquals("Ce compte n'a pas les droits de modification", expectedException.getMessage());
    }

    @Test
    void test_deactivate_account() {
        AccountEntity account = new AccountEntity();

        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
        when(accountRepository.save(Mockito.any(AccountEntity.class))).thenReturn(account);

        AccountDeactivateResource response = objectToTest.deactivateAccount(ACCOUNT_UUID);

        verify(accountRepository).findByUuid(ACCOUNT_UUID);
        verify(accountRepository)
                .save(
                        argThat(
                                accountEntity -> {
                                    assertFalse(accountEntity.isActive());
                                    return true;
                                }));
    }

    @Test
    void test_deactivate_account_throws_Exception_when_user_not_exist() {
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(NotFoundException.class, () -> objectToTest.deactivateAccount(ACCOUNT_UUID));
        assertEquals("Ce compte n'existe pas", expectedException.getMessage());
        verify(accountRepository).findByUuid(ACCOUNT_UUID);
    }

    @Test
    void test_delete_account() {
        AccountEntity account = new AccountEntity();

        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
        when(accountRepository.save(Mockito.any(AccountEntity.class))).thenReturn(account);

        AccountDeleteResponse response = objectToTest.deleteAccount(ACCOUNT_UUID);

        verify(accountRepository).findByUuid(ACCOUNT_UUID);
        verify(accountRepository)
                .save(
                        argThat(
                                accountEntity -> {
                                    assertTrue(accountEntity.isDeleted());
                                    return true;
                                }));
    }

    @Test
    void test_delete_account_throws_Exception_when_user_not_exist() {
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(NotFoundException.class, () -> objectToTest.deleteAccount(ACCOUNT_UUID));
        assertEquals("Ce compte n'existe pas", expectedException.getMessage());
        verify(accountRepository).findByUuid(ACCOUNT_UUID);
    }

    @Test
    void test_change_password_throws_Exception_when_user_not_allowed() {
        String newPassword = "newPassword";
        String confirmPassword = "newPassword";
        String oldPassword = "oldPassword";

        PasswordEdit request =
                PasswordEdit.builder()
                        .uuid("ACCOUNT_UUID")
                        .newPassword(newPassword)
                        .confirmPassword(confirmPassword)
                        .oldPassword(oldPassword)
                        .build();

        ForbiddenException expectedException =
                assertThrows(
                        ForbiddenException.class, () -> objectToTest.changePassword(ACCOUNT_UUID, request));
        assertEquals("Ce compte n'a pas les droits de modification", expectedException.getMessage());
    }

    @Test
    void test_change_password_throws_Exception_when_password_are_different() {
        String newPassword = "newPassword";
        String confirmPassword = "confirmPassword";
        String oldPassword = "oldPassword";

        PasswordEdit request =
                PasswordEdit.builder()
                        .uuid(ACCOUNT_UUID)
                        .newPassword(newPassword)
                        .confirmPassword(confirmPassword)
                        .oldPassword(oldPassword)
                        .build();

        BadRequestException expectedException =
                assertThrows(
                        BadRequestException.class, () -> objectToTest.changePassword(ACCOUNT_UUID, request));
        assertEquals(
                "le nouveau mot de passe et la confirmation sont différents",
                expectedException.getMessage());
    }

    @Test
    void test_change_password_throws_Exception_when_account_not_found() {
        String newPassword = "newPassword";
        String confirmPassword = "newPassword";
        String oldPassword = "oldPassword";
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);

        PasswordEdit request =
                PasswordEdit.builder()
                        .uuid(ACCOUNT_UUID)
                        .newPassword(newPassword)
                        .confirmPassword(confirmPassword)
                        .oldPassword(oldPassword)
                        .build();

        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(
                        NotFoundException.class, () -> objectToTest.changePassword(ACCOUNT_UUID, request));
        assertEquals("Compte innexistant", expectedException.getMessage());
        verify(accountRepository).findByUuid(ACCOUNT_UUID);
    }

    @Test
    void test_change_password_throws_Exception_when_old_password_is_not_correct()
            throws NoSuchAlgorithmException {
        String newPassword = "newPassword";
        String confirmPassword = "newPassword";
        String oldPassword = "oldPassword";
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        account.setEmail(EMAIL);
        account.setPassword(StringHelper.generateSecurePassword(newPassword, account.getEmail()));

        PasswordEdit request =
                PasswordEdit.builder()
                        .uuid(ACCOUNT_UUID)
                        .newPassword(newPassword)
                        .confirmPassword(confirmPassword)
                        .oldPassword(oldPassword)
                        .build();

        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));

        ForbiddenException expectedException =
                assertThrows(
                        ForbiddenException.class, () -> objectToTest.changePassword(ACCOUNT_UUID, request));
        assertEquals("L'ancien mot de passe est invalide", expectedException.getMessage());
        verify(accountRepository).findByUuid(ACCOUNT_UUID);
    }

    @Test
    void test_reset_password()
            throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException,
            BadPaddingException, InvalidKeyException, UnsupportedEncodingException,
            BadRequestException {
        String token = "token";
        String password = "password";
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        account.setEmail(EMAIL);

        when(cryptUtil.decrypt(token)).thenReturn(ACCOUNT_UUID);
        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
        when(accountRepository.save(any(AccountEntity.class))).thenReturn(account);

        objectToTest.resetPassword(password, token);

        verify(accountRepository).findByUuid(ACCOUNT_UUID);
        verify(cryptUtil).decrypt(token);
        verify(emailServiceHttp).sendEmail(any());
        verify(accountRepository)
                .save(
                        argThat(
                                accountEntity -> {
                                    try {
                                        assertEquals(
                                                StringHelper.generateSecurePassword(password, EMAIL),
                                                accountEntity.getPassword());
                                    } catch (NoSuchAlgorithmException e) {
                                        return false;
                                    }
                                    return true;
                                }));
    }

    @Test
    void test_reset_password_throws_Exception_when_user_not_exist()
            throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException,
            BadPaddingException, InvalidKeyException {
        String token = "token";
        String password = "password";

        when(cryptUtil.decrypt(token)).thenReturn(ACCOUNT_UUID);
        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(NotFoundException.class, () -> objectToTest.resetPassword(password, token));
        assertEquals("Un compte avec cet identifiant non trouvé", expectedException.getMessage());
        verify(accountRepository).findByUuid(ACCOUNT_UUID);
    }

    @Test
    void test_init_password_reset()
            throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException,
            BadRequestException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        account.setEmail(EMAIL);
        when(accountRepository.findByEmail(EMAIL)).thenReturn(Optional.of(account));

        AccountDetailsResponse response = objectToTest.initPasswordReset(EMAIL);
        verify(accountRepository).findByEmail(EMAIL);
        verify(emailServiceHttp).sendEmail(any());
    }

    @Test
    void test_init_password_reset_throws_Exception_when_user_not_exist() {
        when(accountRepository.findByEmail(EMAIL)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(NotFoundException.class, () -> objectToTest.initPasswordReset(EMAIL));
        assertEquals("Un compte avec cet adresse email n'existe pas", expectedException.getMessage());
        verify(accountRepository).findByEmail(EMAIL);
    }

    @Test
    void test_find_account_status_metric() {

        when(accountRepository.countAccountsByStatus(STUDENT)).thenReturn(12L);
        when(accountRepository.countAccountsByStatus(TEACHER)).thenReturn(8L);
        when(accountRepository.countAccountsByStatus(PROFESSIONAL)).thenReturn(10L);

        AccountStatusMetricResponse response = objectToTest.findAccountStatusMetric();

        assertEquals(3, response.getResources().size());
        assertTrue(
                response.getResources().stream()
                        .anyMatch(
                                e ->
                                        e.getLabel().equals(STUDENT.name())
                                                && e.getCode().equals(STUDENT.name())
                                                && e.getTotal() == 12));
        assertTrue(
                response.getResources().stream()
                        .anyMatch(
                                e ->
                                        e.getLabel().equals(TEACHER.name())
                                                && e.getCode().equals(TEACHER.name())
                                                && e.getTotal() == 8));
        assertTrue(
                response.getResources().stream()
                        .anyMatch(
                                e ->
                                        e.getLabel().equals(PROFESSIONAL.name())
                                                && e.getCode().equals(PROFESSIONAL.name())
                                                && e.getTotal() == 10));
    }

    @Test
    public void test_find_account_attributes() {
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        account.setEmail(EMAIL);

        SkillEntity skillEntity = new SkillEntity();
        skillEntity.setAccount(account);

        TrainingEntity trainingEntity = new TrainingEntity();
        trainingEntity.setAccount(account);

        DiplomaEntity diplomaEntity = new DiplomaEntity();
        diplomaEntity.setAccount(account);

        JobEntity jobEntity = new JobEntity();
        jobEntity.setAccount(account);

        CertificationEntity certificationEntity = new CertificationEntity();
        certificationEntity.setAccount(account);

        when(skillRepository.countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID)).thenReturn(10L);
        when(trainingRepository.countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID))
                .thenReturn(20L);
        when(diplomaRepository.countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID)).thenReturn(30L);
        when(jobRepository.countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID)).thenReturn(40L);
        when(certificationRepository.countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID))
                .thenReturn(50L);

        when(skillRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID))
                .thenReturn(List.of(skillEntity));
        when(trainingRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID))
                .thenReturn(List.of(trainingEntity));
        when(diplomaRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID))
                .thenReturn(List.of(diplomaEntity));
        when(jobRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID))
                .thenReturn(List.of(jobEntity));
        when(certificationRepository.findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(
                ACCOUNT_UUID))
                .thenReturn(List.of(certificationEntity));

        AccountAttributesResponse response = objectToTest.findAccountAttributes(ACCOUNT_UUID);

        verify(skillRepository).findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID);
        verify(skillRepository).countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID);
        verify(trainingRepository)
                .findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID);
        verify(trainingRepository).countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID);
        verify(diplomaRepository)
                .findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID);
        verify(diplomaRepository).countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID);
        verify(jobRepository).findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID);
        verify(jobRepository).countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID);
        verify(certificationRepository)
                .findByAccountUuidAndDeletedFalseOrderByCreatedAtDesc(ACCOUNT_UUID);
        verify(certificationRepository).countDistinctByAccountUuidAndDeletedFalse(ACCOUNT_UUID);

        assertEquals(10L, response.getResource().getTotalSkills());
        assertNotNull(response.getResource().getSkills());
        assertFalse(response.getResource().getSkills().isEmpty());

        assertEquals(20L, response.getResource().getTotalTrainings());
        assertNotNull(response.getResource().getTrainings());
        assertFalse(response.getResource().getTrainings().isEmpty());

        assertEquals(30L, response.getResource().getTotalDiplomas());
        assertNotNull(response.getResource().getDiplomas());
        assertFalse(response.getResource().getDiplomas().isEmpty());

        assertEquals(40L, response.getResource().getTotalJobs());
        assertNotNull(response.getResource().getJobs());
        assertFalse(response.getResource().getJobs().isEmpty());

        assertEquals(50L, response.getResource().getTotalCertifications());
        assertNotNull(response.getResource().getCertifications());
        assertFalse(response.getResource().getCertifications().isEmpty());
    }

    @Test
    void test_save_avatar_throws_exception_when_user_not_found() throws ThirdPartyException {
        MockMultipartFile multipartFile =
                new MockMultipartFile("user-file", "test.txt", "text/plain", "test data".getBytes());

        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(
                        NotFoundException.class, () -> objectToTest.saveAvatar(ACCOUNT_UUID, multipartFile));
        assertEquals("Ce compte n'existe pas", expectedException.getMessage());
        verify(accountRepository).findByUuid(ACCOUNT_UUID);
    }

    @Test
    void test_save_avatar_throws_exception_when_save_file_fails() throws Exception {
        MockMultipartFile multipartFile =
                new MockMultipartFile("user-file", "test.txt", "text/plain", "test data".getBytes());
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        ResponseEntity<DocumentCreateResponse> responseFromDocumentService =
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
        when(documentServiceHttp.saveDocument(
                any(DocumentCreationRequest.class), any(MultipartFile.class)))
                .thenReturn(responseFromDocumentService);

        ThirdPartyException expectedException =
                assertThrows(
                        ThirdPartyException.class, () -> objectToTest.saveAvatar(ACCOUNT_UUID, multipartFile));
        assertEquals("Erreur lors de la sauvegarde du fichier", expectedException.getMessage());
        verify(accountRepository).findByUuid(ACCOUNT_UUID);
        verify(documentServiceHttp)
                .saveDocument(
                        argThat(
                                documentCreationRequest -> {
                                    assertNull(documentCreationRequest.getMediaType());
                                    assertNull(documentCreationRequest.getUrl());
                                    assertNull(documentCreationRequest.getDocument());
                                    assertEquals(ACCOUNT_UUID, documentCreationRequest.getAccount());
                                    assertEquals("ORIGIN_AVATAR", documentCreationRequest.getOrigin());
                                    return true;
                                }),
                        argThat(
                                multipartFile1 -> {
                                    assertSame(multipartFile, multipartFile1);
                                    return true;
                                }));
    }

    @Test
    void test_save_avatar() throws Exception {
        MockMultipartFile multipartFile =
                new MockMultipartFile("user-file", "test.txt", "text/plain", "test data".getBytes());
        AccountEntity account = new AccountEntity();
        account.setUuid(ACCOUNT_UUID);
        String link = "documentLink";
        DocumentCreateResponse documentCreateResponse = new DocumentCreateResponse();
        DocumentCreateResponseResource documentCreateResponseResource =
                new DocumentCreateResponseResource();
        documentCreateResponseResource.setLink(link);
        documentCreateResponse.setResource(documentCreateResponseResource);

        ResponseEntity<DocumentCreateResponse> responseFromDocumentService =
                ResponseEntity.ok(documentCreateResponse);

        when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
        when(documentServiceHttp.saveDocument(
                any(DocumentCreationRequest.class), any(MultipartFile.class)))
                .thenReturn(responseFromDocumentService);

        objectToTest.saveAvatar(ACCOUNT_UUID, multipartFile);

        verify(accountRepository).findByUuid(ACCOUNT_UUID);
        verify(documentServiceHttp)
                .saveDocument(
                        argThat(
                                documentCreationRequest -> {
                                    assertNull(documentCreationRequest.getMediaType());
                                    assertNull(documentCreationRequest.getUrl());
                                    assertNull(documentCreationRequest.getDocument());
                                    assertEquals(ACCOUNT_UUID, documentCreationRequest.getAccount());
                                    assertEquals("ORIGIN_AVATAR", documentCreationRequest.getOrigin());
                                    return true;
                                }),
                        argThat(
                                multipartFile1 -> {
                                    assertSame(multipartFile, multipartFile1);
                                    return true;
                                }));
        verify(accountRepository)
                .save(
                        argThat(
                                accountEntity -> {
                                    assertEquals(link, accountEntity.getAvatar());
                                    return true;
                                }));
    }

    //    @Test
    //    void test_load_followees() {
    //        NetworkTypeEnum networkTypeEnum = NetworkTypeEnum.FOLLOWEES;
    //        Integer offset = 0;
    //        Integer limit = 10;
    //        String status = "status";
    //        String country = "CMR";
    //        String name = "name";
    //        AccountEntity account = new AccountEntity();
    //
    //        when(accountRepository.findFollowees(
    //                any(String.class),
    //                any(String.class),
    //                any(String.class),
    //                any(String.class),
    //                any(Pageable.class)))
    //                .thenReturn(new PageImpl<>(List.of(account)));
    //
    //        AccountDetailResponse response =
    //                objectToTest.loadNetwork(
    //                        networkTypeEnum, ACCOUNT_UUID, offset, limit, status, country, name);
    //
    //        verify(accountRepository)
    //                .findFollowees(
    //                        argThat(
    //                                s -> {
    //                                    assertEquals(ACCOUNT_UUID, s);
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(status.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(country.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(name.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                pageable -> {
    //                                    assertEquals(offset, pageable.getPageNumber());
    //                                    assertEquals(limit, pageable.getPageSize());
    //                                    return true;
    //                                }));
    //    }

    //    @Test
    //    void test_load_followers() {
    //        NetworkTypeEnum networkTypeEnum = NetworkTypeEnum.FOLLOWERS;
    //        Integer offset = 0;
    //        Integer limit = 10;
    //        String status = "status";
    //        String country = "CMR";
    //        String name = "name";
    //        AccountEntity account = new AccountEntity();
    //
    //        when(accountRepository.findFollowers(
    //                any(String.class),
    //                any(String.class),
    //                any(String.class),
    //                any(String.class),
    //                any(Pageable.class)))
    //                .thenReturn(new PageImpl<>(List.of(account)));
    //
    //        AccountDetailResponse response =
    //                objectToTest.loadNetwork(
    //                        networkTypeEnum, ACCOUNT_UUID, offset, limit, status, country, name);
    //
    //        verify(accountRepository)
    //                .findFollowers(
    //                        argThat(
    //                                s -> {
    //                                    assertEquals(ACCOUNT_UUID, s);
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(status.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(country.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(name.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                pageable -> {
    //                                    assertEquals(offset, pageable.getPageNumber());
    //                                    assertEquals(limit, pageable.getPageSize());
    //                                    return true;
    //                                }));
    //    }

    //    @Test
    //    void test_load_suggestions() {
    //        NetworkTypeEnum networkTypeEnum = NetworkTypeEnum.SUGGESTIONS;
    //        Integer offset = 0;
    //        Integer limit = 10;
    //        String status = "status";
    //        String country = "CMR";
    //        String name = "name";
    //        AccountEntity account = new AccountEntity();
    //
    //        when(accountRepository.findSuggestions(
    //                any(String.class),
    //                any(String.class),
    //                any(String.class),
    //                any(String.class),
    //                any(Pageable.class)))
    //                .thenReturn(new PageImpl<>(List.of(account)));
    //
    //        AccountDetailResponse response =
    //                objectToTest.loadNetwork(
    //                        networkTypeEnum, ACCOUNT_UUID, offset, limit, status, country, name);
    //
    //        verify(accountRepository)
    //                .findSuggestions(
    //                        argThat(
    //                                s -> {
    //                                    assertEquals(ACCOUNT_UUID, s);
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(status.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(country.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(name.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                pageable -> {
    //                                    assertEquals(offset, pageable.getPageNumber());
    //                                    assertEquals(limit, pageable.getPageSize());
    //                                    return true;
    //                                }));
    //    }

    //    @Test
    //    void test_load_online() {
    //        NetworkTypeEnum networkTypeEnum = NetworkTypeEnum.ONLINE;
    //        Integer offset = 0;
    //        Integer limit = 10;
    //        String status = "status";
    //        String country = "CMR";
    //        String name = "name";
    //        AccountEntity account = new AccountEntity();
    //
    //        when(accountRepository
    //
    // .findByConnectedTrueAndActiveTrueAndDeletedFalseAndUuidNotAndStatusLikeAndCountryLike(
    //                        any(String.class), any(String.class), any(String.class),
    // any(Pageable.class)))
    //                .thenReturn(new PageImpl<>(List.of(account)));
    //
    //        AccountDetailResponse response =
    //                objectToTest.loadNetwork(
    //                        networkTypeEnum, ACCOUNT_UUID, offset, limit, status, country, name);
    //
    //        verify(accountRepository)
    //
    // .findByConnectedTrueAndActiveTrueAndDeletedFalseAndUuidNotAndStatusLikeAndCountryLike(
    //                        argThat(
    //                                s -> {
    //                                    assertEquals(ACCOUNT_UUID, s);
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(status.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                s -> {
    //                                    assertTrue(s.contains(country.toLowerCase()));
    //                                    return true;
    //                                }),
    //                        argThat(
    //                                pageable -> {
    //                                    assertEquals(offset, pageable.getPageNumber());
    //                                    assertEquals(limit, pageable.getPageSize());
    //                                    return true;
    //                                }));
    //    }

    @Test
    void test_load_favourite() {
        // TODO: Not yet implmented
        //        throw new RuntimeException("NOT YET IMPLEMENTED");
    }

    @Test
    void test_load_around_you() {
        // TODO: Not yet implmented
        //        throw new RuntimeException("NOT YET IMPLEMENTED");
    }

    @Test
    void test_search_account() {
        // TODO: Not yet implmented
        //        throw new RuntimeException("NOT YET IMPLEMENTED");
    }
}
