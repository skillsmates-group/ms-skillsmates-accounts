package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.JobEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivitySectorEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.JobRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.helpers.Constants;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.ActivityAreaServiceImpl;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.ActivitySectorServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.yamogroup.skillsmates.accounts.services.AccountFixtures.ACCOUNT_UUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class JobServiceImplTest {

  @InjectMocks private JobServiceImpl objectToTest;
  @Mock private AccountRepository accountRepository;
  @Mock private JobRepository jobRepository;
  @Mock private ActivityAreaServiceImpl activityAreaService;
  @Mock private ActivitySectorServiceImpl activitySectorService;

  public static String JOB_UUID = "25155d22-620a-40b3-90a8-8b89eb86469a";

  @AfterEach
  void checkNoMoreInteraction() {
    verifyNoMoreInteractions(
        accountRepository, jobRepository, activityAreaService, activitySectorService);
  }

  @Test
  void test_save_fails_when_owner_not_exist() {
    JobEntity jobEntity = new JobEntity();
    ActivitySectorEntity activitySector = new ActivitySectorEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);

    jobEntity.setActivitySector(activitySector);
    jobEntity.setActivityArea(activityArea);
    jobEntity.setAccount(account);

    when(activitySectorService.findOrCreate(activitySector)).thenReturn(activitySector);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.save(jobEntity));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_save() {
    JobEntity jobEntity = new JobEntity();
    ActivitySectorEntity activitySector = new ActivitySectorEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);

    jobEntity.setActivityArea(activityArea);
    jobEntity.setActivitySector(activitySector);
    jobEntity.setAccount(account);

    when(activitySectorService.findOrCreate(activitySector)).thenReturn(activitySector);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));

    objectToTest.save(jobEntity);
    verify(jobRepository)
        .save(
            argThat(
                job -> {
                  assertSame(jobEntity.getActivitySector(), job.getActivitySector());
                  assertSame(jobEntity.getActivityArea(), job.getActivityArea());
                  return true;
                }));
  }

  @Test
  void test_update_fails_when_job_not_exist() {
    JobEntity newJob = new JobEntity();
    when(jobRepository.findByDeletedFalseAndUuid(JOB_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.update(JOB_UUID, newJob));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_update_throws_exception_when_not_authorize() {
    JobEntity newJob = new JobEntity();
    JobEntity oldJob = new JobEntity();
    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid("test");
    oldOwner.setUuid("test1");
    newJob.setAccount(newOwner);
    oldJob.setAccount(oldOwner);

    when(jobRepository.findByDeletedFalseAndUuid(JOB_UUID)).thenReturn(Optional.of(oldJob));

    ForbiddenException expectedException =
        assertThrows(ForbiddenException.class, () -> objectToTest.update(JOB_UUID, newJob));
    assertEquals(
        "Ce compte n'a pas les droit de modifier cette experience profesionnelle",
        expectedException.getMessage());
  }

  @Test
  void test_update() throws BadRequestException {
    JobEntity newJob = new JobEntity();
    JobEntity oldJob = new JobEntity();

    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid(ACCOUNT_UUID);
    oldOwner.setUuid(ACCOUNT_UUID);

    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    ActivitySectorEntity activitySector = new ActivitySectorEntity();

    newJob.setAccount(newOwner);
    oldJob.setAccount(oldOwner);
    newJob.setTitle("diploma title");
    newJob.setActivityArea(activityArea);
    newJob.setActivitySector(activitySector);
    newJob.setCompany("YAMO Group");
    newJob.setStartDate(new Date());
    newJob.setEndDate(new Date());
    newJob.setCity("Yaoundé");
    newJob.setDescription("new Description");
    newJob.setCurrentJob(true);

    when(jobRepository.findByDeletedFalseAndUuid(JOB_UUID)).thenReturn(Optional.of(oldJob));
    when(activitySectorService.findOrCreate(activitySector)).thenReturn(activitySector);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);

    objectToTest.update(JOB_UUID, newJob);
    verify(jobRepository)
        .save(
            argThat(
                job -> {
                  assertEquals(newJob.getTitle(), job.getTitle());
                  assertSame(newJob.getActivityArea(), job.getActivityArea());
                  assertSame(newJob.getActivitySector(), job.getActivitySector());
                  assertEquals(newJob.getCompany(), job.getCompany());
                  assertSame(newJob.getStartDate(), job.getStartDate());
                  assertSame(newJob.getEndDate(), job.getEndDate());
                  assertEquals(newJob.getCity(), job.getCity());
                  assertEquals(newJob.getDescription(), job.getDescription());
                  assertEquals(newJob.isCurrentJob(), job.isCurrentJob());
                  return true;
                }));
  }

  @Test
  void test_delete_fails_when_job_not_exist() {
    when(jobRepository.findByDeletedFalseAndUuid(JOB_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.delete(JOB_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_delete() {
    JobEntity jobEntity = new JobEntity();
    when(jobRepository.findByDeletedFalseAndUuid(JOB_UUID)).thenReturn(Optional.of(jobEntity));

    objectToTest.delete(JOB_UUID);
    verify(jobRepository)
        .save(
            argThat(
                diploma -> {
                  assertTrue(diploma.isDeleted());
                  return true;
                }));
  }

  @Test
  void test_find_fails_when_job_not_found() {
    when(jobRepository.findByDeletedFalseAndUuid(JOB_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.find(JOB_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find() {
    JobEntity diplomaEntity = new JobEntity();
    when(jobRepository.findByDeletedFalseAndUuid(JOB_UUID)).thenReturn(Optional.of(diplomaEntity));

    JobEntity response = objectToTest.find(JOB_UUID);
    assertSame(diplomaEntity, response);
  }

  @Test
  void test_find_by_account_fails_when_account_not_exist() {
    when(accountRepository.findByActiveTrueAndUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.findByAccount(ACCOUNT_UUID));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find_by_account() {
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    when(accountRepository.findByActiveTrueAndUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
    when(jobRepository.findByDeletedFalseAndAccount(any(AccountEntity.class), any(Pageable.class)))
        .thenReturn(Optional.of(new PageImpl<>(List.of(new JobEntity()))));

    objectToTest.findByAccount(ACCOUNT_UUID);
    verify(jobRepository)
        .findByDeletedFalseAndAccount(
            argThat(
                accountEntity -> {
                  assertSame(account, accountEntity);
                  return true;
                }),
            argThat(
                pageable -> {
                  assertEquals(Constants.PAGE_DEFAULT_OFFSET, pageable.getPageNumber());
                  assertEquals(Constants.PAGE_DEFAULT_LIMIT, pageable.getPageSize());
                  return true;
                }));
  }
}
