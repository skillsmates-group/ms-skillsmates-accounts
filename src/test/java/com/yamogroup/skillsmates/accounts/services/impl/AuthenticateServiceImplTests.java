package com.yamogroup.skillsmates.accounts.services.impl;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.exceptions.UnauthorizedException;
import com.yamogroup.skillsmates.accounts.helpers.StringHelper;
import com.yamogroup.skillsmates.accounts.services.AccountFixtures;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthenticateServiceImplTests {
    @InjectMocks
    private AuthenticateServiceImpl authenticateService;
    @Mock
    private AccountRepository accountRepository;

    @Test
    void should_throw_not_found_exception_when_account_not_in_db() {
        AccountEntity entity = AccountFixtures.generateAccountEntity();
        when(accountRepository.findByEmail(AccountFixtures.EMAIL)).thenReturn(Optional.empty());

        NotFoundException expectedException =
                assertThrows(NotFoundException.class, () -> authenticateService.authenticate(entity));
        assertEquals("Aucun compte avec ses identifiants. Veuillez en creer un", expectedException.getMessage());
        verify(accountRepository).findByEmail(entity.getEmail());
    }

    @Test
    void should_throw_unauthorized_exception_when_account_not_active() {
        AccountEntity entity = AccountFixtures.generateAccountEntity();
        entity.setActive(false);
        when(accountRepository.findByEmail(AccountFixtures.EMAIL)).thenReturn(Optional.of(entity));

        UnauthorizedException expectedException =
                assertThrows(UnauthorizedException.class, () -> authenticateService.authenticate(entity));
        assertEquals("Votre compte n'est pas encore activé. Veuillez consulter vos emails", expectedException.getMessage());
        verify(accountRepository).findByEmail(entity.getEmail());
    }

    @Test
    void should_throw_bad_request_exception_when_account_password_does_not_match() {
        AccountEntity entity = AccountFixtures.generateAccountEntity();
        entity.setActive(true);
        entity.setPassword("PASSWORD");

        AccountEntity returnedEntity = AccountFixtures.generateAccountEntity();
        returnedEntity.setActive(true);
        returnedEntity.setPassword("WORDPASS");
        when(accountRepository.findByEmail(AccountFixtures.EMAIL)).thenReturn(Optional.of(returnedEntity));

        BadRequestException expectedException =
                assertThrows(BadRequestException.class, () -> authenticateService.authenticate(entity));
        assertEquals("Votre email/mot de passe n'est pas valide", expectedException.getMessage());
        verify(accountRepository).findByEmail(entity.getEmail());
    }

    @Test
    void should_authenticate_account_when_input_is_correct()
            throws BadRequestException, NoSuchAlgorithmException {
        AccountEntity entity = AccountFixtures.generateAccountEntity();
        entity.setActive(true);
        entity.setPassword("PASSWORD");

        AccountEntity returnedEntity = AccountFixtures.generateAccountEntity();
        returnedEntity.setActive(true);
        returnedEntity.setPassword(
                StringHelper.generateSecurePassword(entity.getPassword(), entity.getEmail()));
        when(accountRepository.findByEmail(AccountFixtures.EMAIL)).thenReturn(Optional.of(returnedEntity));
        when(accountRepository.save(returnedEntity)).thenReturn(returnedEntity);

        AccountEntity data = authenticateService.authenticate(entity);

        assertThat(data).isNotNull();
        assertThat(data.getFirstname()).isEqualTo(entity.getFirstname());
        assertThat(data.getLastname()).isEqualTo(entity.getLastname());
        assertThat(data.getEmail()).isEqualTo(entity.getEmail());
        Assertions.assertThat(data.getUuid()).isNotBlank();
        Assertions.assertThat(data.isDeleted()).isFalse();
    }
}
