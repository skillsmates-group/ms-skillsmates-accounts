package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.CertificationEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.ActivityAreaEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.SchoolTypeEntity;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.CertificationRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.helpers.Constants;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.ActivityAreaServiceImpl;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.SchoolTypeServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.yamogroup.skillsmates.accounts.services.AccountFixtures.ACCOUNT_UUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CertificationServiceImplTest {
  @InjectMocks private CertificationServiceImpl objectToTest;
  @Mock private AccountRepository accountRepository;
  @Mock private CertificationRepository certificationRepository;
  @Mock private SchoolTypeServiceImpl schoolTypeService;
  @Mock private ActivityAreaServiceImpl activityAreaService;

  public static String CERTIFICATION_UUID = "696fca85-3c14-4a4c-b69e-3fe0f191b878";

  @Test
  void test_save() {
    CertificationEntity certificationEntity = new CertificationEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    AccountEntity accountEntity = new AccountEntity();
    accountEntity.setUuid(ACCOUNT_UUID);

    certificationEntity.setActivityArea(activityArea);
    certificationEntity.setSchoolType(schoolType);
    certificationEntity.setAccount(accountEntity);

    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);
    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(accountEntity));

    objectToTest.save(certificationEntity);

    verify(certificationRepository)
        .save(
            argThat(
                certification -> {
                  assertSame(certificationEntity.getSchoolType(), certification.getSchoolType());
                  assertSame(
                      certificationEntity.getActivityArea(), certification.getActivityArea());
                  assertSame(certificationEntity.getAccount(), certification.getAccount());
                  return true;
                }));
  }

  @Test
  void test_save_fails_when_account_owner_not_exist() {
    CertificationEntity certificationEntity = new CertificationEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    AccountEntity accountEntity = new AccountEntity();
    accountEntity.setUuid(ACCOUNT_UUID);

    certificationEntity.setActivityArea(activityArea);
    certificationEntity.setSchoolType(schoolType);
    certificationEntity.setAccount(accountEntity);

    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);
    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.save(certificationEntity));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_update() throws BadRequestException {
    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid(ACCOUNT_UUID);
    oldOwner.setUuid(ACCOUNT_UUID);
    CertificationEntity newCertification = new CertificationEntity();
    CertificationEntity oldCertification = new CertificationEntity();
    newCertification.setAccount(newOwner);
    oldCertification.setAccount(oldOwner);
    newCertification.setSchoolType(schoolType);
    newCertification.setSchoolName("schoolName");
    newCertification.setTitle("certification title");
    newCertification.setActivityArea(activityArea);
    newCertification.setPermanent(true);
    newCertification.setStartDate(new Date());
    newCertification.setEndDate(new Date());
    newCertification.setDescription("new Description");
    when(certificationRepository.findByDeletedFalseAndUuid(CERTIFICATION_UUID))
        .thenReturn(Optional.of(oldCertification));
    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);

    objectToTest.update(CERTIFICATION_UUID, newCertification);
    verify(certificationRepository)
        .save(
            argThat(
                certificationEntity -> {
                  assertSame(newCertification.getSchoolType(), certificationEntity.getSchoolType());
                  assertEquals(
                      newCertification.getSchoolName(), certificationEntity.getSchoolName());
                  assertEquals(newCertification.getTitle(), certificationEntity.getTitle());
                  assertSame(
                      newCertification.getActivityArea(), certificationEntity.getActivityArea());
                  assertEquals(newCertification.isPermanent(), certificationEntity.isPermanent());
                  assertSame(newCertification.getStartDate(), certificationEntity.getStartDate());
                  assertSame(newCertification.getEndDate(), certificationEntity.getEndDate());
                  assertEquals(
                      newCertification.getDescription(), certificationEntity.getDescription());
                  return true;
                }));
  }

  @Test
  void test_update_fails_when_certification_not_exist() {
    CertificationEntity newCertification = new CertificationEntity();
    when(certificationRepository.findByDeletedFalseAndUuid(CERTIFICATION_UUID))
        .thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(
            NotFoundException.class,
            () -> objectToTest.update(CERTIFICATION_UUID, newCertification));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_update_throws_exception_when_not_authorize() {
    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid("test");
    oldOwner.setUuid("test1");
    CertificationEntity newCertification = new CertificationEntity();
    CertificationEntity oldCertification = new CertificationEntity();
    newCertification.setAccount(newOwner);
    oldCertification.setAccount(oldOwner);
    when(certificationRepository.findByDeletedFalseAndUuid(CERTIFICATION_UUID))
        .thenReturn(Optional.of(oldCertification));

    ForbiddenException expectedException =
        assertThrows(
            ForbiddenException.class,
            () -> objectToTest.update(CERTIFICATION_UUID, newCertification));
    assertEquals(
        "Ce compte n'a pas les droit de modifier cette certification",
        expectedException.getMessage());
  }

  @Test
  void test_delete() {
    CertificationEntity certificationEntity = new CertificationEntity();
    when(certificationRepository.findByDeletedFalseAndUuid(CERTIFICATION_UUID))
        .thenReturn(Optional.of(certificationEntity));

    objectToTest.delete(CERTIFICATION_UUID);
    verify(certificationRepository)
        .save(
            argThat(
                certificationEntity1 -> {
                  assertTrue(certificationEntity1.isDeleted());
                  return true;
                }));
  }

  @Test
  void test_delete_fails_when_certification_not_exist() {
    when(certificationRepository.findByDeletedFalseAndUuid(CERTIFICATION_UUID))
        .thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.delete(CERTIFICATION_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find() {
    CertificationEntity certificationEntity = new CertificationEntity();
    when(certificationRepository.findByDeletedFalseAndUuid(CERTIFICATION_UUID))
        .thenReturn(Optional.of(certificationEntity));

    CertificationEntity response = objectToTest.find(CERTIFICATION_UUID);
    assertSame(certificationEntity, response);
    checkNoMoreInteraction();
  }

  @Test
  void test_find_fails_when_certification_not_found() {
    when(certificationRepository.findByDeletedFalseAndUuid(CERTIFICATION_UUID))
        .thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.find(CERTIFICATION_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find_by_account() {
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    when(accountRepository.findByActiveTrueAndUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
    when(certificationRepository.findByDeletedFalseAndAccount(
            any(AccountEntity.class), any(Pageable.class)))
        .thenReturn(Optional.of(new PageImpl<>(List.of(new CertificationEntity()))));

    objectToTest.findByAccount(ACCOUNT_UUID);
    verify(certificationRepository)
        .findByDeletedFalseAndAccount(
            argThat(
                accountEntity -> {
                  assertSame(account, accountEntity);
                  return true;
                }),
            argThat(
                pageable -> {
                  assertEquals(Constants.PAGE_DEFAULT_OFFSET, pageable.getPageNumber());
                  assertEquals(Constants.PAGE_DEFAULT_LIMIT, pageable.getPageSize());
                  return true;
                }));
  }

  @Test
  void test_find_by_account_fails_when_account_not_exist() {
    when(accountRepository.findByActiveTrueAndUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.findByAccount(ACCOUNT_UUID));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @AfterEach
  void checkNoMoreInteraction() {
    verifyNoMoreInteractions(
        accountRepository, certificationRepository, schoolTypeService, activityAreaService);
  }
}
