package com.yamogroup.skillsmates.accounts.services.impl.attributes;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.attributes.TrainingEntity;
import com.yamogroup.skillsmates.accounts.dao.entities.parameters.*;
import com.yamogroup.skillsmates.accounts.dao.repositories.AccountRepository;
import com.yamogroup.skillsmates.accounts.dao.repositories.attributes.TrainingRepository;
import com.yamogroup.skillsmates.accounts.exceptions.BadRequestException;
import com.yamogroup.skillsmates.accounts.exceptions.ForbiddenException;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.helpers.Constants;
import com.yamogroup.skillsmates.accounts.services.impl.parameters.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum.HIGHER;
import static com.yamogroup.skillsmates.accounts.dao.enums.account.EducationEnum.SECONDARY;
import static com.yamogroup.skillsmates.accounts.services.AccountFixtures.ACCOUNT_UUID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TrainingServiceImplTest {

  @InjectMocks private TrainingServiceImpl objectToTest;
  @Mock private AccountRepository accountRepository;
  @Mock private TrainingRepository trainingRepository;
  @Mock private SchoolTypeServiceImpl schoolTypeService;
  @Mock private SchoolClassServiceImpl schoolClassService;
  @Mock private DiplomaLevelServiceImpl diplomaLevelService;
  @Mock private TeachingNameServiceImpl teachingNameService;
  @Mock private ActivityAreaServiceImpl activityAreaService;

  public static String TRAINING_UUID = "56aa20cf-bd31-4d5b-8713-c9d1ba91dafa";

  @AfterEach
  void checkNoMoreInteraction() {
    verifyNoMoreInteractions(
        trainingRepository,
        accountRepository,
        schoolTypeService,
        schoolClassService,
        diplomaLevelService,
        teachingNameService,
        activityAreaService);
  }

  @Test
  void test_save_fails_when_education_is_null() {
    TrainingEntity trainingEntity = new TrainingEntity();

    BadRequestException expectedException =
        assertThrows(BadRequestException.class, () -> objectToTest.save(trainingEntity));
    assertEquals("Education est incorrect", expectedException.getMessage());
  }

  @Test
  void test_save_fails_when_owner_is_not_found() {
    TrainingEntity trainingEntity = new TrainingEntity();
    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    SchoolClassEntity schoolClass = new SchoolClassEntity();
    DiplomaLevelEntity diplomaLevel = new DiplomaLevelEntity();
    TeachingNameEntity teachingName = new TeachingNameEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    trainingEntity.setAccount(account);
    trainingEntity.setEducation(SECONDARY);
    trainingEntity.setSchoolType(schoolType);
    trainingEntity.setSchoolClass(schoolClass);
    trainingEntity.setLevel(diplomaLevel);
    trainingEntity.setTeachingName(teachingName);
    trainingEntity.setActivityArea(activityArea);

    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(schoolClassService.findOrCreate(schoolClass)).thenReturn(schoolClass);
    when(diplomaLevelService.findOrCreate(diplomaLevel)).thenReturn(diplomaLevel);
    when(teachingNameService.findOrCreate(teachingName)).thenReturn(teachingName);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.save(trainingEntity));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_save() throws BadRequestException {
    TrainingEntity trainingEntity = new TrainingEntity();
    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    SchoolClassEntity schoolClass = new SchoolClassEntity();
    DiplomaLevelEntity diplomaLevel = new DiplomaLevelEntity();
    TeachingNameEntity teachingName = new TeachingNameEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    trainingEntity.setAccount(account);
    trainingEntity.setEducation(SECONDARY);
    trainingEntity.setSchoolType(schoolType);
    trainingEntity.setSchoolClass(schoolClass);
    trainingEntity.setLevel(diplomaLevel);
    trainingEntity.setTeachingName(teachingName);
    trainingEntity.setActivityArea(activityArea);

    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(schoolClassService.findOrCreate(schoolClass)).thenReturn(schoolClass);
    when(diplomaLevelService.findOrCreate(diplomaLevel)).thenReturn(diplomaLevel);
    when(teachingNameService.findOrCreate(teachingName)).thenReturn(teachingName);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));

    objectToTest.save(trainingEntity);
    verify(trainingRepository)
        .save(
            argThat(
                training -> {
                  assertSame(account, training.getAccount());
                  assertSame(schoolType, training.getSchoolType());
                  assertSame(schoolClass, training.getSchoolClass());
                  assertSame(diplomaLevel, training.getLevel());
                  assertSame(teachingName, training.getTeachingName());
                  assertSame(activityArea, training.getActivityArea());
                  return true;
                }));
  }

  @Test
  void test_update_fails_when_education_is_null() {
    TrainingEntity trainingEntity = new TrainingEntity();

    BadRequestException expectedException =
        assertThrows(
            BadRequestException.class, () -> objectToTest.update(TRAINING_UUID, trainingEntity));
    assertEquals("Education est incorrect", expectedException.getMessage());
  }

  @Test
  void test_update_fails_when_not_authorize() {
    TrainingEntity newTrainingEntity = new TrainingEntity();
    TrainingEntity oldTrainingEntity = new TrainingEntity();
    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid("test");
    oldOwner.setUuid("test1");
    newTrainingEntity.setAccount(newOwner);
    oldTrainingEntity.setAccount(oldOwner);
    newTrainingEntity.setEducation(HIGHER);

    when(trainingRepository.findByDeletedFalseAndUuid(TRAINING_UUID))
        .thenReturn(Optional.of(oldTrainingEntity));

    ForbiddenException expectedException =
        assertThrows(
            ForbiddenException.class, () -> objectToTest.update(TRAINING_UUID, newTrainingEntity));
    assertEquals(
        "Ce compte n'a pas les droit de modifier cette formation", expectedException.getMessage());
  }

  @Test
  void test_update() throws BadRequestException {
    TrainingEntity newTrainingEntity = new TrainingEntity();
    TrainingEntity oldTrainingEntity = new TrainingEntity();
    AccountEntity newOwner = new AccountEntity();
    AccountEntity oldOwner = new AccountEntity();
    newOwner.setUuid(ACCOUNT_UUID);
    oldOwner.setUuid(ACCOUNT_UUID);
    SchoolTypeEntity schoolType = new SchoolTypeEntity();
    SchoolClassEntity schoolClass = new SchoolClassEntity();
    DiplomaLevelEntity diplomaLevel = new DiplomaLevelEntity();
    TeachingNameEntity teachingName = new TeachingNameEntity();
    ActivityAreaEntity activityArea = new ActivityAreaEntity();
    newTrainingEntity.setAccount(newOwner);
    oldTrainingEntity.setAccount(oldOwner);
    newTrainingEntity.setEducation(HIGHER);
    newTrainingEntity.setSchoolType(schoolType);
    newTrainingEntity.setSchoolName("new school name");
    newTrainingEntity.setCity("Douala");
    newTrainingEntity.setSchoolClass(schoolClass);
    newTrainingEntity.setLevel(diplomaLevel);
    newTrainingEntity.setDescription("new training description");
    newTrainingEntity.setTitle("new training title");
    newTrainingEntity.setTeachingName(teachingName);
    newTrainingEntity.setActivityArea(activityArea);

    when(trainingRepository.findByDeletedFalseAndUuid(TRAINING_UUID))
        .thenReturn(Optional.of(oldTrainingEntity));
    when(schoolTypeService.findOrCreate(schoolType)).thenReturn(schoolType);
    when(schoolClassService.findOrCreate(schoolClass)).thenReturn(schoolClass);
    when(diplomaLevelService.findOrCreate(diplomaLevel)).thenReturn(diplomaLevel);
    when(teachingNameService.findOrCreate(newTrainingEntity)).thenReturn(teachingName);
    when(activityAreaService.findOrCreate(activityArea)).thenReturn(activityArea);

    objectToTest.update(TRAINING_UUID, newTrainingEntity);

    verify(trainingRepository)
        .save(
            argThat(
                training -> {
                  assertSame(newTrainingEntity.getEducation(), training.getEducation());
                  assertSame(newTrainingEntity.getSchoolType(), training.getSchoolType());
                  assertSame(newTrainingEntity.getSchoolClass(), training.getSchoolClass());
                  assertSame(newTrainingEntity.getLevel(), training.getLevel());
                  assertSame(newTrainingEntity.getTeachingName(), training.getTeachingName());
                  assertSame(newTrainingEntity.getActivityArea(), training.getActivityArea());
                  assertEquals(newTrainingEntity.getSchoolName(), training.getSchoolName());
                  assertEquals(newTrainingEntity.getCity(), training.getCity());
                  assertEquals(newTrainingEntity.getDescription(), training.getDescription());
                  assertEquals(newTrainingEntity.getTitle(), training.getTitle());
                  return true;
                }));
  }

  @Test
  void test_delete_fails_when_training_not_found() {
    when(trainingRepository.findByDeletedFalseAndUuid(TRAINING_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.delete(TRAINING_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_delete() {
    TrainingEntity trainingEntity = new TrainingEntity();
    when(trainingRepository.findByDeletedFalseAndUuid(TRAINING_UUID))
        .thenReturn(Optional.of(trainingEntity));

    objectToTest.delete(TRAINING_UUID);
    verify(trainingRepository)
        .save(
            argThat(
                training -> {
                  assertTrue(training.isDeleted());
                  return true;
                }));
  }

  @Test
  void test_find_fails_when_training_not_found() {
    when(trainingRepository.findByDeletedFalseAndUuid(TRAINING_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.find(TRAINING_UUID));
    assertEquals("Cette entité n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find() {
    TrainingEntity trainingEntity = new TrainingEntity();
    when(trainingRepository.findByDeletedFalseAndUuid(TRAINING_UUID))
        .thenReturn(Optional.of(trainingEntity));

    TrainingEntity response = objectToTest.find(TRAINING_UUID);
    assertSame(trainingEntity, response);
  }

  @Test
  void test_find_by_account_fails_when_account_not_found() {
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.empty());

    NotFoundException expectedException =
        assertThrows(NotFoundException.class, () -> objectToTest.findByAccount(ACCOUNT_UUID));
    assertEquals("Ce compte n'existe pas", expectedException.getMessage());
  }

  @Test
  void test_find_by_account() {
    AccountEntity account = new AccountEntity();
    account.setUuid(ACCOUNT_UUID);
    when(accountRepository.findByUuid(ACCOUNT_UUID)).thenReturn(Optional.of(account));
    when(trainingRepository.findByDeletedFalseAndAccount(
            any(AccountEntity.class), any(Pageable.class)))
        .thenReturn(Optional.of(new PageImpl<>(List.of(new TrainingEntity()))));

    Page<TrainingEntity> response = objectToTest.findByAccount(ACCOUNT_UUID);
    assertEquals(1, response.getTotalElements());
    verify(trainingRepository)
        .findByDeletedFalseAndAccount(
            argThat(
                training -> {
                  assertSame(account, training);
                  return true;
                }),
            argThat(
                pageable -> {
                  assertEquals(Constants.PAGE_DEFAULT_OFFSET, pageable.getPageNumber());
                  assertEquals(Constants.PAGE_DEFAULT_LIMIT, pageable.getPageSize());
                  return true;
                }));
  }
}
