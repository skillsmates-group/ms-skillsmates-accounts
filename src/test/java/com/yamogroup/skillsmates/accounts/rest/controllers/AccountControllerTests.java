package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.AccountCreationRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountCreationResponse;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountDetailsResponse;
import com.yamogroup.skillsmates.accounts.services.AccountFixtures;
import com.yamogroup.skillsmates.accounts.services.AccountService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@WebMvcTest(AccountController.class)
public class AccountControllerTests extends BaseControllerUnitTest {
    private static final String URL_TEMPLATE = "/skillsmates/accounts";

    @MockBean
    private AccountService accountService;

    @Test
    public void should_throw_exception_when_blank_firstname() {
        AccountCreationRequest request = AccountCreationRequest.builder()
                .firstname("")
                .lastname("edima")
                .email("samantha.edima@yopmail.com")
                .password("password")
                .confirmPassword("password").build();

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertErrorResponseBodyContains(HttpStatus.BAD_REQUEST, "firstname is required");
    }

    @Test
    void should_throw_exception_when_blank_lastname() {
        AccountCreationRequest request = AccountCreationRequest.builder()
                .firstname("samantha")
                .lastname("")
                .email("samantha.edima@yopmail.com")
                .password("password")
                .confirmPassword("password").build();

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertErrorResponseBodyContains(HttpStatus.BAD_REQUEST, "lastname is required");
    }

    @Test
    void should_throw_exception_when_blank_email() {
        AccountCreationRequest request = AccountCreationRequest.builder()
                .firstname("samantha")
                .lastname("edima")
                .email("")
                .password("password")
                .confirmPassword("password").build();

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertErrorResponseBodyContains(HttpStatus.BAD_REQUEST, "email is required");
    }

    @Test
    void should_throw_exception_when_invalid_email() {
        AccountCreationRequest request = AccountCreationRequest.builder()
                .firstname("samantha")
                .lastname("edima")
                .email("samantha.edima.yopmail.com")
                .password("password")
                .confirmPassword("password").build();

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertErrorResponseBodyContains(HttpStatus.BAD_REQUEST, "must be a correct email address");
    }

    @Test
    void should_throw_exception_when_not_matching_password() {
        AccountCreationRequest request = AccountCreationRequest.builder()
                .firstname("samantha")
                .lastname("edima")
                .email("samantha.edima@yopmail.com")
                .password("password")
                .confirmPassword("wordpass").build();

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertErrorResponseBodyContains(HttpStatus.BAD_REQUEST, "Passwords are not the same");
    }

    @Test
    void should_create_account_when_valid_input() throws Exception {
        AccountCreationRequest request = AccountCreationRequest.builder()
                .firstname("samantha")
                .lastname("edima")
                .email("samantha.edima@yopmail.com")
                .password("password")
                .confirmPassword("password").build();
        AccountEntity created = AccountFixtures.generateAccountEntity();
        when(accountService.create(any(AccountEntity.class))).thenReturn(created);

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.CREATED);
        assertResponseBodyIs(new AccountCreationResponse(created));
        verify(accountService).create(Mockito.argThat(argument -> {
            assertEquals(request.firstname(), argument.getFirstname());
            assertEquals(request.lastname(), argument.getLastname());
            assertEquals(request.email(), argument.getEmail());
            assertEquals(request.password(), argument.getPassword());
            assertNull(argument.getStatus());
            return true;
        }));
    }

    @Test
    void should_throw_not_found_exception_when_account_does_not_exist() {
        String exceptionMessage = "exceptionMessage";
        Mockito.doThrow(new NotFoundException(exceptionMessage)).when(accountService).find(AccountFixtures.NOT_EXISTED_ACCOUNT_UUID);

        performGet("/" + AccountFixtures.NOT_EXISTED_ACCOUNT_UUID, new HashMap<>());
        Assertions.assertTrue(thrownException instanceof NotFoundException);
        assertErrorResponseBodyContains(HttpStatus.NOT_FOUND, exceptionMessage);
        verify(accountService).find(AccountFixtures.NOT_EXISTED_ACCOUNT_UUID);
    }

    @Test
    void should_get_account_when_account_exists() {
        AccountEntity account = AccountFixtures.generateAccountEntity();
//        when(accountService.find(AccountFixtures.NOT_EXISTED_ACCOUNT_UUID)).thenReturn(account);

        performGet("/" + AccountFixtures.NOT_EXISTED_ACCOUNT_UUID, new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.OK);
        assertResponseBodyIs(new AccountDetailsResponse(account));
        verify(accountService).find(AccountFixtures.NOT_EXISTED_ACCOUNT_UUID);
    }

    @Override
    protected String basePath() {
        return URL_TEMPLATE;
    }

    @Override
    protected List<Object> getControllerMockedBeans() {
        return List.of(accountService);
    }
}
