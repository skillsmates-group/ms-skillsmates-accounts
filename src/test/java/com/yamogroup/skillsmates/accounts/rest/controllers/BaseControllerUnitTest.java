package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yamogroup.skillsmates.accounts.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@Slf4j
public abstract class BaseControllerUnitTest {
    protected Exception thrownException;
    protected MockHttpServletResponse response;
    @Autowired
    private MockMvc mockMvc;
    private MvcResult mvcResult;
    private MockHttpServletRequestBuilder request;
    private MockMultipartHttpServletRequestBuilder multipartRequest;
    private String currentEnpoint;

    protected void performGet(String endpoint, Map<String, String> queryParams) {
        currentEnpoint = basePath().concat(endpoint);
        request = get(currentEnpoint);
        buildAndSendRequest(Optional.empty(), queryParams);
    }

    protected void performGet(String endpoint, Object queryParams) {
        currentEnpoint = basePath().concat(endpoint);
        request = get(currentEnpoint);
        buildAndSendRequest(Optional.empty(), JsonUtils.convertToMap(queryParams));
    }

    protected void performDelete(String endpoint, Map<String, String> queryParams) {
        currentEnpoint = basePath().concat(endpoint);
        request = delete(currentEnpoint);
        buildAndSendRequest(Optional.empty(), queryParams);
    }

    protected void performPost(String endpoint, Optional<Object> body, Map<String, String> queryParams) {
        currentEnpoint = basePath().concat(endpoint);
        request = post(currentEnpoint);
        buildAndSendRequest(body, queryParams);
    }

    protected void performPut(String endpoint, Optional<Object> body, Map<String, String> queryParams) {
        currentEnpoint = basePath().concat(endpoint);
        request = put(currentEnpoint);
        buildAndSendRequest(body, queryParams);
    }

    protected void performPatch(String endpoint, Optional<Object> body, Map<String, String> queryParams) {
        currentEnpoint = basePath().concat(endpoint);
        request = patch(currentEnpoint);
        buildAndSendRequest(body, queryParams);
    }

    protected void performMultipart(String endpoint, HttpMethod method, Map<String, Object> multipartData) {
        try {
            currentEnpoint = basePath().concat(endpoint);
            multipartRequest = multipart(currentEnpoint);
            ReflectionTestUtils.setField(multipartRequest, "method", method.name());
            multipartData.keySet().parallelStream().forEach(key -> {
                Object data = multipartData.get(key);
                if (data instanceof String) {
                    multipartRequest.file(new MockMultipartFile(key, "test.txt", MediaType.TEXT_PLAIN_VALUE, ((String) (data)).getBytes()));
                } else {
                    multipartRequest.file(new MockMultipartFile(key, null, MediaType.APPLICATION_JSON_VALUE, JsonUtils.mapToJson(multipartData.get(key)).getBytes()));
                }
            });
            mvcResult = mockMvc.perform(multipartRequest).andReturn();
            response = mvcResult.getResponse();
            thrownException = mvcResult.getResolvedException();
        } catch (Exception e) {
            thrownException = e;
        }
    }

    protected void assertResponseStatusCodeIs(HttpStatus statusCode) {
        assertEquals(statusCode.value(), response.getStatus());
    }

    protected void assertResponseBodyIs(Object expectedBody) {
        try {
            JsonUtils.assertObjectAndJsonAreEquals(expectedBody, response.getContentAsString());
        } catch (UnsupportedEncodingException e) {
            log.warn(e.getMessage());
        }
    }

    protected void assertPageResponseContains(List<Object> objects) {
        assertResponseBodyIs(new PageImpl<>(objects));
    }

    protected void assertResponseBodyIsEmpty() {
        assertResponseBodyIs("");
    }

    protected void performVerificationWhenBodyBadRequestValidation(String errorMessage) {
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertValidationErrorContains(errorMessage);
    }

    protected void assertErrorResponseBodyContains(HttpStatus status, String errorMessage) {
        assertResponseStatusCodeIs(status);
        if (thrownException instanceof NotFoundException) {
            assertResponseBodyIs(
                    Error.builder().errors(
                            List.of(ErrorResponse.builder()
                                    .id(response.getHeader("X-YG-Correlation-Id"))
                                    .status(String.valueOf(status.value()))
                                    .title("Not Found")
                                    .code(status.name())
                                    .detail(errorMessage).build())).build());
            return;
        }
        if (thrownException instanceof BindException ex) {
            assertTrue(ex.getBindingResult().getAllErrors().parallelStream().anyMatch(e ->
                    Objects.requireNonNull(e.getDefaultMessage()).equals(errorMessage)), "Thrown exception message not found");
        } else {
            assertEquals(errorMessage, thrownException.getMessage());
        }
    }

    private void assertValidationErrorContains(String message) {
        MethodArgumentNotValidException methodArgumentNotValidException = (MethodArgumentNotValidException) mvcResult.getResolvedException();
        List<ObjectError> objectErrors = Objects.requireNonNull(methodArgumentNotValidException).getBindingResult().getAllErrors();
        objectErrors.parallelStream().forEach(error -> log.warn("Validation error message: ##" + error.getDefaultMessage() + "##"));
        assertTrue(objectErrors.parallelStream().anyMatch(error -> Objects.requireNonNull(error.getDefaultMessage()).equals(message)));
    }

    private void buildAndSendRequest(Optional<Object> body, Map<String, String> queryParams) {
        try {
            request.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_VALUE);
            body.ifPresent(o -> {
                String bodyAsJsonString = JsonUtils.mapToJson(o);
                log.info("body is: {}", bodyAsJsonString);
                request.content(bodyAsJsonString);
            });
            if (!queryParams.isEmpty()) {
                log.info("query params are: {}", JsonUtils.mapToJson(queryParams));
            }
            queryParams.keySet().forEach(key -> request.param(key, queryParams.get(key)));
            mvcResult = mockMvc.perform(request).andReturn();
            response = mvcResult.getResponse();
            thrownException = mvcResult.getResolvedException();
        } catch (Exception e) {
            thrownException = e;
        }
    }

    @BeforeEach
    public void setup() {
        this.thrownException = null;
        this.response = null;
        this.currentEnpoint = null;
    }

    @AfterEach
    public void commonVerification() {
        getControllerMockedBeans().parallelStream().forEach(Mockito::verifyNoMoreInteractions);
    }

    protected abstract String basePath();

    protected abstract List<Object> getControllerMockedBeans();

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ErrorResponse implements Serializable {
        private String id;
        private String status;
        private String title;
        private String code;
        private String detail;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Error implements Serializable {
        private List<ErrorResponse> errors;
    }

    public static class JsonUtils {
        public static ObjectMapper objectMapper = new ObjectMapper();

        public static void assertObjectAndJsonAreEquals(Object expected, String actual) {
            if (expected instanceof String) {
                String expectedAsString = (String) expected;
                assertEquals(expectedAsString, actual);
            } else {
                assertEquals(mapToJsonNode(expected), mapToJsonNode(actual));
            }
        }

        public static String mapToJson(Object object) {
            String jsonString = null;
            try {
                jsonString = objectMapper.writeValueAsString(object);
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
            }
            return jsonString;
        }

        private static JsonNode mapToJsonNode(Object object) {
            JsonNode jsonNode = null;
            try {
                jsonNode = objectMapper.readTree(mapToJson(object));
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
            }
            return jsonNode;
        }

        private static JsonNode mapToJsonNode(String jsonString) {
            JsonNode jsonNode = null;
            try {
                jsonNode = objectMapper.readTree(jsonString);
            } catch (JsonProcessingException e) {
                log.error(e.getMessage());
            }
            return jsonNode;
        }

        public static boolean assertJsonValueAreEquals(Object expected, Object actual) {
            return mapToJsonNode(expected).equals(mapToJsonNode(actual));
        }

        public static Map<String, String> convertToMap(Object object) {
            Map<String, String> map = new HashMap<>();
            JsonNode jsonNode = mapToJsonNode(object);
            Iterator<String> fieldNames = jsonNode.fieldNames();
            while (fieldNames.hasNext()) {
                String key = fieldNames.next();
                map.put(key, jsonNode.findValue(key).asText(""));
            }
            return map;
        }
    }

}
