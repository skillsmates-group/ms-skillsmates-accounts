package com.yamogroup.skillsmates.accounts.rest.controllers;

import com.yamogroup.skillsmates.accounts.dao.entities.account.AccountEntity;
import com.yamogroup.skillsmates.accounts.rest.dto.requests.AccountAuthRequest;
import com.yamogroup.skillsmates.accounts.rest.dto.responses.account.AccountAuthResponse;
import com.yamogroup.skillsmates.accounts.services.AccountFixtures;
import com.yamogroup.skillsmates.accounts.services.AuthenticateService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@WebMvcTest(AuthenticationController.class)
public class AuthenticationControllerTests extends BaseControllerUnitTest {
    private static final String URL_TEMPLATE = "/skillsmates/authenticate";
    @MockBean
    private AuthenticateService authenticateService;

    @Test
    void should_throw_exception_when_blank_email() {
        AccountAuthRequest request = AccountAuthRequest.builder().email("").password("password").build();

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertErrorResponseBodyContains(HttpStatus.BAD_REQUEST, "email is required");
    }

    @Test
    void should_throw_exception_when_blank_password() {
        AccountAuthRequest request = AccountAuthRequest.builder().email("samantha.edima@yopmail.com").password("").build();

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertErrorResponseBodyContains(HttpStatus.BAD_REQUEST, "password is required");
    }

    @Test
    void should_throw_exception_when_invalid_email() {
        AccountAuthRequest request =
                AccountAuthRequest.builder().email("samantha.edima.yopmail.com").password("password").build();

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.BAD_REQUEST);
        assertErrorResponseBodyContains(HttpStatus.BAD_REQUEST, "must be a correct email address");
    }

    @Test
    void should_authenticate_account_when_valid_input() throws Exception {
        AccountAuthRequest request =
                AccountAuthRequest.builder().email("samantha.edima@yopmail.com").password("password").build();
        AccountEntity account = AccountFixtures.generateAccountEntity();
        when(authenticateService.authenticate(any(AccountEntity.class))).thenReturn(account);

        performPost("/", Optional.of(request), new HashMap<>());
        assertResponseStatusCodeIs(HttpStatus.OK);
        assertResponseBodyIs(new AccountAuthResponse(account));
        verify(authenticateService).authenticate(Mockito.argThat(argument -> {
            assertEquals(request.email(), argument.getEmail());
            assertEquals(request.password(), argument.getPassword());
            return true;
        }));
    }

    @Override
    protected String basePath() {
        return URL_TEMPLATE;
    }

    @Override
    protected List<Object> getControllerMockedBeans() {
        return List.of(authenticateService);
    }
}
