# Changelog

## Next

### 0.1.4

* filter suggestion accounts

### 0.1.3

* fix database migration

### 0.1.2

* fix count posts

### 0.1.1

* fix search profile

### 0.1.0

* Data migration